<?php
error_reporting(0);
ini_set('display_errors', 'Off');
include_once "configuration/config.php";
include "viisp/xmlclasstst.php";
if (empty($_POST['ticket']) === FALSE && empty($_POST['customData']) === FALSE) {
    $ret_data = get_viisp_user_info($_POST['ticket'], $_POST['customData']);
    $authentication_data = ['code' => $ret_data['lt-personal-code'], 'uname' => $ret_data['firstName'], 'usurname' => $ret_data['lastName']];
} 
else {
    $document = new DOMDocument('1.0', 'utf-8');
    $document->formatOutput = false;
    $request = $document->createElement('ns2:authenticationRequest');
    $id = $document->createAttribute('id');
    $id->value = 'uniqueNodeId';
    $xmlns2 = $document->createAttribute('xmlns:ns2');
    $xmlns2->value = 'http://www.epaslaugos.lt/services/authentication';
    $xmlns0 = $document->createAttribute('xmlns');
    $xmlns0->value = 'http://www.w3.org/2000/09/xmldsig#';
    $xmlns3 = $document->createAttribute('xmlns:ns3');
    $xmlns3->value = 'http://www.w3.org/2001/10/xml-exc-c14n#';
    $request->appendChild($xmlns2);
    $request->appendChild($xmlns0);
    $request->appendChild($xmlns3);
    $request->appendChild($id);
    //<ns2:pid>VSID000000000113</ns2:pid>
    $pid = $document->createElement('ns2:pid', PID);
	//$pid = $document->createElement('ns2:pid', 'VSID000000001950');
    //<ns2:authenticationProvider>auth.lt.identity.card</ns2:authenticationProvider>
    $authenticationProvider1 = $document->createElement('ns2:authenticationProvider', 'auth.lt.identity.card');
    //<ns2:authenticationProvider>auth.lt.bank</ns2:authenticationProvider>
    $authenticationProvider2 = $document->createElement('ns2:authenticationProvider', 'auth.lt.bank');
    //<ns2:authenticationProvider>auth.signatureProvider</ns2:authenticationProvider>
    $authenticationProvider3 = $document->createElement('ns2:authenticationProvider', 'auth.signatureProvider');
    //<ns2:authenticationProvider>auth.login.pass</ns2:authenticationProvider>
    $authenticationProvider4 = $document->createElement('ns2:authenticationProvider', 'auth.login.pass');
    //<ns2:authenticationAttribute>lt-personal-code</ns2:authenticationAttribute>
    $authenticationAttribute1 = $document->createElement('ns2:authenticationAttribute', 'lt-personal-code');
    //<ns2:authenticationAttribute>lt-company-code</ns2:authenticationAttribute>
    $authenticationAttribute2 = $document->createElement('ns2:authenticationAttribute', 'lt-company-code');
    //<ns2:userInformation>firstName</ns2:userInformation>
    $userInformation1 = $document->createElement('ns2:userInformation', 'firstName');
    //<ns2:userInformation>lastName</ns2:userInformation>
    $userInformation2 = $document->createElement('ns2:userInformation', 'lastName');
    //<ns2:userInformation>companyName</ns2:userInformation>
    $userInformation3 = $document->createElement('ns2:userInformation', 'companyName');
    //<ns2:postbackUrl>http://localhost</ns2:postbackUrl>
    $postbackUrl = $document->createElement('ns2:postbackUrl', GLOBAL_SITE_URL.'login.php');
    //<ns2:customData>correlation-123</ns2:customData>
    $customData = $document->createElement('ns2:customData', 'emiestas');
    $request->appendChild($pid);
    $request->appendChild($authenticationProvider1);
    $request->appendChild($authenticationProvider2);
    $request->appendChild($authenticationProvider3);
    $request->appendChild($authenticationProvider4);
    $request->appendChild($authenticationAttribute1);
    $request->appendChild($authenticationAttribute2);
    $request->appendChild($userInformation1);
    $request->appendChild($userInformation2);
    $request->appendChild($userInformation3);
    $request->appendChild($postbackUrl);
    $request->appendChild($customData);

    //prikabinam requesta prie dokumento
    $document->appendChild($request);
    //pasiimam xml'a
    $xml_pay2 = trim($document->saveXml());

    $doc = $xml_pay2;

    if (!$doc) {
        //echo "Error while parsing the input xml document\n";
        exit;
    }
    $xmlsec = new xmlsec(dirname(__FILE__) . '/viisp/keys.xml', dirname(__FILE__) . '/temp/');

    $xmlsec->viisp_act = 'authenticationRequest';
    $res = $xmlsec->sign($document, XMLSEC_RSA_SHA1 /* , array(''=>'testKey') */);

    if (!$res) {
        die($xmlsec->errorMsg . '<hr>' . $xmlsec->cmd);
        header("Location: /index.php");
    } 
    $res = str_replace('<?xml version="1.0" encoding="utf-8"?>', '', $res);
    $soap = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://www.epaslaugos.lt/services/authentication" xmlns:xd="http://www.w3.org/2000/09/xmldsig#">
	   <soapenv:Header/>
	   <soapenv:Body>
	' . $res . '
	   </soapenv:Body>
	</soapenv:Envelope>';
    $url = 'https://www.epaslaugos.lt/portal/authenticationServices/auth';
    $soap_do = curl_init();
    curl_setopt($soap_do, CURLOPT_URL, $url);
    curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($soap_do, CURLOPT_POST, true);
    curl_setopt($soap_do, CURLOPT_POSTFIELDS, $soap);
    curl_setopt($soap_do, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($soap)));
    //curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
    $result = curl_exec($soap_do);
    $err = curl_error($soap_do);
    $response = new DOMDocument('1.0', 'utf-8');
    $response->loadXML($result);
    $ticket = $response->getElementsByTagName('ticket')->item(0)->nodeValue;
    $form = "	
		<form id='login' action='https://www.epaslaugos.lt/portal/external/services/authentication/v2/'>
			<input type='hidden' name='ticket' value='" . $ticket . "'>
			"/* ."<input type=\"submit\" value=\"Submit\">
          <input type='text' name='postbackURL' value='http://smartweb.dev'>
          <input type='text' name='customData' value='http://smartweb.dev'>" */ . "	
		</form>
		<script type='text/javascript'> document.getElementById(\"login\").submit(); </script>
			";
    echo $form;
    die;
}