<?php
ini_set('max_execution_time', 120);
ini_set('session.cookie_httponly', 1);
ini_set('session.cookie_secure', 1);  
session_start();
error_reporting(0);
ini_set('display_errors','Off');
date_default_timezone_set('Europe/Vilnius');
include_once('./configuration/config.php');

if(isset($_GET['city']) && is_numeric($_GET['city'])){
	$_SESSION['CITY_ID']=$_GET['city'];
}
else if(isset($problemCity) && $problemCity>0){
	$_SESSION['CITY_ID']=$problemCity;
}
else if(!$_SESSION['CITY_ID']){
	$_SESSION['CITY_ID']=1; // default - Vilnius
}
if(isset($PRINTSESS) && $PRINTSESS==1){
	print_r($_SESSION);
}
define('CITY_ID', $_SESSION['CITY_ID']); 
$pageCfg=getPageConfig($_SESSION['CITY_ID']);
define('PAGE_SIZE', $pageCfg['PAGE_SIZE']);
define('MAP_ITEMS_SHOW', $pageCfg['MAP_ITEMS_SHOW']);
define('MINIO_BUCKET_NAME', 'tvarkau.'.$pageCfg['CITY_SYSTEM_NAME'].'.'.date("Y"));

// SMARTY include smarty file and set options
include_once(LIB_REAL_URL .'smarty/libs/Smarty.class.php');
$smarty = new Smarty;
$smarty->template_dir = './';
$smarty->compile_dir  = MODULE_SMARTY_PATH .'templates_c';
$smarty->cache_dir= MODULE_SMARTY_PATH .'cache';
$smarty->config_dir   = MODULE_SMARTY_PATH .'configs';
$smarty->assign('module_title'   , MODULE_TITLE);
$smarty->assign('CITY_NAME_EN',$pageCfg['CITY_SYSTEM_NAME']);
$smarty->assign('cityname', $pageCfg['CITY_NAME']);
$smarty->assign('CITY_LABEL', $pageCfg['CITY_LABEL'] );
$smarty->assign('CITY_LAT', $pageCfg['CITY_LAT']);
$smarty->assign('CITY_LNG', $pageCfg['CITY_LNG']);
$smarty->assign('LOGO_IMAGE', $pageCfg['LOGO_IMAGE']);
$smarty->assign('SITE_MAIN_COLOR', $pageCfg['SITE_MAIN_COLOR']);
$smarty->assign('SITE_SECOND_COLOR', $pageCfg['SITE_SECOND_COLOR']);
$smarty->assign('GLOBAL_SITE_URL', GLOBAL_SITE_URL);
$smarty->assign('MINIO_SITE_URL', MINIO_SITE_URL);
$smarty->assign('CITY_ID', $_SESSION['CITY_ID']);
$smarty->assign('SESSION', (isset($_SESSION['AUTH']) && $_SESSION['AUTH']==1)?$_SESSION:'');
$smarty->assign('PHONE_NUMBER', $pageCfg['PHONE']);
$smarty->assign('EMAIL', $pageCfg['MAIL']);
$smarty->assign('FACEBOOK_APP_ID', FACEBOOK_APP_ID);
$smarty->assign('GOOGLE_CLIENT_ID', GOOGLE_CLIENT_ID);
$smarty->assign('GOOGLE_API_KEY', GOOGLE_API_KEY);
$smarty->assign('OSM_ACCESS_TOKEN', OSM_ACCESS_TOKEN);
$smarty->assign('sentry_tracker', SENTRY_JS);
$smarty->assign('POWERBI_LINK', POWERBI_LINK);
// END SMARTY

if(!empty($_GET['problemID'])) {
    if((int)$_GET['problemID']) {
        $get_problem=$_GET['problemID'];
        $problemCity=getProblemCity($get_problem);
    } else {
        die('Klaida 404');
    }
}

//get current menu page
$currentMenuPage= $_SERVER['REQUEST_URI'];
$smarty->assign('currentMenuIsset', $currentMenuPage);

/* getActiveCity */
$activeCity = getActiveCities();
$smarty->assign('activeCity', $activeCity);

$content   = '';// page content
if((isset($_POST['ticket']) && isset($_POST['customData']))){
	include 'login.php';
}

$menu = new Menu();
$menu_array = array();
$menu_array = $menu -> getMenu();
for($i = 0; $i < count($menu_array); $i++){
	$menu_front[] = array(
		'sys_name' => $menu_array[$i]['sys_name'],
		'name' => $menu_array[$i]['name'],
		'link' => $menu_array[$i]['sys_name'],
		'private' => $menu_array[$i]['private'],
		'tpl'  => $menu_array[$i]['tpl'],
		'php'  => $menu_array[$i]['php'],
		'tpl_dir'   => isset($menu_array[$i]['tpl_dir']) ? $menu_array[$i]['tpl_dir'] : "",
		'visible'  => ($menu_array[$i]['hide']==1 || ($menu_array[$i]['sys_name']=='logout' && !isset($_SESSION['AUTH'])))?false:true,      
		'icon'  => $menu_array[$i]['icon'],
        'group'  => $menu_array[$i]['group'],
        'currentMenu' => ($currentMenuPage == '/'.$menu_array[$i]['sys_name'] ? 1 : 0),
        'index_page' =>$menu_array[$i]['index_page']
	);
}
$pagename=(isset($_GET['p']))?$_GET['p']:'problems_list';
$menu_key=array_search($pagename, array_column($menu_front,'sys_name'));  
if (is_numeric($menu_key)){
	$mid=$menu_key;
}
else{
	$error='
		<div class="span4 offset4 alert alert-danger thanks-div">Tokio puslapio nėra!<br>
			<img src="'.MODULE_IMAGES_URL.'VILNIUS_RED_TRANSPARENT_RGB.png">
		</div>
		<div class="span4 offset4 alert alert-info">
			<a href="'.GLOBAL_SITE_URL.'" class="alert-link">Grįžti į pradinį puslapį</a>
		</div>';
}

if(isset($_POST['unsubscribe'])) {
    unsubscribe($_POST['problem_id'], $_POST['user_id']);
    die();
}

if(isset($_POST['follow'])) {
    addToSubscription($_POST['problem_id'], $_POST['user_id']);
    die();
}

if(isset($_POST['like_push'])){
	setcookie('counter'.$_POST['problem_id'], 'like', time()+3600*24);
	if($_POST['like_push']=='glyphicon-thumbs-up'){
		$type='LIKE';
	}
	else if($_POST['like_push']=='glyphicon-thumbs-down'){
		$type='DISLIKE';
	}
	updateProblemLikes($_POST['problem_id'], $type);
	$problem=getProblem($_POST['problem_id']);
	echo json_encode(array(
		'like'=>$problem['data']['NUM_LIKE'],
		'dislike'=>$problem['data']['NUM_DISLIKE']
	));
	die();
}

if(isset($_POST['typeIDForSub']) && ((int)$_POST['typeIDForSub'])>0){   /// check if selected type has subtypes >> ajax
	$getTypes=getProblemTypes(null,null,null,$_POST['typeIDForSub']);
	echo $getTypes ? json_encode($getTypes):'';
	die();
}

if(isset($_POST['editProblemSubmit'])){
	$problemData=getProblem($_POST['edit_problem_id']);   // for getting USER_ID
	if($problemData['data']['PROBLEM_USER']==$_SESSION['USER_ID']){
		if(trim($_POST['edit_desc'])!=''){    // !no empty message
			updateProblem($_POST['edit_problem_id'], 'DESCRIPTION', filter_var($_POST['edit_desc'], FILTER_SANITIZE_STRING));
			updateProblemAvilys($_POST['edit_problem_oid'], array('edit_desc'=>filter_var($_POST['edit_desc'], FILTER_SANITIZE_STRING), 'foto'=>$_POST['foto']));
			$message2='
			<div class="span4 offset4 alert alert-success alert-dismissable thanks-div">
				<button type="button" class="close" aria-hidden="true">&times;</button>
				Jūsų pranešimas buvo sėkmingai papildytas. Dėkojame!
			</div>';
		}
		else{
			$message2='
			<div class="span4 offset4 alert alert-danger alert-dismissable thanks-div">
				<button type="button" class="close" aria-hidden="true">&times;</button>
				Problemos aprašymas negali būti tuščias!
			</div>';
		}
	}
	else{
		$message2='
		<div class="span4 offset4 alert alert-danger alert-dismissable thanks-div">
			 <button type="button" class="close" aria-hidden="true">&times;</button>
				Įvyko klaida
		</div>';	
	}
	$smarty->assign('message2', $message2);
}
if(isset($_POST['editImgSubmit'])) {
    $problemData = getProblem($_POST['problem_id']);
	if($problemData['data']['PROBLEM_USER'] == $_SESSION['USER_ID']) {
		if(trim($_POST['desc']) != '') {
            if(!empty($_POST['foto'])) {
                $_POST['imgs'] = (isset($_POST['imgs'])) ? $_POST['imgs'] : array();
                $count = count($_POST['imgs']);
                foreach($_POST['foto'] as $photo) {
                    array_push($_POST['imgs'], $photo);
                }
                foreach($_POST['imgs'] as $img) {
                    $count++;
                    $title[] = 'Prisegtas_dokumentas_'.$count;
                }
                addImages($_POST['problem_id'], $_POST['foto'], $title);
                updateProblemAvilys($_POST['problem_oid'], array('edit_desc' => filter_var($_POST['desc'], FILTER_SANITIZE_STRING), 'foto' => $_POST['imgs'], 'problem_id' => $_POST['problem_id']));
            } 
            if(!empty($_POST['img'])) {
                $imgs = getPhotosByID($_POST['img']);
                deleteImages($_POST['img']);
                deleteProblemImg($_POST['problem_oid'], array('foto' => $imgs));
            }
            
			$message2='
			<div class="span4 offset4 alert alert-success alert-dismissable thanks-div">
				<button type="button" class="close" aria-hidden="true">&times;</button>
				Jūsų pranešimas buvo sėkmingai papildytas. Dėkojame!
			</div>';
		} else {
			$message2='
			<div class="span4 offset4 alert alert-danger alert-dismissable thanks-div">
				<button type="button" class="close" aria-hidden="true">&times;</button>
				Problemos aprašymas negali būti tuščias!
			</div>';
		}
	} else {
		$message2='
		<div class="span4 offset4 alert alert-danger alert-dismissable thanks-div">
			 <button type="button" class="close" aria-hidden="true">&times;</button>
				Įvyko klaida
		</div>';	
	}
	$smarty->assign('message2', $message2);
}
if(isset($_POST['deleteProblem'])) {
	$problemData=getProblem($_POST['problem_id']);   // for getting USER_ID
	if($problemData['data']['PROBLEM_USER']==$_SESSION['USER_ID']){
		deleteProblem($_POST['problem_id'], $_POST['problem_oid'], $_POST['reason']);
		header('Location: /user_problems_list');
	}
}
if($menu_front[$mid]['php']){
	include_once($menu_front[$mid]['php']);
}
if(isset($_POST['problemID'])){  /// if selected from right list >> ajax
	$last_problem=getProblem($_POST['problemID']);
	$smarty->assign('actual_problem' , $actual_problem);
	$smarty->assign('last_problem_data' , $last_problem['data']);
	$smarty->assign('last_problem_foto' , $last_problem['foto']);
	$smarty->assign('last_problem_answer' , $last_problem['answer_foto']);
	$smarty->assign('voted', 'counter'.$_POST['problemID']);
    $smarty->assign('subscribed', checkIfSubscribed($_POST['problemID'], $_SESSION['USER_ID']));
    $smarty->assign('isOwner', checkIfProblemOwnedByOwner($_POST['problemID'], $_SESSION['USER_ID']));
	$smarty->display(MODULE_TEMPLATES_PATH.'last_problem.tpl');
	die();
}
$smarty->assign('error', isset($error)?$error:'');
$smarty->assign('menu'   , $menu_front);
$smarty->assign('content', $content);
$smarty->assign('MODULE_IMAGES_URL', MODULE_IMAGES_URL);
$smarty->assign('MODULE_PATH_INNER', MODULE_PATH_INNER);
$smarty->assign('LIB_SITE_URL', LIB_SITE_URL);
$smarty->assign('NEW_USER_LINK', NEW_USER_LINK);
$smarty->assign('CITY_LOGO', CITY_LOGO);
$smarty->display(MODULE_TEMPLATES_PATH .'main_template.tpl'); 
?> 