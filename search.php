<?php
$selectedStatusArray = array();
$selectedProblemTypeArray = array();
if(isset($_POST['searchButton']) || isset($_POST['pageSelected'])) {
	$smarty->assign('addressOrRegId', !empty($_POST['addressOrRegId']) ? filter_var($_POST['addressOrRegId'], FILTER_SANITIZE_STRING) : '');
	$smarty->assign('dateFrom', !empty($_POST['dateFrom']) ? $_POST['dateFrom'] : '');
	$smarty->assign('dateTo', !empty($_POST['dateTo']) ? $_POST['dateTo'] : '');
	$smarty->assign('keyWord', !empty($_POST['keyWord']) ? filter_var($_POST['keyWord'], FILTER_SANITIZE_STRING) : '');
        
    if(!empty($_POST['dateFrom']) || !empty($_POST['dateTo']) || !empty($_POST['keyWord']) || !empty($_POST['problemTypeId']) ||!empty($_POST['statusesId'])) {
        $smarty->assign('detailSearchIsActive', 'activeFilter activeSearch');
    }
        
        $searchCriteria = array(
            'addressOrRegId' => filter_var($_POST['addressOrRegId'], FILTER_SANITIZE_STRING),
            'problemStatus' => $_POST['statusesId'],
            'problemType' => $_POST['problemTypeId'],
            'dateFrom' => $_POST['dateFrom'],
            'dateTo' => $_POST['dateTo'],
            'keyWord' => filter_var($_POST['keyWord'], FILTER_SANITIZE_STRING)
        );
        
        if(!empty($_POST['statusesId']))
            $selectedStatusArray=explode(",",$_POST['statusesId']);
        
        if(!empty($_POST['problemTypeId'])) {
            $selectedProblemTypeArray=explode(",",$_POST['problemTypeId']);
        }
}

$userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : '';

$smarty->assign('selectedSType' , $selectedProblemTypeArray);
$smarty->assign('selectedStatus' , $selectedStatusArray);
$smarty->assign('problem_statuses' , getProblemStatuses());
$smarty->assign('problem_types' , getProblemTypes($searchCriteria, $selectedProblemTypeArray, $userId));
if (!empty($searchCriteria)):
    if(isset($_POST['detailSearchButton'])):
        saveSearchCriterias($searchCriteria,$_SESSION,2);
    else:
        saveSearchCriterias($searchCriteria,$_SESSION,1);
    endif;
endif;
?>