<?php
include('configuration/config.php');
include(LIB_REAL_URL.'fileuploader/class.fileuploader.php');
/*
// upload directory and check/create if doesn't exists
if (!file_exists($upload_path)) {
	mkdir($upload_path, 0777, true);
}
*/
$FileUploader = new FileUploader('files', array(
	'uploadDir' => UPLOAD_PATH,
	'title' => date('Ymd').'_'.time().'_'.rand(1,1000)
));

$data = $FileUploader->upload();   // call to upload the files

if(isset($data['files'][0])) {
    $exif = @exif_read_data($data['files'][0]['file'], 0, true); 
}

if(isset($exif['EXIF']['DateTimeOriginal'])) {
    $data['date'] = date('Y-m-d', strtotime($exif['EXIF']['DateTimeOriginal']));
    $data['time'] = date('H:i', strtotime($exif['EXIF']['DateTimeOriginal']));
}

if(isset($exif['GPS']['GPSLongitude']) && isset($exif['GPS']['GPSLatitude'])) {
    $data['lon'] = getGps($exif['GPS']['GPSLongitude'], $exif['GPS']['GPSLongitudeRef']);
    $data['lat'] = getGps($exif['GPS']['GPSLatitude'], $exif['GPS']['GPSLatitudeRef']); 
}

echo json_encode($data);

function getGps($exifCoord, $hemi) {
    $degrees = count($exifCoord) > 0 ? gps2Num($exifCoord[0]) : 0;
    $minutes = count($exifCoord) > 1 ? gps2Num($exifCoord[1]) : 0;
    $seconds = count($exifCoord) > 2 ? gps2Num($exifCoord[2]) : 0;

    $flip = ($hemi == 'W' or $hemi == 'S') ? -1 : 1;

    return $flip * ($degrees + $minutes / 60 + $seconds / 3600);
}

function gps2Num($coordPart) {
    $parts = explode('/', $coordPart);

    if (count($parts) <= 0)
        return 0;

    if (count($parts) == 1)
        return $parts[0];

    return floatval($parts[0]) / floatval($parts[1]);
}

exit;
?> 