$(document).ready(function(){
    
    $('.useDatepicker').datepicker({
        language: 'lt',
        autoclose: true,
        todayHighlight:true,
        changeMonth: true,
        changeYear: true,
        format: 'yyyy-mm-dd'
    });
    
    $('.reportimage').uniform_thumbnails({
        fit: 'crop',
        align: 'bottom',
        format: 'square'
    })
    
    $("li.list-group-item").click(function(){
        $('li.list-group-item').removeClass('actual_problem');
        $(this).addClass('actual_problem');
    });
    
    $("a.disabled").click(function(e){
        e.preventDefault();
    });
    
     $(".showReporterInfo").click(function(){
        $(this).parent().next('.reporterInfoInerHide').slideToggle();
    });
    
    var slider = $('.bxslider').bxSlider({ 
        infiniteLoop :false,
        auto: true,
        autoHover: true
    });
    
    $('#cancelEditImg').click(function(){ 
        slider.reloadSlider();
    });

    var myElement = document.getElementById('mobileMenu');

    // create a simple instance
    // by default, it only adds horizontal recognizers
    var mc = new Hammer(myElement);

    // listen to events...
    mc.on("swipe", function(ev) {
    if(ev.type == 'swipe') {
        $('#mobileMenu').offcanvas('hide');
    }
    });

    $('.pagination>li>a.previous').on('click', function(e){
        e.preventDefault();
        if(!$('.pagination .previous').parent().next().hasClass('activePage')) {
           var page = $(".pagination>li.activePage>a").attr('href').match(/(page=)(\d+)/);
            $("#pageSelected").val(parseInt(page[2]) - 1);
            $("#searchForm").submit();
        } 
    });
    
    $('.pagination>li>a.next').on('click', function(e){
        e.preventDefault();
        if(!$('.pagination .next').parent().prev().hasClass('activePage')) {
            var page = $(".pagination>li.activePage>a").attr('href').match(/(page=)(\d+)/);
            $("#pageSelected").val(parseInt(page[2]) + 1);
            $("#searchForm").submit();
        } 
    });
    
    if($("#pageSelected").val() != 1 || $("#addressOrRegId").val() != '' ) {
        $('.right > .list-group > .reportshort:first-child').click();
    }
    
    var activeButtonsStatus = [];
        $(".activeSearchElementStatus").each(function(){
            activeButtonsStatus.push($(this).attr('name'));
        });
        $('#statusesId').val(activeButtonsStatus.join());
    
    $('.statuses').click(function(){
        if(!$(this).hasClass('activeSearchElementStatus'))
            $(this).addClass('activeSearchElementStatus');
        else 
            $(this).removeClass('activeSearchElementStatus');
        
        var activeButtonsStatus = [];
        $(".activeSearchElementStatus").each(function(){
            activeButtonsStatus.push($(this).attr('name'));
        });
        $('#statusesId').val(activeButtonsStatus.join());
    });
    
    var activeButtonsType = [];
        $(".activeSearchElementType").each(function(){
            activeButtonsType.push($(this).attr('name'));
        });
        $('#problemTypeId').val(activeButtonsType.join());
    
    $('.detailedSearch .problemTypes').click(function(){
        if(!$(this).hasClass('activeSearchElementType'))
            $(this).addClass('activeSearchElementType');
        else 
            $(this).removeClass('activeSearchElementType');
        
        var activeButtonsType = [];
        $(".activeSearchElementType").each(function(){
            activeButtonsType.push($(this).attr('name'));
        });
        $('#problemTypeId').val(activeButtonsType.join());
    });
    
    /* filterButton */
    $('.filterButton').click(function(){
        if($(this).hasClass('activeFilter') && !$(this).hasClass('mobileActiveSearch')) {
            $(this).removeClass('activeSearch');
            $(this).removeClass('activeFilter');
            $('.searchButton').css('visibility', 'visible');
            $('.detailedSearchIner').slideUp();
        } else if($(this).hasClass('activeFilter') && $(this).hasClass('mobileActiveSearch')) {
            $(this).removeClass('mobileActiveSearch');
            $('.detailedSearchIner').slideDown();      
        }  else {
            $(this).addClass('activeSearch');
            $(this).addClass('activeFilter');
            $('.searchButton').css('visibility', 'hidden');
            $('.detailedSearchIner').slideDown();
        }
    });

});

(function($, viewport){
    $(document).ready(function() {
        // Executes only in XS breakpoint
        if(viewport.is('xs')) {
            $('.filterButton').addClass('mobileActiveSearch');
            $('.detailedSearchIner').hide();
        }
    });
})(jQuery, ResponsiveBootstrapToolkit);
