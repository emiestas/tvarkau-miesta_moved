$.validator.addMethod("pickFromList",
    function(value, element, param) {
		var $otherElement = $(param);
		if($otherElement.val() == ''){
            return false;
        }
		else{
            return true;
        }
});
	
$.validator.addMethod("valueNotEquals", 
	function(value, element, arg){
		return arg != value;
});

$.validator.addMethod("municipalValidation",
    function(value, element, param) {
        var cities = param;
        cities = cities.replace(/\s/g, '');
        cities = cities.split(",");
        var i = 0;
        var checkCities = false;
        while (cities[i]) {
            if(value.indexOf(cities[i]) > -1) {
                checkCities = true;
                break;
            }
            i++;
        }
    return checkCities;
});

$.validator.addMethod("phoneNumberFormat",
    function(value, element) {
    if(value != '')
        {
            return value.match(/^[0-9+]{9,15}$/);
        }
    return true;
});

$.validator.addMethod("emailValidate",
    function(value, element) {
    if(value != '')
        {
            return value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
        }
    return true;
});

function newProblemValidate(cityname){
    var cities = cityname.replace(/\s/g, '');
    cities = cities.split(",");
    $('#problemForm').validate({
		ignore:":not(:visible)",
		//debug:true,
		errorElement: 'label',
		//errorClass: 'error help-inline',
		rules:{
			new_type: { 
				valueNotEquals: '0',
                required:true
			},
			new_subtype: { 
				valueNotEquals: '0',
                required:true
			},
			new_lat:{
				required:true
			},
			new_lng:{
				required:true
			},
			new_place:{
				required:true,
				municipalValidation: cityname
			},
			new_desc:{
				required:true
			},
			car_plate_no:{
				required:true
			},
			violation_date:{
				required:true
			},
			violation_time:{
				required:true
			},
			user_email:{
				emailValidate: true
			},
			user_phone:{
				 phoneNumberFormat: true
			}
		},
        messages:{
			new_type: {
                valueNotEquals: 'Prašome pasirinkti problemos tipą'
            },
			new_type: {
                valueNotEquals: 'Prašome pasirinkti problemos subtipą'
            },
			new_lat: {
                required: 'Prašome pasirinkti problemos vietą'
            },
			new_lng: {
                required: 'Prašome pasirinkti problemos vietą'
            },
			new_place: {
                required: 'Prašome įvesti problemos adresą',
				municipalValidation: 'Pasirinkite adresą ('+cities[0]+')'
            },
            new_desc: {
                required: 'Prašome aprašyti problemą'
            },
			car_plate_no: {
				required: 'Prašome įvesti automobilio valstybinį numerį!'
			},
			violation_date:{
				required: 'Prašome įvesti pažeidimo datą!',
				cantBeGreater: 'Data negali būti vėlesnė už šiandien!',
			},
			violation_time:{
				required: 'Prašome įvesti pažeidimo laiką!'
			},
            user_email:{
				emailValidate: 'Neteisingas elektroninio pašto adresas'
			},
            user_phone:{
				 phoneNumberFormat: 'Neteisingas telefono numeris'
			}   
        }
    });
}; 

function loginFormValidate(){
    $('#loginFormMain').validate({
		errorElement: 'label',
		//errorClass: 'error help-inline',
		rules:{
			username:{
				required:true
			},
			password:{
				required:true
			}
		},
        messages:{
			username: {
                required: 'Neįvestas vartotojo vardas!'
            },
			password: {
                required: 'Neįvestas slaptažodis!'
            }		
        }
    });
}; 

function editProblemValidate(){
    $('#editForm').validate({
		errorElement: 'label',
		rules:{
			edited_desc:{
				required:true
			}
		},
        messages:{
			edited_desc: {
                required: 'Pranešimas negali būti tuščias!'
            }	
        }
    });
}; 

/*$(document).ready(function(){
    $('#changeUserInfo').validate({
    rules:
    {
        changeEmail: 
        {
            emailValidate: true,
            maxlength: 60
        },
        changePhoneNumber: 
        {
            phoneNumberFormat: true
        },
        changePhoneAddress:
        {
            maxlength: 40,
            streetValidation: true
        }
    },
    messages:
    {
        changeEmail: 
        {
            emailValidate:'Neteisingas elektroninio pašto adresas',
            maxlength: 'Jūs viršijote simbolių limitą. Limitas yra 60 simbolių'
                
        },
        changePhoneNumber: 
        {
            phoneNumberFormat: 'Neteisingas telefono numeris',
        },
        changePhoneAddress: 
        {
            streetValidation:'Prašome nurodyti teisingą adresą',
            maxlength: 'Maksimalus simbolių skaičius negali viršyti 40'
                
        }
     },
            errorElement : 'span',
            errorLabelContainer: '.errors'
    });
});*/