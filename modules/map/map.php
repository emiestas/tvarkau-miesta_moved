<?php
include 'search.php';
$searchCriteria = array(
            'addressOrRegId'=> filter_var($_POST['addressOrRegId'], FILTER_SANITIZE_STRING),
            'dateFrom' => $_POST['dateFrom'],
            'dateTo' => $_POST['dateTo'],
            'problemStatus' => (!empty($_POST['statusesId']) && !isset($_POST['searchButton']) ? $_POST['statusesId'] : '1,2'),
            'problemType' => $_POST['problemTypeId'],
            'keyWord' => filter_var($_POST['keyWord'], FILTER_SANITIZE_STRING)
        );
$smarty->assign('problem_types' , getProblemTypes($searchCriteria, $selectedProblemTypeArray));
$smarty->assign('locations' , getLocations($searchCriteria));
$smarty->assign('page' , 'map');
$smarty->assign('selectedStatus' , explode(',', $searchCriteria['problemStatus']));
$content = $smarty->fetch($menu_front[$mid]['tpl']);
?>