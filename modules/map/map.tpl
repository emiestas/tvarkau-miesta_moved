<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
crossorigin=""></script>
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css">
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css">
<script src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
<div class="col-sm-12 right">
		{include file="templates/search.tpl"}
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mapSide">
	    <div id="map"></div>
        <div class="problemInformationMap navmenu-fixed-right offcanvas-lg" id="mapInformationBlock">
            <a href="#" class="closeRightPanel"><span class="fa fa-close"></span></a>
            <div class="problemInformationIner">
            </div>
        </div>
    </div>
</div>
{literal}
    <script>
        $(document).ready(function() {

            $('.closeRightPanel').click(function(e){
                e.preventDefault();
                $('#mapInformationBlock').offcanvas('hide');
            });
            
            var mapProblemDetails = document.getElementById('mapInformationBlock');

            // create a simple instance
            // by default, it only adds horizontal recognizers
            var mc = new Hammer(mapProblemDetails);

            // listen to events...
            mc.on("swipe", function(ev) {
                if(ev.type == 'swipe') {
                    $('#mapInformationBlock').offcanvas('hide');
                }
            });
            
        });

        var map = L.map('map').setView([{/literal}{$CITY_LAT}{literal}, {/literal}{$CITY_LNG}{literal}], 10);

        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            'attribution': 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
        }).addTo(map);

        var markers = L.markerClusterGroup({ showCoverageOnHover: false }).on('click', openWindow);
        var index;
        
        var data = {/literal}{$locations|json_encode}{literal}; 
             
        for (var key in data) {
            index = key;
            
            var marker = L.marker([parseFloat(data[key].koordLAT), parseFloat(data[key].koordLONG)]);
            marker.index = index;
            markers.addLayer(marker);
        }

        function openWindow(event) {
            var key = event.layer.index;

            var photos = '';
            if(data[key].problemPhotos != '') {
                photos+='<ul class="imageThumbnails">';
                data[key].problemPhotos.forEach(function(item) {
                    if(item['FILE_TYPE'] != 'pdf') {
                        photos += '<li class="mapReportImage"><a data-fancybox="group" href="{/literal}{$MINIO_SITE_URL}{literal}'+item['BUCKET']+'/'+item['SAVED_AS']+'"><img src="{/literal}{$MINIO_SITE_URL}{literal}'+item['BUCKET']+'/'+item['SAVED_AS']+'" alt="Gallery photo" /></a></li>';                                  
                    }
                });
                photos+='</ul>';
            }

            var answer = '';
            if(data[key].PROBLEM_ANSWER) {
                answer = '<p class="problemcategory labelMapProblem">Atsakymas <span class="completeDate">('+data[key].PROBLEM_CLOSE_DATE+')</span></p><p>'+data[key].PROBLEM_ANSWER+'</p>'; 
            }

            var problemInformation = '<div class="mapProblemIner"><div class="mapProblemTop"><p class="problemcategory pull-left">'+data[key].PROBLEM_TYPE+'</p><p class="reportdatatext pull-right">'+data[key].PROBLEM_AVILIO_NR+'</p></div><p class="problemDate pull-right">'+data[key].PROBLEM_DATE+'</p><div class="mapProblemTop"><span class="label label-primary" style="background:#'+data[key].PROBLEM_STATUS_COLOR+';">'+data[key].PROBLEM_STATUS_NAME+'</span><p class="problemAddress pull-right">'+data[key].PROBLEM_ADDRESS+'</p></div><p class="problemcategory labelMapProblem">Pranešimo tekstas</p><p class="mapProblemText">'+data[key].PROBLEM_DESC+'</p>'+photos+'<p class="problemcategory labelMapProblem">Atsakingas padalinys</p><p>'+data[key].PROBLEM_DEP_NAME+'</p>'+answer+'</div>';
            $('.problemInformationIner').html(problemInformation);
            $("#mapInformationBlock").offcanvas({ autohide: false, disableScrolling: false }).offcanvas('show');
            $('.imageThumbnails').bxSlider({
                infiniteLoop :false,
                autoHover: true
            }); 
        }

        map.addLayer(markers);

    </script>
{/literal}