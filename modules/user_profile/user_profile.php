<?php
if(!$_SESSION['AUTH']){
	header("Location: ".GLOBAL_SITE_URL);
}
if(isset($_POST['changeUserData'])) {
	$changeData['EMAIL'] = filter_var($_POST['changeEmail'], FILTER_SANITIZE_EMAIL);  
    $changeData['PHONE'] = filter_var($_POST['changePhoneNumber'], FILTER_SANITIZE_STRING);
    $changeData['ADDRESS'] = filter_var($_POST['changeAddress'], FILTER_SANITIZE_STRING);
    if(updateUserInfo($_SESSION['USER_ID'], $changeData, 'user_profile')){
		$message='<div class="span4 offset4 alert alert-success thanks-div">Jūsų duomenys sėkmingai atnaujinti</div>';	
    }
    else{
       $message='<div class="span4 offset4 alert alert-danger thanks-div">Klaida, duomenys nebuvo išsaugoti</div>';
    }
	$smarty->assign('message', $message);
}
$user_info=getUserInfo($_SESSION['USER_ID']);
$smarty->assign('user_name'    , filter_var($user_info['FULL_NAME'], FILTER_SANITIZE_STRING));
$smarty->assign('user_email'     , $user_info['EMAIL']);
$smarty->assign('user_phone'     , $user_info['PHONE']);
$smarty->assign('user_address'   , filter_var($user_info['ADDRESS'], FILTER_SANITIZE_STRING));
$smarty->assign('user_personal_code'   , $user_info['PERSONAL_CODE']);   
$smarty->assign('user_fb_id'     , $user_info['FACEBOOK_ID']);
$smarty->assign('user_g_id'     , $user_info['GOOGLE_ID']);

$content = $smarty->fetch($menu_front[$mid]['tpl']);
?>