<div class="col-lg-8 col-lg-offset-2 text-center">
	{if $message}
		{$message}
	{/if}
	<form method="POST">
	<h2>Mano duomenys</h2>
	<div class="row">
		<div class="col-md-3"><small class="pull-right form-text">Vardas pavardė</small></div>
		<div class="col-md-6 form-group"><input disabled class="form-control" name="" type="text" value="{$user_name}"></div>
	</div>
	<div class="row">
		<div class="col-md-3"><small class="pull-right form-text">El. paštas</small></div>
		<div class="col-md-6 form-group"><input class="form-control" name="changeEmail" type="text" value="{$user_email}"></div>
	</div>
	<div class="row">
		<div class="col-md-3"><small class="pull-right form-text">Gyvenamoji vieta</small></div>
		<div class="col-md-6 form-group"><input class="form-control" name="changeAddress" type="text" value="{$user_address}"></div>
	</div>
	<div class="row">
		<div class="col-md-3"><small class="pull-right form-text">Telefonas</small></div>
		<div class="col-md-6 form-group"><input class="form-control" name="changePhoneNumber" type="text" value="{$user_phone}"></div>
	</div>
	<div>
		<button type="submit" name="changeUserData" class="btn btn-primary">Išsaugoti pakeitimus</button>
	</div>
	</form>
<hr><hr>
{if $user_personal_code}
<div id="fbDiv">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
		<p><u>Facebook:</u></p>
		<button id="fbConnectButton" onclick="fbLogin();" class="hidden btn btn-primary">Prisijungti prie FB ir susieti paskyrą</button>
		{if $user_fb_id==''}
			<p>Nėra susietos Facebook paskyros</p>
			<button id="fbAddButton" onclick="checkLoginState('add');" class="btn btn-social btn-facebook"><span class="fa fa-facebook"></span>Susieti su Facebook paskyra: <span id="fbToAssoc"></span></button>
		{else}
			<button id="fbRemoveButton" onclick="checkLoginState('remove');" class="btn btn-social btn-facebook"><span class="fa fa-facebook"></span>Atsieti Facebook paskyrą:  <span id="fbAssoc"></span></button>
		{/if}
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">

	</div>
	<div class="col-lg-8 col-lg-offset-2 text-center">
<hr><hr>
	</div>
</div>
{/if}
</div>
{literal}
<script>
function fbLogin(){
	FB.login(function (response) {
        if (response.authResponse) {
			fbAssociate('add');
			fbDivReload();
        } 
	});
}

function fbDivReload(){
	$('#fbDiv').load(location.href + ' #fbDiv');
	checkLoginState();
}

function checkLoginState(action) {
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			fbAssociate();
			if(action=='add'){
				fbAssociate('add');
			}
			else if(action=='remove'){
				fbAssociate('remove');
			}
		}
		else{
			$('#fbAddButton').addClass('hidden');
			$('#fbRemoveButton').addClass('hidden');
			$('#fbConnectButton').removeClass('hidden');
		}	
	});
}
 
window.fbAsyncInit = function() {
    FB.init({
      appId      : '{/literal}{$FACEBOOK_APP_ID}{literal}',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
	checkLoginState();
    FB.AppEvents.logPageView();   
};

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

function fbAssociate(type) {
    FB.api('/me', function(response) {
		$('#fbAssoc').html(response.name);
		$('#fbToAssoc').html(response.name);
		if(type=='add' || type=='remove'){
			$.ajax({  
				url:"login.php",  
				method:"POST",  
				data: {fb_response_id:response.id,type:type},  
				success:function(data){ 
					if($.isNumeric(data)){
						if(confirm('Toks vartotojas duomenų bazėje jau yra. Ar norite apjungti vartotojus?')){
							fbAccJoin(response.id,data);
						}
					}
					else if ($.trim(data)){ 
						alert('Paskyra sėkmingai '+data);
						fbDivReload();
					}
				}  
			});
		}
	});
}  

function fbAccJoin(fb_id, user_id){
	$.ajax({  
		url:"login.php",  
		method:"POST",  
		data: {fb_response_id:fb_id,type:'join', user_to_join:user_id},  
		success:function(data){ 
			alert('Paskyros sėkmingai apjungtos');
			fbDivReload();
		}  
	});
}

function onSignIn(googleUser) {
	var profile = googleUser.getBasicProfile();
	var id_token = googleUser.getAuthResponse().id_token;
	console.log('Token: ' + id_token); 
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profile.getName());
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	gAccJoin(profile.getId());
}

function gAccJoin(gid, type){
	//alert('useris: '+gid);
	
}
</script>
{/literal}