<?php
if(!empty($_SESSION['USER_ID'])){
    $smarty->assign('user_personal_code' , $_SESSION['USER_CODE']);
	$smarty->assign('auth' , $_SESSION['AUTH']);
	$user_info=getUserInfo($_SESSION['USER_ID']);
	if(!empty($user_info['FULL_NAME'])) $smarty->assign('user_name' , filter_var($user_info['FULL_NAME'], FILTER_SANITIZE_STRING));
	if(!empty($user_info['EMAIL'])) $smarty->assign('user_email' , filter_var($user_info['EMAIL'], FILTER_SANITIZE_EMAIL));
	if(!empty($user_info['PHONE'])) $smarty->assign('user_phone' , filter_var($user_info['PHONE'], FILTER_SANITIZE_STRING));
	if(!empty($user_info['ADDRESS'])) $smarty->assign('user_address' , filter_var($user_info['ADDRESS'], FILTER_SANITIZE_STRING));   
}
$smarty->assign('upload_img', (!empty($upload_img))?$upload_img:'');
$smarty->assign('problem_types' , getProblemTypes());
$smarty->assign('GLOBAL_SITE_URL', GLOBAL_SITE_URL);
$smarty->assign('MODULE_IMAGES_URL', MODULE_IMAGES_URL);
$smarty->assign('ATMINTINE_URL', ATMINTINE_URL);

if(isset($_POST['submitNew'])){ 
	$new_type=(isset($_POST['new_subtype'])&&$_POST['new_subtype']>0)?$_POST['new_subtype']:$_POST['new_type'];
	$new_problem_arr=array();
	$new_problem_arr['new_type']=filter_var($new_type, FILTER_SANITIZE_NUMBER_INT); 
	$new_problem_arr['new_lat']=filter_var($_POST['new_lat'], FILTER_SANITIZE_STRING);
	$new_problem_arr['new_lng']=filter_var($_POST['new_lng'], FILTER_SANITIZE_STRING);
	$new_problem_arr['new_place']=str_replace(' &', ',', filter_var($_POST['new_place'], FILTER_SANITIZE_STRING));
	$new_problem_arr['car_plate_no']=filter_var($_POST['car_plate_no'], FILTER_SANITIZE_STRING);
	$new_problem_arr['violation_date']=filter_var($_POST['violation_date'], FILTER_SANITIZE_STRING);
	$new_problem_arr['violation_time']=filter_var($_POST['violation_time'], FILTER_SANITIZE_STRING);
	$new_problem_arr['new_desc']=filter_var($_POST['new_desc'], FILTER_SANITIZE_STRING);
	$new_problem_arr['foto']=$_POST['foto'];
	$new_problem_arr['user_name']=filter_var($_POST['user_name'], FILTER_SANITIZE_STRING);
	$new_problem_arr['user_email']=filter_var($_POST['user_email'], FILTER_SANITIZE_EMAIL);
	$new_problem_arr['user_address']=filter_var($_POST['user_address'], FILTER_SANITIZE_STRING);
	$new_problem_arr['user_phone']=filter_var($_POST['user_phone'], FILTER_SANITIZE_STRING);
	$problem_id=addProblem($new_problem_arr);
	$avilys_arr=addProblemAvilys($new_problem_arr, $problem_id);
	if(!empty($avilys_arr['registrationNo'])){
		linkProblemToAvilys($avilys_arr,$problem_id);  
	}
	$problem_data=getProblem($problem_id);
	if(!empty($_POST['user_email'])){
		$send=($new_problem_arr['user_email'])?sendMail(1,$new_problem_arr['user_email'],$avilys_arr['registrationNo'],'',$problem_data['data']['PROBLEM_TYPE'],$problem_id):
		array('userEmail'=>'-','subject'=>'-','body'=>'-');
    }
	else{
		$send=array('userEmail'=>'Tvarkau','subject'=>'pranešta anonimiškai','body'=>'-');
	}
	updateProblemLog($problem_id,1, $send);
    if(isset($_POST['updateProfileInfomation'])) {
        $changeData['EMAIL'] = filter_var($_POST['user_email'], FILTER_SANITIZE_EMAIL);
        $changeData['PHONE'] = filter_var($_POST['user_phone'], FILTER_SANITIZE_STRING);
        $changeData['ADDRESS'] = filter_var($_POST['user_address'], FILTER_SANITIZE_STRING);
        updateUserInfo($_SESSION['USER_ID'], $changeData);
    }
	$header_loc=(!$_SESSION['USER_ID'])?'problems_list':'user_problems_list';
	if(!empty($_POST['user_email']))
	   header("Location: /".$header_loc."?msg=2");
    else
		header("Location: /".$header_loc."?msg=3");
}
$content = $smarty->fetch($menu_front[$mid]['tpl']);
?>