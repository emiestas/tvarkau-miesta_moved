<div id="transportCanvas" class="transportCanvas row modal fade" role="dialog">
	<div class="transportCanvasInner">
		<div class="row pull-right close-cross" style="padding-right:5px;"><button type="button" class="close" data-dismiss="modal">×</button></div>
		<div class="text-center">
			<p>Pažeidimas turi būti užfiksuotas ne mažiau nei dviejose kokybiškose nuotraukose, iš kurių būtų galima spręsti apie administracinės teisės pažeidimo sudėtį ir galimybes taikyti administracinio poveikio priemones:</p>
			<p><img class="transportimg" src="images/transport_example_photo_1.png"></p>
			<p>Turi aiškiai matytis, kurioje vietoje stovi transporto priemonė, šalia esantis kelio ženklo ar ženklinimas, koks tiksliai KET pažeidimas padarytas</p>
			<p><img class="transportimg" src="images/transport_example_photo_2.png"></p>
			<p>Iš arti - automobilio valstybinis numeris, ir ar yra (nėra) palikta Neįgaliųjų asmenų automobilio statymo kortelė</p>
			<p>Išsamesnę informaciją su pavyzdžiais rasite čia: <a target="_blank" href="{$ATMINTINE_URL}"><img src="images/atsisiusti_pdf.png"> {$ATMINTINE_URL}</a></p>
            <p><button data-dismiss="modal" id="DontShow" class="btn btn-primary btn-sm">GERAI</button></p>
		</div>
	</div>
</div>

<div id="noPersonalCodeInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<p>Gerbiamas pranešėjau,</p>
				<p>Jūs esate prisijungęs su savo Facebook paskyra.</p>
				<p>Esant pagrįstam pranešimui neturėsime Jūsų, kaip liudytojo, asmens duomenų, kurie reikalingi administracinio nusižengimo nagrinėjimo procedūros pradėjimui.</p>
				<p>Norint registruoti pranešimus pagal šį pranešimo tipą reikia prisijungi per <a href="evartai.php" title="Elektroniniai valdžios vartai - prisijungti">e. valdžios vartus</a> arba su vilnius.lt vartotoju.</p>
				<p>Prisijungus aukščiau nurodytais būdais savo paskyrą galėsite susieti su Facebook.</p>
			</div>
			<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Uždaryti</button></div>
		</div>  
	</div>
</div>

<div id="attachmentWarning" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body"><p id="modalMsg" class="alert alert-danger"></p></div>
			<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Uždaryti</button></div>
		</div>  
	</div>
</div>

<div class="col-lg-8 col-lg-offset-2 text-center">
	{if $message}
		{$message}
	{else}
	<h2>Registruoti problemą</h2>
	<form method="POST" id="problemForm">
	<input type="hidden" id="cityname" name="{$cityname}">
	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-12">
			<textarea name="new_desc" id="new_desc" class="form-control" placeholder="Problemos aprašymas"></textarea>
		</div>
	</div>
	<div class="row" id="problemTypeSelect">
		<div class="form-group col-xs-12 col-sm-12 col-md-12 problemTypeSelectIner">
			<select name="new_type" id="problem_type" class="{if $user_personal_code == ''}no-personal-code{/if} selectpicker" data-show-icon="true" data-live-search="true" title="Pasirinkite problemos tipą">
				{foreach from=$problem_types key=key item=item}
					{if $item.PARENT_ID==0}
					<option {if !empty($item.KEYWORDS)}data-tokens="{$item.KEYWORDS}"{/if} {if $item.ATTACHMENT>0}data-attach="{$item.ATTACHMENT}"{/if} value="{$item.ID}" {if $item.SHOW_REGISTER==1}data-subtext="<i class='glyphicon glyphicon-lock'></i> (tik registruotiems vartotojams)"  {if $auth!=1}disabled{/if}{/if}>{$item.PROBLEM_TYPE}</option>
					{/if}
				{/foreach}
			</select>
		</div>
	</div>
	
	<!--hidden excl. Parking Violation -->
	<div class="row fillFormHidden hidden" id="car_plate">
		<div class="col-auto col-md-4">
            <div class="input-group mb-2 mb-sm-0">
                <div class="input-group-addon"><span class="fa fa-car"></span></div>
                <input name="car_plate_no" class="form-control" type="text" placeholder="Automobilio valst. nr.">
            </div>
        </div>
	</div>
	<div class="row fillFormHidden hidden" id="violation_time">
        <div class="col-auto col-md-3">
            <div class="input-group mb-2 mb-sm-0">
                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                <input name="violation_date" class="useDatepicker form-control" type="text" placeholder="Pažeidimo data" id="violation_datepicker" autocomplete="off">
            </div>
        </div>
        <div class="col-auto col-md-3">
            <div class="input-group mb-2 mb-sm-0">
                <div class="input-group-addon"><span class="fa fa-clock-o"></span></div>
                <input name="violation_time" class="form-control" type="text" placeholder="Pažeidimo laikas" id="violation_timepicker" autocomplete="off">
            </div>
        </div>
	</div>
	<!--hidden excl. Parking Violation End -->
	
    <input type="file" name="files">  <!-- file upload drop menu -->
	<div class="row" id="first_row">
		<div class="form-group col-xs-12 col-sm-12 col-md-12">
			<input id="new_place" name="new_place" class="form-control" data-geo="formatted_address" autocomplete="off" type="text" placeholder="Adresas">   
		</div>
	</div>
    <div class="alert alert-info">
        Pažymėkite problemos vietą žemėlapyje arba suveskite problemos adresą
    </div>
	<div id="map_canvas" class="googleiframe">
		<div id="map_new_problem"></div>
	</div>
	<input data-geo="lat" type="hidden" id="new_lat" name="new_lat">
	<input data-geo="lng" type="hidden" id="new_lng" name="new_lng">

    <div class="alert alert-warning">
	   Apie miesto problemos sprendimą informuosime el. paštu, todėl prašome jį įvesti. Kitus duomenis galite įvesti, dėl galimybės su Jumis susisiekti, prireikus patikslinti informaciją
    </div>
    <div class="senderInformationIner">
        <h3>Pranešėjo duomenys {if !empty($user_name) && !empty($user_email) && !empty($user_phone) && !empty($user_address) }<span class="fa fa-bars text-primary showReporterInfo" aria-hidden="true"></span>{/if}</h3>
        <div class="{if !empty($user_name) && !empty($user_email) && !empty($user_phone) && !empty($user_address) }reporterInfoInerHide{/if}">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <input id="user_name" name="user_name" class="form-control" type="text" value="{$user_name}" placeholder="Vardas pavardė" />
                    </div> 
                    <div class="form-group">
                        <input id="user_email" name="user_email" class="form-control" type="text" value="{$user_email}" placeholder="El. paštas" />
                    </div>
                    <div class="form-group">
                        <input id="user_address" name="user_address" class="form-control" type="text" value="{$user_address}" placeholder="Gyvenamoji vieta" />
                    </div>
                    <div class="form-group">
                        <input id="user_phone" name="user_phone" class="form-control" type="text" value="{$user_phone}" placeholder="Telefono numeris">
                    </div>
                </div>
            </div>
            <div id="transport_disclaimer" class="hidden">
                <p>Sutinku, kad pateikti duomenys yra mano, jie yra teisingi ir tikri.</p>
                <p>Jūsų asmeninė informacija yra reikalinga siekiant suteikti teisinį pagrindą Jūsų pranešimui. Duomenys bus matomi savivaldybės darbuotojų ir gali būti perduoti valdžios institucijoms, atsakingoms už administracinių baudų skyrimą.</p>
            </div>
            {if isset($user_personal_code)}
            <div class="checkbox">
              <label><input type="checkbox" value="1" checked name="updateProfileInfomation">Atnaujinti paskyros duomenis</label>
            </div>
            {/if}
        </div>
        <div class="actionButtons">
            <a href="{$GLOBAL_SITE_URL}" id="cancel" name="cancel" class="btn btn-danger">Atšaukti</a>
            <button type="submit" name="submitNew" id="newProblemSubmit" class="btn btn-primary">Pateikti problemą</button>
            <button class='btn btn-success registeringProblem' type='button' onclick='return false;'><span class="fa fa-spinner fa-spin"></span> Problema registruojama</button>
        </div>
    </div>
	</form>

<div class="modal fade registerProblem">
	<div class="loadingHead">Problema registruojama</div>
	<img class="svg loadingImage" src="../../images/loading{$CITY_NAME_EN}.svg">
</div>

	{/if}
</div>
{literal}
<script>
var map_new_problem; //Will contain map object.
var marker = false; ////Has the user plotted their location marker? 
var infoWindow;

function initMap() {
	var map_center = {lat: {/literal}{$CITY_LAT}{literal}, lng: {/literal}{$CITY_LNG}{literal}};
	map_new_problem = new google.maps.Map(document.getElementById('map_new_problem'), {
		zoom: 15,
		center: map_center
    });
    
	var input = document.getElementById('new_place');
    var autocomplete = new google.maps.places.Autocomplete(input);
    
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setComponentRestrictions({'country': 'LT'});
    
	infoWindow = new google.maps.InfoWindow;
	// Try HTML5 geolocation.
    if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
            };
            map_new_problem.setCenter(pos);
			
			//Create the marker.
            marker = new google.maps.Marker({
                position: pos,
                map: map_new_problem,
                draggable: true //make it draggable
            });
			markerLocation();   //Get the marker's location.
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event) {
                markerLocation();
            });
            $('body').on('click', '#geolocationIcon', function() {
                if($(this).css('background-position') == '-18px 50%') {
                    map_new_problem.setCenter(pos);
                }
            });
        }, function() {
            $('body').on('click', '#geolocationBtn', function() {
                handleLocationError(true, infoWindow, map_new_problem.getCenter());
            });
			handleLocationError(true, infoWindow, map_new_problem.getCenter());
          });
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map_new_problem.getCenter());
	}
	
    //Listen for any clicks on the map.
    google.maps.event.addListener(map_new_problem, 'click', function(event) {
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map_new_problem,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event) {
                markerLocation();
            });
        } else {
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        infoWindow.close();
        markerLocation();   //Get the marker's location.
	});
    
    autocomplete.addListener('place_changed', function() {
        infoWindow.close();
        if(marker !== false) {
            marker.setVisible(false);
        }
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            infoWindow.setPosition(map_new_problem.getCenter());
            infoWindow.setContent('Nepavyko rasti adreso pagal jūsų užklausą');
            infoWindow.open(map_new_problem);
        }

        //Create the marker.
        marker = new google.maps.Marker({
            position: place.geometry.location,
            map: map_new_problem,
            draggable: true //make it draggable
        });

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map_new_problem.fitBounds(place.geometry.viewport);
        } else {
            map_new_problem.setCenter(place.geometry.location);
            map_new_problem.setZoom(17); 
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
        
        $('#new_lat').val(autocomplete.getPlace().geometry.location.lat());
        $('#new_lng').val(autocomplete.getPlace().geometry.location.lng());
        
        geocodeLatLng(true);
    });
    
	if(document.getElementById('new_lat').value!=''){
		geocodeLatLng();
	}
    
    var geoloccontrol = new klokantech.GeolocationControl(map_new_problem, 17);
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
                              'Klaida: Jūsų įrenginyje neįgalinta geolokacija.' :
                              'Klaida: Jūsų naršyklė nepalaiko geolokacijos.');
	infoWindow.open(map_new_problem);
}

function geocodeLatLng(autocomplete = false) {
    var inputLat = document.getElementById('new_lat').value;   //      54.69759673325415
	var inputLng = document.getElementById('new_lng').value;   //      25.29052734375
	var latlng = {lat: parseFloat(inputLat), lng: parseFloat(inputLng)};
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
                if(autocomplete) {
                    var address_components = results[0].address_components;
                    var components={}; 
                    jQuery.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.short_name});});
                    var address = (components.premise !== undefined) ? (components.premise + ', ' + components.locality + ' ' + components.postal_code) : components.route + ' ' + ((components.street_number !== undefined) ? components.street_number + ', ' + components.locality + ' ' + components.postal_code : components.locality + ' ' + components.postal_code);
                    console.log(components);
                    console.log(components);
                    $('#new_place').val(address);
                } else {
    				var addr_expl = results[0].formatted_address.split(', ');
                    var cities = '{/literal}{$cityname}{literal}';
                    cities = cities.replace(/\s/g, '');
                    cities = cities.split(",");
                    var i = 0;
                    var checkCities = false;
                    while (cities[i]) {
                        if(results[0]['formatted_address'].indexOf(cities[i]) > -1 ) {
                            checkCities = true;
                            break;
                        }
                        i++;
                    }
    				if(checkCities){
    					document.getElementById('new_place').value=addr_expl[0]+', '+addr_expl[1];
    				}
    				else{
    					alert('Pasirinkote vietą, esančią už savivaldybės ribų!\n'+results[0]['formatted_address']);
    				}
                }
            } 
			else {
              alert('No results found');
            }
          } 
		  else {
            alert('Geocoder failed due to: ' + status);
          }
	});
}

function markerLocation() {
	if (marker == false) {
		alert('NULL');
	} else{
		var currentLocation = marker.getPosition();   //Get location.
	
        //Add lat and lng values to a field that we can save.
        document.getElementById('new_lat').value = currentLocation.lat(); //latitude
        document.getElementById('new_lng').value = currentLocation.lng(); //longitude
    	geocodeLatLng();
	}
}

$(document).ready(function(){
	$('#problem_type').change(function(){ 
		$.ajax({        // check if type has subtypes
			url: "index.php",  
			method: "POST",  
			data: {typeIDForSub: $(this).val()},  
			dataType: 'html',
			success:function(data){
				if ($.trim(data)){ 
					var subtypeHTML='<div class="row" id="problemSubtypeSelect">'+
						'<div class="form-group col-xs-12 col-sm-12 col-md-12 problemTypeSelectIner">'+
							'<select name="new_subtype" id="problem_subtype" class="selectpicker" data-show-icon="true" data-live-search="true" title="Pasirinkite problemos subtipą">';
					var res = $.parseJSON(data);
					$.each( res, function( key, value ) {
						subtypeHTML +="<option value="+value['ID']+">"+value['PROBLEM_TYPE']+"</option>";
					});
					subtypeHTML +="<option value="+res[0]['PARENT_ID']+"> - kita - </option>";
					subtypeHTML +='</select>'+
						'</div>'+
					'</div>';
					$('#problemTypeSelect').after(subtypeHTML);
					$('.selectpicker').selectpicker('refresh');
				}
				else{
					$('#problemSubtypeSelect').addClass('hidden');
				}
			}
		});	
		if($(this).val()=='20'){   // if problem_type 'Transporto ... pazeidimai' chosen
			if($(this).hasClass('no-personal-code')){
				$('#noPersonalCodeInfo').modal('show');
				$(this).prop('selectedIndex',0);
			} else {
                $(document).on("click", "#DontShow", function() {
                    localStorage.setItem("DontShow", "true");
                });
                if(!localStorage.getItem("DontShow")) {
				    $('#transportCanvas').modal('show');
                }
				$('#violation_time').removeClass('hidden');
				$('#car_plate').removeClass('hidden');
				$('#transport_disclaimer').removeClass('hidden');
			}
		}
		else{
			$('#car_plate').addClass('hidden');  //   better alternative to .hide() method
			$('#violation_time').addClass('hidden');			
		}
	});
	
	$('#newProblemSubmit').click(function(){   
		if($('#problem_type').val()=='20'){   // minimum of two photos in Parking violation check
			if($('.fileuploader-items-list').children().length<2){
				$("#modalMsg").html('Pažeidimas turi būti užfiksuotas ne mažiau nei dviejose kokybiškose nuotraukose!');
				$('#attachmentWarning').modal('show');
				return false;
			}
		}
		if($('#problem_type option:selected').attr('data-attach')=='1'){  // minimum photo where it's marked in problem_types table in DB
			if($('.fileuploader-items-list').children().length<1){
				$("#modalMsg").html('Turite pateikti bent vieną nuotrauką');
				$('#attachmentWarning').modal('show');
				return false;
			}
		}
		newProblemValidate($('#cityname').attr('name'));
        if($("#problemForm").valid()) {
        	$('.registerProblem').modal({backdrop: 'static',keyboard: false });
            $("#newProblemSubmit").hide();
            $(".registeringProblem").show();
        }

	});
	
	$('#violation_timepicker').wickedpicker({
		twentyFour: true
	});	
});
</script>
{/literal}
<script src="https://cdn.klokantech.com/maptilerlayer/v1/index.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={$GOOGLE_API_KEY}&callback=initMap&libraries=places&v=3.33"></script>
