<?php
$smarty->assign('problemPage', '/problems_list');
include 'search.php';
$total=getProblemCount($searchCriteria);
if($total>0){
    //PAGE_SIZE, $page, $page_count
    
	/* new pagination */
	if(isset( $_POST['pageSelected']) && (int)$_POST['pageSelected']) 
        $currentPage = $_POST['pageSelected'];
    else $currentPage = 1;
    
    $pageNumbersBetween = 2;
    $start = ( ( $currentPage - $pageNumbersBetween ) > 0 ) ? $currentPage - $pageNumbersBetween : 1;
    $last = ceil($total / PAGE_SIZE);
    $end = ( ( $currentPage + $pageNumbersBetween ) < $last ) ? $currentPage + $pageNumbersBetween : $last;
    $smarty->assign('currentPage' , $currentPage);
    $smarty->assign('start' , $start);
    $smarty->assign('last' , $last);
    $smarty->assign('end' , $end);
    
	$problems=getProblemList($currentPage, $searchCriteria);
	$last_problem_id=$problems[0]['PROBLEM_ID'];
	$actual_problem=($get_problem>0)?$get_problem:$last_problem_id;
	$last_problem=getProblem($actual_problem);
	if($last_problem==''){ //if registered problem doesn't exist
		$message='
		<div class="span4 offset4 alert alert-danger thanks-div">Tokios registruotos problemos nėra!</div>
		<div class="span4 offset4 alert alert-info">
			<a href="'.GLOBAL_SITE_URL.'" class="alert-link">Grįžti į pradinį puslapį</a>
		</div>';
		$smarty->assign('message', $message);
	}
	$smarty->assign('actual_problem' , $actual_problem);
	$smarty->assign('last_problem_data' , $last_problem['data']);
	$smarty->assign('last_problem_foto' , $last_problem['foto']);
	$smarty->assign('last_problem_answer' , $last_problem['answer_foto']);
    $smarty->assign('subscribed', checkIfSubscribed($actual_problem, $_SESSION['USER_ID']));
    $smarty->assign('isOwner', checkIfProblemOwnedByOwner($actual_problem, $_SESSION['USER_ID']));
	
    foreach($problems as $key => $value){
        $checkDescriptionLenght = strip_tags($value['PROBLEM_DESC']);
        if (strlen($checkDescriptionLenght) > 192) {
            $stringCut = substr($checkDescriptionLenght, 0, 192);
            $checkDescriptionLenght = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
        }
        
        $checkProblemTypeLenght = strip_tags($value['PROBLEM_TYPE']);
        if (strlen($checkProblemTypeLenght) > 60) {
            $stringCut = substr($checkProblemTypeLenght, 0, 60);
            $checkProblemTypeLenght = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
        }
        
        $problems[$key]['PROBLEM_DESC'] = $checkDescriptionLenght;
        $problems[$key]['PROBLEM_TYPE'] = $checkProblemTypeLenght;
        $problems[$key]['SUBSCRIBED'] = checkIfSubscribed($problems[$key]['PROBLEM_ID'], $_SESSION['USER_ID']);
    }
	$smarty->assign('IMG_DIR' ,MODULE_IMAGES_URL);
	$smarty->assign('problem_data'   ,$problems );
	$smarty->assign('voted', 'counter'.$actual_problem);
}
else{
	$message='
		<div class="span4 offset4 alert alert-danger thanks-div">Nerasta jokių rezultatų!</div>
		<div class="span4 offset4 alert alert-info">
			<a href="'.GLOBAL_SITE_URL.'" class="alert-link">Grįžti į pradinį puslapį</a>
		</div>';
	$smarty->assign('message', $message);	
}
if(isset($_GET['msg']) && $_GET['msg']==2){
	$message2='
		<div class="span4 offset4 alert alert-success alert-dismissable thanks-div">
			 <button type="button" class="close" aria-hidden="true">&times;</button>
			<span class="glyphicon glyphicon-ok"></span> Jūsų pranešimas užregistruotas. Jis bus išnagrinėtas ne ilgiau kaip per 20 d. d. Apie pranešimo sprendimo eigą informuosime el. paštu. Dėkojame!
		</div>';	
	$smarty->assign('message2', $message2);
}
if(isset($_GET['msg']) && $_GET['msg']==3){
	$message2='
		<div class="span4 offset4 alert alert-success alert-dismissable thanks-div">
			 <button type="button" class="close" aria-hidden="true">&times;</button>
			<span class="glyphicon glyphicon-ok"></span> Jūsų pranešimas užregistruotas. Jis bus išnagrinėtas ne ilgiau kaip per 20 d. d. Dėkojame!
		</div>';	
	$smarty->assign('message2', $message2);
}
$smarty->assign('GLOBAL_SITE_URL', GLOBAL_SITE_URL);
$content = $smarty->fetch($menu_front[$mid]['tpl']);
?>