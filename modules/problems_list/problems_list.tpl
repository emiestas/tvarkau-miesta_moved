<!-- pagination, left and right blocks -->
<div class="col-sm-12 right">
		{include file="templates/search.tpl"}
</div>
{if $message}
	<div class="col-lg-8 col-lg-offset-2 text-center">
	{$message}
	</div>
{else}
	{if $message2}
	<div class="col-lg-8 col-lg-offset-2 text-center">
		{$message2}
	</div>
	{/if}
<div class="col-sm-12 col-md-6 left showingProblemIner">
	<div class="row" id="ProblemDiv">
		{include file="templates/last_problem.tpl"}
	</div>
</div>
<div class="col-sm-12 col-md-6 right">
	<ul class="list-group">
	{foreach from=$problem_data key=key item=item}
		<!--onclick="location.href='?problem_id={$item.PROBLEM_ID}';"-->
		<li class="list-group-item reportshort {if $item.PROBLEM_ID==$actual_problem}actual_problem{/if}" data-value="{$item.PROBLEM_ID}">
			<div class="row">
                <span class="reportdatatext regnumber pull-right">{if $item.VERSION != 1}<span class="fa fa-mobile"></span>{/if} {if $item.PROBLEM_AVILIO_NR}{$item.PROBLEM_AVILIO_NR}{else}{$item.PROBLEM_ID}{/if}</span>
				<div class="col-xs-6 col-md-6 col-md-6 reportimage" title="Žiūrėti išsamiau">
					<img alt="" src="{$item.THUMBNAIL}" class="img-rounded">
				</div>
				<div class="col-xs-4 col-sm-6 col-md-6 report-text" title="Žiūrėti išsamiau">
					<span class="problemcategory">{$item.PROBLEM_TYPE}</span>
					<div class="report-description">{$item.PROBLEM_DESC}</div>
					<div class="report-text-bottom">
						<span class="label label-primary" style="background:#{$item.PROBLEM_STATUS_COLOR};">{$item.PROBLEM_STATUS_NAME}</span>
						<span class="reportdatatext address">{$item.PROBLEM_ADDRESS}</span>
					</div>
				</div>
                {if $item.SUBSCRIBED}
                    <span class="reportdatatext time subscribed pull-left" title="Nustoti stebėti pranešimą"><i class="fa fa-heart"></i> Pranešimas stebimas</span>
				{/if}
                <span class="reportdatatext time pull-right">{$item.PROBLEM_DATE}</span>
			</div>
		</li>		
	{/foreach}
	</ul>
</div>
<div class="col-sm-12 right">
	{include file="templates/paging.tpl"}
</div>
{literal}
<script>
var getUrlPart =  window.location.pathname;

$(".alert button.close").click(function (e) {
    $(this).parent().fadeOut('slow');
});

$(".alert-dismissable").click(function (e) {
    $(this).fadeOut('slow');
});

$('body').on('click', '.subscribed', function() {
    var problemID = $(this).closest('.list-group-item').attr('data-value');
    if(typeof problemID == 'undefined') { problemID = $('input[name=problem_id]').val(); }
    $('.registerProblem').modal({backdrop: 'static', keyboard: false });
    $.ajax({  
        url: 'index.php',  
        method: 'POST',  
        data: {
            unsubscribe: true,
            problem_id: problemID,
            {/literal}{if $smarty.session.USER_ID}{literal}
            user_id: {/literal}{$smarty.session.USER_ID}{literal}
            {/literal}{/if}{literal}
        },
        success: function() {
            sessionStorage.setItem('problemID', problemID);
            $("#searchForm").submit();
        },
        error: function() {
            $('.registerProblem').modal("hide");
        }  
    });
});

$('body').on('click', '.list-group-item', function(data, handler) {
    if(data.target.className != 'reportdatatext time subscribed pull-left') {
        var problemID = $(this).attr('data-value');
    	if(problemID > 0) { 
    		$.ajax({  
    			url: "index.php",  
    			method: "POST",  
    			data: { problemID: problemID },  
    			dataType: 'html',
    			success: function(data) {
    				$('#ProblemDiv').html(data);
    				history.pushState(null, null, window.location.origin + '/problem/' + problemID);
    				$('html, body').animate(
    					{scrollTop: $('#ProblemDiv').offset().top }, 
    					'slow');
                    $('.bxslider').bxSlider({infiniteLoop: false, auto: true});
                    
    			}  
    		});
    	} else {  
    		alert('Įvyko klaida');
    	}
    }
});

if(sessionStorage.getItem('problemID')) {
    setTimeout(function() {
        var problemID = sessionStorage.getItem('problemID');
        $.ajax({
            url: "index.php",  
            method: "POST",  
            data: { problemID: parseInt(problemID) },  
            dataType: 'html',
            success: function(data) {
                $('#ProblemDiv').html(data);
                
                history.pushState(null, null, window.location.origin + '/problem/' + problemID);
                $('html, body').animate(
                    {scrollTop: $('#ProblemDiv').offset().top }, 
                    'slow');
                $('.bxslider').bxSlider({infiniteLoop: false, auto: true});
                sessionStorage.removeItem('problemID');
                
            }  
        });
    }, 1000);
}

/* pagination */
$('.pagination>li>a.pageLink').on('click', function(e){
    e.preventDefault();
    var page = $(this).attr('href').match(/(page=)(\d+)/);
    $("#pageSelected").val(page[2]);
    history.pushState(null, null, window.location.origin + '{/literal}{$problemPage}{literal}');
    $("#searchForm").submit();
});
    
$('#detailSearchButton, .searchButton').on('click', function(){
    history.pushState(null, null, window.location.origin + '{/literal}{$problemPage}{literal}');
});
    
</script>
{/literal}
{/if}