<?php
ini_set('session.cookie_httponly', 1);
ini_set('session.cookie_secure', 1);  
session_start();
set_include_path(dirname(dirname(__FILE__)));
include_once("configuration/config.php");
$city_list=getActiveCities();
foreach($city_list as $city){
	$_SESSION['CITY_ID']=$city['ID'];
	$arr['search_status']=2; // where status - Vykdoma
	$total=getProblemCount($arr, '', $city['ID']);
	echo '<br>Iš viso: '.$total;
	$data=getProblemList(0, $arr, '', $city['ID']);
	foreach($data as $d){
		echo '<br>------------------------------------<p># '.$d['PROBLEM_ID'].' - '.$d['PROBLEM_AVILIO_NR'].' - '.$d['PROBLEM_AVILIO_OID'].' ';
		if($d['PROBLEM_AVILIO_NR']!=''){   // if problem registered in Avilys
			$getStatus=getProblemDataAvilys($d['PROBLEM_AVILIO_NR'],$d['PROBLEM_AVILIO_OID'],$city['ID']);
			if(empty($d['PROBLEM_AVILIO_OID'])){
				updateAvilysTable($d['PROBLEM_AVILIO_NR'],$getStatus[2]);
			}
			echo '<span style="color:red">'.$getStatus[1].'</span>';
			if($getStatus[1]=='completed'){
				$getResult=getResultAvilys($d['PROBLEM_AVILIO_NR'], $city['ID']);
				if(!empty($getResult)){
					if(updateProblem($d['PROBLEM_ID'],'STATUS_ID',3, $getResult)>0){
						$problem_data=getProblem($d['PROBLEM_ID']);
						//echo '<p>Problem data: </p>';print_r($problem_data);
						if(!empty($problem_data['data']['REPORTER_EMAIL'])){
							$email=$problem_data['data']['REPORTER_EMAIL'];
						}
						else{
							$user_info = getUserInfo($problem_data['data']['PROBLEM_USER']);
							$email=$user_info['EMAIL'];
						}
						$send=($email!='')?sendMail(3,$email,$problem_data['data']['PROBLEM_AVILIO_NR'],$problem_data['data']['PROBLEM_ANSWER'],$problem_data['data']['PROBLEM_TYPE'],$d['PROBLEM_ID']):
						array('userEmail'=>'-','subject'=>'-','body'=>'-'); 
						updateProblemLog($d['PROBLEM_ID'],3, $send);
						echo '<br><br><span style="color:green">Pakeista!</span>';
					}
				}
				else{
					echo '<br><br><span style="color:red">Kazkas blogai su rezultato nuskaitymu!</span>';
				}
			}
		}
		echo '</p>';
	}
}
?>