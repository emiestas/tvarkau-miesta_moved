<?php
set_include_path(dirname(dirname(__FILE__)));
include_once("class/Database.php");
include_once("configuration/dbconfig.php");
include_once("class/myFunctions.php");

// Post deletion from database from all tables after X days
define('DELETE_AFTER_X_DAYS', 365);

define('MINIO_PATH', '/var/www/html/minio/files/');

$db = new MysqliDb (Array (
    'host' => PROBLEMOS_HOST,
    'username' => PROBLEMOS_USER, 
    'password' => PROBLEMOS_PASS,
    'db'=> PROBLEMOS_DB,
    'prefix' => PREFIX,
    'charset' => 'utf8'
));

$cols = array('PROBLEM_ID', 'DELETION_DATE');
$results = $db->get('REG_PROBLEMS_DELETED', null, $cols);

foreach($results as $result) {
    if(date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime($result['DELETION_DATE']. ' + '.DELETE_AFTER_X_DAYS.' days'))) {
        $problemID = $result['PROBLEM_ID'];

        $db->where('ID', $problemID);
        $city_id = $db->getValue('REG_PROBLEMS', 'CITY_ID');

        $pageCfg = getPageConfig($city_id);
        $minio_bucket_name = 'tvarkau.'.$pageCfg['CITY_SYSTEM_NAME'].'.'.date("Y");

        $db->where('PROBLEM_ID', $problemID);
        $results = $db->get('REG_FILES', null, 'SAVED_AS');

        foreach($results as $result) {
            unlink(MINIO_PATH.$minio_bucket_name.'/'.$result['SAVED_AS']);
            unlink(MINIO_PATH.$minio_bucket_name.'.thumbs/'.$result['SAVED_AS']);
    
            if(file_exists(MINIO_PATH.$minio_bucket_name.'.edited/'.$result['SAVED_AS'])) {
                unlink(MINIO_PATH.$minio_bucket_name.'.edited/'.$result['SAVED_AS']);
                unlink(MINIO_PATH.$minio_bucket_name.'.edited.thumbs/'.$result['SAVED_AS']); 
            }
        }

        $db->where('PROBLEM_ID', $problemID);
        $db->delete('REG_AVILYS');
        $db->where('PROBLEM_ID', $problemID);
        $db->delete('REG_AVILYS_TX_LOGS');
        $db->where('PROBLEM_ID', $problemID);
        $db->delete('REG_FILES');
        $db->where('PROBLEM_ID', $problemID);
        $db->delete('REG_FILES_ANSWER');
        $db->where('PROBLEM_ID', $problemID);
        $db->delete('REG_PROBLEMS_DELETED');
        $db->where('PROBLEM_ID', $problemID);
        $db->delete('STATUS_LOGS');
        $db->where('PROBLEM_ID', $problemID);
        $db->delete('SUBSCRIBED_PROBLEMS');
        $db->where('ID', $problemID);
        $db->delete('REG_PROBLEMS');
    }
}