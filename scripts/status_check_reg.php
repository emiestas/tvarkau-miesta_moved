<?php
ini_set('session.cookie_httponly', 1);
ini_set('session.cookie_secure', 1);  
session_start();
set_include_path(dirname(dirname(__FILE__)));
include_once("configuration/config.php");
$city_list=getActiveCities();
foreach($city_list as $city){
	$_SESSION['CITY_ID']=$city['ID'];	
	$arr['search_status']=1; // where status - Registruota
	$total=getProblemCount($arr, '', $city['ID']);
	echo '<br>Iš viso: '.$total;
	$data=getProblemList(0,$arr, '', $city['ID']);
	foreach($data as $d){ 
		echo '<p># '.$d['PROBLEM_ID'].' - '.$d['PROBLEM_AVILIO_NR'].' - '.$d['PROBLEM_AVILIO_OID'].' ';
		if($d['PROBLEM_AVILIO_NR']!=''){   // if problem registered in Avilys
			$getStatus=getProblemDataAvilys($d['PROBLEM_AVILIO_NR'],$d['PROBLEM_AVILIO_OID'],$city['ID']);
			if(empty($d['PROBLEM_AVILIO_OID'])){
				updateAvilysTable($d['PROBLEM_AVILIO_NR'],$getStatus[2]);
			}
			echo '<span style="color:red">'.$getStatus[1].'</span>';
			if($getStatus[1]=='completed'){
				$getResult=getResultAvilys($d['PROBLEM_AVILIO_NR'], $city['ID']);
				echo '<h2>';
				print_r($getResult);
				echo '</h2>';
				if(!$getResult){
					updateProblem($d['PROBLEM_ID'],'STATUS_ID',3);
					echo '<span style="color:blue"><b>TUSCIAS ATSAKYMAS, problema tusciai ATLIKTA!</b></span>';	
				}
				else{
					updateProblem($d['PROBLEM_ID'],'STATUS_ID',3, $getResult);
					$problem_data=getProblem($d['PROBLEM_ID']);
					if(!empty($problem_data['data']['REPORTER_EMAIL'])){
						$email=$problem_data['data']['REPORTER_EMAIL'];
					}
					else{
						$user_info = getUserInfo($problem_data['data']['PROBLEM_USER']);
						$email=$user_info['EMAIL'];
					}
					$send=($email!='')?sendMail(3,$email,$problem_data['data']['PROBLEM_AVILIO_NR'],$problem_data['data']['PROBLEM_ANSWER'],$problem_data['data']['PROBLEM_TYPE'],$d['PROBLEM_ID']):
					array('userEmail'=>'-','subject'=>'-','body'=>'-'); 
					updateProblemLog($d['PROBLEM_ID'],3, $send);
					echo '<span style="color:green">Pakeista i ATLIKTA!</span>';
				}
			}
			if($getStatus[1]=='inExecution'){
				updateProblem($d['PROBLEM_ID'],'STATUS_ID',2);
				$problem_data=getProblem($d['PROBLEM_ID']);
				print_r($problem_data);
				echo '<p>REPORTER_EMAIL: '.$problem_data['data']['REPORTER_EMAIL'].'</p>';
				if(!empty($problem_data['data']['REPORTER_EMAIL'])){
					$email=$problem_data['data']['REPORTER_EMAIL'];
				}
				else{
					$user_info = getUserInfo($problem_data['data']['PROBLEM_USER']);
					$email=$user_info['EMAIL'];	
				}
				echo '<p>'.$email.'</p>';
				$send=($email!='')?sendMail(2,$email,$problem_data['data']['PROBLEM_AVILIO_NR'],'',$d['PROBLEM_TYPE'],$d['PROBLEM_ID']):array('userEmail'=>'-','subject'=>'-','body'=>'-'); 
				updateProblemLog($d['PROBLEM_ID'],2, $send);
				echo '<span style="color:green">Pakeista i VYKDOMA!</span>';
			}
		}
		else{   // if problem NOT registered in Avilys
			//if((strtotime(date('Y-m-d H:i:s'))-strtotime($d['PROBLEM_DATE']))>600){ // if 10min past from db registration, to avoid problem dublicates when registered from app
				echo 'neregistruota Avilyje<br>';
				$getProblem=getProblem($d['PROBLEM_ID']);
				$arr['foto']=array_column($getProblem['foto'], 'SAVED_AS');
				$arr['new_type']=$d['PROBLEM_TYPE_ID'];
				$arr['new_desc']=filter_var($d['PROBLEM_DESC'], FILTER_SANITIZE_STRING);
				$arr['new_place']=str_replace(' &', ',', filter_var($d['PROBLEM_ADDRESS'], FILTER_SANITIZE_STRING));		
				$arr['user_id']=$d['PROBLEM_USER'];
				$arr['reporter_name']=filter_var($getProblem['data']['REPORTER_NAME'], FILTER_SANITIZE_STRING);     
				$arr['reporter_email']=filter_var($getProblem['data']['REPORTER_EMAIL'], FILTER_SANITIZE_EMAIL);
				$arr['personal_data']=filter_var($getProblem['data']['REPORTER_BIRTHDATE'], FILTER_SANITIZE_STRING);
				$avilys_arr=addProblemAvilys($arr, $d['PROBLEM_ID'], 1, $city['ID']);
				echo '<p>$avilys_arr: ';
				print_r($avilys_arr);
				echo '</p>';
				if($avilys_arr['registrationNo']!=''){  // only if successfully registered in Avilys
					linkProblemToAvilys($avilys_arr,$d['PROBLEM_ID']);
					if(!empty($getProblem['data']['REPORTER_EMAIL'])){
						$email=$getProblem['data']['REPORTER_EMAIL'];
					}
					else{
						$user_info=getUserInfo($d['PROBLEM_USER']);
						$email=$user_info['EMAIL'];
					}
					$send=($email!='')?sendMail(1,$email,$avilys_arr['registrationNo'],'',$d['PROBLEM_TYPE'],$d['PROBLEM_ID']):array('userEmail'=>'-','subject'=>'-','body'=>'-'); 
					updateProblemLog($d['PROBLEM_ID'],1, $send);
				}
			//}
			//else{
			//	echo 'per greitai bandoma registruoti<br>';
			//}
		}
		echo '</p>';
	}
}
?>