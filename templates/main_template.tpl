{include file="templates/header.tpl"}
{$sentry_tracker}
<div class="container">
	<div class="row navigation">
		<div class="col-md-12 mobilemenu">
			{include file="templates/menu1.tpl"}
		</div>
	</div>
	<div class="row body maincontent">
		<div class="col-md-12">
			<!-- row of desktop menu items -->
			<div class="row mainMenuIner">{include file="templates/menu2.tpl"}</div>
			  <!-- end of row of desktop menu items -->
			 <div class="row mainContentIner">
				{if $error}
					<div class="col-lg-8 col-lg-offset-2 text-center">{$error}</div>
				{else}	
					{$content}
				{/if}
			</div>            
		</div>
	</div>
    {include file="templates/footer.tpl"}
</div>
{include file="templates/login_form.tpl"}
<!-- scripts -->
<script src="{$LIB_SITE_URL}libs/bootsrapDatePicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="{$LIB_SITE_URL}libs/bootsrapDatePicker/dist/locales/bootstrap-datepicker.lt.min.js"></script>
<script src="{$LIB_SITE_URL}libs/bxslider-4-master/dist/jquery.bxslider.min.js?v=4.2.15"></script>
<script src="{$LIB_SITE_URL}libs/fancybox-master/dist/jquery.fancybox.min.js"></script>
<script src="{$LIB_SITE_URL}libs/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
<script src="{$LIB_SITE_URL}libs/uniform-thumbnails/dist/jquery.uniform_thumbnails.min.js"></script>
<script src="{$LIB_SITE_URL}libs/hammer.min.js"></script>
<script src="{$LIB_SITE_URL}libs/responsive-bootstrap-toolkit-master/dist/bootstrap-toolkit.min.js"></script>
<script src="js/myjs.js"></script>
<script src="js/image-picker.min.js"></script>
</body>
</html>