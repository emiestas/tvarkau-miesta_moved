<!DOCTYPE HTML>
<html lang="lt-LT">
	<head>
		<title>{$module_title} {$CITY_LABEL}</title>
		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width = device-width, initial-scale = 1">
		<base href="{$GLOBAL_SITE_URL}">
		<link rel="icon" type="image/x-icon" href="favicon.ico">
		<script src="{$LIB_SITE_URL}libs/jquery/jquery-min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
		<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
		<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
		<link rel="stylesheet" type="text/css" href="{$LIB_SITE_URL}libs/bootsrapDatePicker/dist/css/bootstrap-datepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="{$LIB_SITE_URL}libs/bootstrap-social/bootstrap-social.css" />
		<script src="{$LIB_SITE_URL}libs/jqueryUI/jquery-ui.min.js"></script>	
		<script src="{$LIB_SITE_URL}libs/jqueryValidate/jquery.validate.min.js"></script>
		<script src="{$LIB_SITE_URL}libs/datepicker-lt.js"></script>
		<link href="{$LIB_SITE_URL}libs/jqueryUI/jquery-ui.min.css" media="all" rel="stylesheet">
		<script src="js/validate.js"></script>
		<link href="{$LIB_SITE_URL}fileuploader/jquery.fileuploader.css" media="all" rel="stylesheet">
		<link href="{$LIB_SITE_URL}fileuploader/jquery.fileuploader-theme-dragdrop.css" media="all" rel="stylesheet">
		<script src="{$LIB_SITE_URL}fileuploader/jquery.fileuploader.min.js" type="text/javascript"></script>
		<script src="{$LIB_SITE_URL}fileuploader/custom.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
		<script src="{$LIB_SITE_URL}libs/wickedPickerJs/wickedpicker.min.js"></script> 
		<link rel="stylesheet" href="{$LIB_SITE_URL}libs/wickedPickerJs/wickedpicker.css">
		<link rel="stylesheet" href="styles/search.css">
		<link rel="stylesheet" type="text/css" href="{$LIB_SITE_URL}libs/bxslider-4-master/dist/jquery.bxslider.min.css?v=4.2.15" />
		<link rel="stylesheet" type="text/css" href="{$LIB_SITE_URL}libs/fancybox-master/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" type="text/css" href="{$LIB_SITE_URL}libs/jasny-bootstrap/css/jasny-bootstrap.min.css" />
        <script src="//cdn.trackduck.com/toolbar/prod/td.js" async data-trackduck-id="59e0758eeafb7e4c618a082b" data-trackduck-lang="lt"></script>
		<meta name="google-signin-client_id" content="{$GOOGLE_CLIENT_ID}">
        <link rel="stylesheet" href="styles/style.css">
        <link rel="stylesheet" href="styles/image-picker.css">
		<meta property="og:site_name" content="Tvarkau miestą">
		<meta property="og:title" content="Tvarkau miestą">
		<meta property="og:description" content="Pranešk apie problemą savo mieste">
		<meta property="og:image" content="{$CITY_LOGO}">
		 {literal}
    <script>
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '{/literal}{$FACEBOOK_APP_ID}{literal}',
			cookie     : true,
			xfbml      : true,
			version    : 'v2.8'
		});
		//FB.getLoginStatus(function(response) {
			//if (response.status === 'connected') {
			//	fbConnect('check');
			//} 
		//});
		FB.AppEvents.logPageView();   
	};

	(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	  </script>
	   {/literal}
	   	   
	</head>
	<body onload="fbLoginCheck()">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108252534-1"></script>
    {literal}
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-108252534-1');
    </script>
    {/literal}
	<style>
        .navigation, .footer .row  {
           background-color: #{$SITE_MAIN_COLOR};
        }
        .nav .open>a, .nav .open>a:focus, .nav .open>a:hover, .submitSearch, 
        .submitSearch:focus, .submitSearch:active, .submitSearch:visited, 
        .pagination > li.active > .page-link {
            background-color:#{$SITE_MAIN_COLOR};
            border-color:#{$SITE_MAIN_COLOR};
        }
        .submitSearch:hover, .activePage a{
            background-color:#{$SITE_MAIN_COLOR} !important;
            color:#fff !important;
        }
        
        .activePage > span {
            background-color:#{$SITE_MAIN_COLOR} !important;
            color:#fff !important;
        }
        
        .navmenu-default, .navbar-default .navbar-offcanvas {
            background-color: #{$SITE_MAIN_COLOR};
            border-color: #{$SITE_MAIN_COLOR};
        }
            
        .current-menu-item, .home-page {
            background-color:  #{$SITE_SECOND_COLOR} !important;
        }

        .current-menu-item span, .current-menu-item a, .home-page a, 
        .home-page span, .desktopmenuitem:hover span {
             color:  #{$SITE_MAIN_COLOR} !important;
        }
        
        .activeSearch, .activeSearch:active, .activeSearch:focus, .activeSearch:visited, 
        .searchButton:hover, .filterButton:hover, .activeSearch.focus{
            background-color: #{$SITE_MAIN_COLOR} !important;
            border: 1px solid #{$SITE_MAIN_COLOR} !important;
        }

	</style>