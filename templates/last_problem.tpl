<script>
var problem_location_LAT = {$last_problem_data['koordLAT']};
var problem_location_LNG = {$last_problem_data['koordLONG']};
</script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
crossorigin=""></script>
<div class="modal fade" id="deleteProblem" tabindex="-1" role="dialog" aria-labelledby="deleteProblemLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Uždaryti"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteProblemLabel">Ar tikrai norite ištrinti pranešimą?</h4>
            </div>
            <form method="POST">
                <input type="hidden" name="problem_id" value="{$last_problem_data['PROBLEM_ID']}">
        		<input type="hidden" name="problem_oid" value="{$last_problem_data['PROBLEM_AVILIO_OID']}">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Priežastis:</label>
                        <textarea rows="3" class="form-control" name="reason"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Uždaryti</button>
                    <button type="submit" id="deleteProblemBtn" name="deleteProblem" class="btn btn-danger">Trinti</button>
                    <button class='btn btn-danger registeringProblem disabled' type='button' onclick='return false;'><span class="fa fa-spinner fa-spin"></span> Pranešimas trinamas</button>
                </div>
            </form>
        </div>
    </div>
</div>
{assign var=pictures value=['jpg','jpeg','png','gif']}
<div class="col-md-12 mainreport">
	<div class="row col-md-12 last_problem">
		<div class="col-md-12 last_problem_top">
			<span class="problemcategory pull-left">{$last_problem_data['PROBLEM_TYPE']}</span>
			<span class="reportdatatext pull-right">{if $last_problem_data['VERSION']!=1}<span class="fa fa-mobile"></span>{/if} {if $last_problem_data['PROBLEM_AVILIO_NR']}{$last_problem_data['PROBLEM_AVILIO_NR']}{else}{$last_problem_data['PROBLEM_ID']}{/if}</span>
		</div>
		<div class="col-md-12 last_problem_top">
			<span class="reportdatatext pull-right">{$last_problem_data['PROBLEM_DATE']}</span>
		</div>
		<div class="col-md-12 last_problem_top">
			<span class="label label-primary" style="background:#{$last_problem_data['PROBLEM_STATUS_COLOR']};">{$last_problem_data['PROBLEM_STATUS_NAME']}</span>
			<span class="reportdatatext adress pull-right">{$last_problem_data['PROBLEM_ADDRESS']}</span>
		</div>
	</div>
    <form method="POST" id="editForm">
	<div class="col-md-12">
        <div class="problemTextIner">
            <p class="reportfulltext problemFullText">{$last_problem_data['PROBLEM_DESC']}</p>
        </div>
	</div>
	{if is_array($SESSION)}
	{if $last_problem_data['PROBLEM_USER']==$SESSION['USER_ID'] && $last_problem_data['PROBLEM_STATUS'] < 3}
	<div class="col-md-12 hidden" id="editProblemDiv">
		<input type="hidden" name="edit_problem_id" value="{$last_problem_data['PROBLEM_ID']}">
		<input type="hidden" name="edit_problem_oid" value="{$last_problem_data['PROBLEM_AVILIO_OID']}">
		<div class="pull-right">
            <button type="button" name="cancelEditProblem" id="cancelEditProblem" class="btn btn-danger">Atšaukti</button>
			<button type="submit" name="editProblemSubmit" id="editProblemSubmit" class="btn btn-success">Pateikti</button>
		</div>
	</div>
	<div class="col-md-12 last_problem actionButton">	
		<button type="button" id="editProblemButton" name="edit_problem" class="btn btn-default" title="Redaguoti">
            <span class="glyphicon glyphicon-pencil"></span>
        </button>
        <button type="button" id="deleteProblem" name="deleteProblem" data-toggle="modal" data-target="#deleteProblem" class="btn btn-default{if $last_problem_data['PROBLEM_STATUS'] > 1} disabled{/if}" title="{if $last_problem_data['PROBLEM_STATUS'] > 1}Pranešimą ištrinti leidžiama tik kol jis nėra perduotas vykdymui{else}Ištrinti pranešimą{/if}">
            <span class="glyphicon glyphicon-trash"></span>
        </button>
        <button type="button" id="addImg" name="addImg" class="btn btn-default" title="Pridėti nuotrauką-(as)">
            <span style="color:green;" class="glyphicon glyphicon-plus"></span>
        </button>
        {if $last_problem_foto}
            <button type="button" id="deleteImg" name="deleteImg" class="btn btn-default" title="Ištrinti nuotrauką-(as)">
                <span style="color: red;" class="glyphicon glyphicon-ban-circle"></span>
            </button>
        {/if}
	</div>
	{/if}
	{/if}
    </form>
	<div class="col-md-12 last_problem">
        {if is_array($SESSION)}
    	{if $last_problem_data['PROBLEM_USER']==$SESSION['USER_ID'] && $last_problem_data['PROBLEM_STATUS'] < 3}
        <form method="POST">
		    <input type="hidden" name="problem_id" value="{$last_problem_data['PROBLEM_ID']}">
            <input type="hidden" name="problem_oid" value="{$last_problem_data['PROBLEM_AVILIO_OID']}">
            <input type="hidden" name="desc" value="{$last_problem_data['PROBLEM_DESC']}">
            {if $last_problem_foto}
                {foreach from=$last_problem_foto key=k item=i}
                    <input type="hidden" name="imgs[]" value="{$i['SAVED_AS']}" />
                {/foreach}
    		{/if}
            <div id="editImgDiv" style="margin-top: -20px; margin-bottom: 10px;" class="pull-right hidden">
                <button type="button" name="cancelEditImg" id="cancelEditImg" class="btn btn-danger">Atšaukti</button>
    			<button type="submit" name="editImgSubmit" id="editImgSubmit" class="btn btn-success">Pateikti</button>
    		</div>
            <div id="first_row" style="clear: both;"></div>
            <div id="files" class="hidden">
                <input type="file" name="files">
            </div>
        {/if}
    	{/if}
		{if $last_problem_foto}
		<ul class="bxslider">
				{foreach from=$last_problem_foto key=k item=i}
                <li>
                        <a href="{$MINIO_SITE_URL}{$i['BUCKET']}{if $i['EDITED']==1}.edited{/if}/{$i['SAVED_AS']}" {if $i['FILE_TYPE']|in_array:$pictures}data-fancybox="group"{else}target="_blank"{/if}>
                        <img src="{if $i['FILE_TYPE']|in_array:$pictures}{$MINIO_SITE_URL}{$i['BUCKET']}{if $i['EDITED']==1}.edited{/if}/{$i['SAVED_AS']}{else if $i['FILE_TYPE']=='pdf'}images/pdf_image.png{else}images/file_image.png{/if}" data-id="{$i['ID']}" alt="">
                        </a>
                </li>
				{/foreach}
		</ul>
        {if is_array($SESSION)}
    	{if $last_problem_data['PROBLEM_USER']==$SESSION['USER_ID'] && $last_problem_data['PROBLEM_STATUS'] < 3}
        </form>
        {/if}
    	{/if}
		{/if}
	</div>

	<div class="col-md-12 last_problem">
		<div class="report-dep">
			<span class="problemcategory">Atsakingas padalinys</span>
			<p>{$last_problem_data['PROBLEM_DEP_NAME']}</p>
		</div>
	</div>
{if $last_problem_data['PROBLEM_ANSWER']}
	<div class="col-md-12 last_problem">
		<div>
			<span class="problemcategory">Atsakymas</span>		
			<span class="reportdatatext time pull-right">{$last_problem_data['PROBLEM_CLOSE_DATE']}</span>
		</div>
		<div class="reportAsnwerIner">
			<span class="">{$last_problem_data['PROBLEM_ANSWER']}</span>	
		</div>
		{if $last_problem_answer}
			<p class="reportanswer problemcategory">Prisegtos bylos:</p>
			{foreach from=$last_problem_answer key=k item=i}
			<div class="col-md-12 row last_problem" style="">
				<div class="col-md-2 pull-left">
					<a href="{$MINIO_SITE_URL}{$i['BUCKET']}/{$i['SAVED_AS']}" target="_blank" title="Atsisiųsti"><img alt="" src="images/file_image_small.png" class="img-answer"></a>
				</div>
				<div>
					<span class=""><a href="{$MINIO_SITE_URL}{$i['BUCKET']}/{$i['SAVED_AS']}" target="_blank" title="Atsisiųsti">{$i['SAVED_AS']|substr:25}</a></span>
				</div>
			</div>
			{/foreach}	
		{/if}
	</div>
{/if}
	<div class="col-md-12 googleiframe last_problem">
		<div id="map-box" class="map-last-problem"></div>
	</div>	
	
    <div class="row">
        <div class="col-md-3 {if $smarty.cookies.$voted}like-inactive{/if}" id="like-dislike">
            <span class="glyphicon glyphicon-thumbs-up like-push"></span><span class="like-count" style="">{$last_problem_data['NUM_LIKE']}</span>
            <span class="glyphicon glyphicon-thumbs-down like-push"></span><span class="dislike-count" style="">{$last_problem_data['NUM_DISLIKE']}</span>
        </div>
        <div class="col-md-9 {if $smarty.cookies.$voted}like-inactive{/if}" id="like-dislike">
            <div id="share" class="jssocials pull-right"></div>
        </div>
    </div>
    {if !$isOwner}
        <div class="row">
            <div class="col-md-12">
                <span class="{if !$subscribed}follow{else}subscribed{/if}{if !$SESSION} disabled{/if}" {if !$SESSION}title="Stebėti pranešimus galit tik prisijungę!" {/if}><i class="fa fa-heart{if !$subscribed}-o{/if} fa-2x"></i> {if $subscribed}Pranešimas stebimas{else}Stebėti pranešimą{/if}</span>
            </div>
        </div>
    {/if}
</div>
<div class="modal fade registerProblem">
	<img class="svg loadingImage" src="../../images/loading{$CITY_NAME_EN}.svg">
</div>

{literal}
<script>
    var map = L.map('map-box').setView([problem_location_LAT, problem_location_LNG], 15);

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        'attribution': 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
    }).addTo(map);

    L.marker([problem_location_LAT, problem_location_LNG]).addTo(map);

    $('#editProblemButton').click(function(){ 
        editProblemValidate();
        $('#editProblemDiv').removeClass('hidden');
        $('#files').addClass('hidden');
        $('#editProblemButton').addClass('hidden');
        $('#deleteProblem').addClass('hidden');
        $('#addImg').addClass('hidden');
        $('#deleteImg').addClass('hidden');
        $("p.problemFullText").wrapInner('<textarea name="edit_desc" id="problemTextEditTextarea" class="form-control" style="height:150px;"></textarea>');
    });
        
    $('#cancelEditProblem').click(function(){ 
        $('#problemTextEditTextarea').contents().unwrap();
        $('#files').addClass('hidden');
        $('#editProblemDiv').addClass('hidden');
        $('#editProblemButton').removeClass('hidden');
        $('#deleteProblem').removeClass('hidden');
        $('#addImg').removeClass('hidden');
        $('#deleteImg').removeClass('hidden');
        $('.image-picker').remove();
        $('.thumbnails').remove();
    });

    $('#cancelEditImg').click(function(){ 
        $('#editImgDiv').addClass('hidden');
        $('#files').addClass('hidden');
        $('#editProblemButton').removeClass('hidden');
        $('#deleteProblem').removeClass('hidden');
        $('#addImg').removeClass('hidden');
        $('#deleteImg').removeClass('hidden');
        $('.image-picker').remove();
        $('.thumbnails').remove();
    });

    $('#addImg').click(function(){ 
        $('#editImgDiv').removeClass('hidden');
        $('#files').removeClass('hidden');
        $('#editProblemButton').addClass('hidden');
        $('#deleteProblem').addClass('hidden');
        $('#addImg').addClass('hidden');
        $('#deleteImg').addClass('hidden');
        $('.bx-wrapper').fadeOut();
    });

    $('#deleteImg').click(function(){ 
        $('#editImgDiv').removeClass('hidden');
        $('#files').addClass('hidden');
        $('#editProblemButton').addClass('hidden');
        $('#deleteProblem').addClass('hidden');
        $('#addImg').addClass('hidden');
        $('#deleteImg').addClass('hidden');
        $('.bx-wrapper').fadeOut();
        $('.bx-wrapper').parent().append('<select multiple="multiple" name="img[]" class="image-picker show-html"></select>');
        $('.bxslider li a img').each(function(index, element) {
            $('.image-picker').append('<option data-img-src=' + $(element).attr('src') + ' value="' + $(element).data('id') + '">' + $(element).data('id') + '</option>');
        });
        $(".image-picker").imagepicker();
        $('.thumbnail').uniform_thumbnails({
            fit: 'crop',
            align: 'middle',
            format: 'square',
        });
        $('.thumbnail').each(function( index ) {
            $(this).parent().wrapInner('<div class="col-md-4"></div>');
        });
    });

    $('#deleteProblemBtn').click(function() {
        $('#deleteProblemBtn').hide();
        $('.registeringProblem').show();
    })

    $('body').on('click', '.follow', function() {
        if(!$(this).hasClass('disabled')) {
            var problem_id = $('input[name=problem_id]').val();
            $('.registerProblem').modal({backdrop: 'static', keyboard: false });
            $.ajax({
                type: "POST",
                url: "index.php",
                data: {
                    follow: true,
                    problem_id: problem_id,
                    {/literal}{if $smarty.session.USER_ID}{literal}
                    user_id: {/literal}{$smarty.session.USER_ID}{literal}
                    {/literal}{/if}{literal}
                },
                success: function() {
                    sessionStorage.setItem('problemID', problem_id);
                    $("#searchForm").submit();
                },
                error: function() {
                    $('.registerProblem').modal("hide");
                }
            });
        }
    });

    if(!$('#like-dislike').hasClass('like-inactive')){
        //$('.like-push').click(function(){
        $('.like-push').one("click",function(){
            var classUsed=$(this).attr('class').split(' ')[1];
            //alert(classUsed);
            $.ajax({
                type: "POST",
                url: "index.php",
                data: {
                    like_push: classUsed,
                    problem_id: {/literal}{$last_problem_data['PROBLEM_ID']}{literal}
                },
                success: function(data){
                    var res = $.parseJSON(data);
                    $('.like-count').html(res['like']);
                    $('.dislike-count').html(res['dislike']);
                    $('#like-dislike').addClass('like-inactive');
                }, 
                error: function(err, status) { 
                    console.log(err);
                }
            })
        });
    }
    
    $("#share").jsSocials({
        shares: [{ share: "email", label: "El. p." }, { share: "facebook", label: "Dalintis" }, "googleplus", "twitter"],
        url: '{/literal}{$GLOBAL_SITE_URL}{literal}problem/{/literal}{$last_problem_data['PROBLEM_ID']}{literal}',
        text: "noriu pasidalinti - Tvarkau miestą",
        showCount: false,
        showLabel: function(screenWidth) {
            return (screenWidth > 350);
        }
    });

    $("#deleteProblem").on("click", function (event) {         
        if ($(this).hasClass("disabled")) {
            event.stopPropagation()
        }
    });

</script>
{/literal}