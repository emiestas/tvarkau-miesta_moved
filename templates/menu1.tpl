<span class="logo pull-left">
    <a href="{$GLOBAL_SITE_URL}">
        <img class="desktopmenuimage" src="images/{$LOGO_IMAGE}" height="42"/>
    </a>
</span>
{if $SESSION}
<p class="user_logged"><span class="fa fa-sign-out"></span><a title="Atsijungti" href="logout">Atsijungti</a></p>
{/if}
<div class="navbar-default hidden-md hidden-lg mainMenuNavigation">
    <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
<div class="citySelect">
    <p>Tvarkau</p>
    <div class="dropdown">
      <button class="btn-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        {$CITY_LABEL}
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        {foreach from=$activeCity key=key item=item}
            <li {if $CITY_ID == {$item.ID}}class='activeCity'{/if}>
                <a href="?city={$item.ID}">{$item.CITY_LABEL}</a>
            </li>
        {/foreach}
      </ul>
    </div>
</div>
<div class="navmenu navmenu-default navmenu-fixed-right offcanvas-lg" id="mobileMenu">
      <ul class="nav navmenu-nav">
        {foreach from=$menu key=key item=item}
	        {if $item.visible && $item.group!='user'}
            <li {if $item.currentMenu == 1} class="current-menu-item" {elseif $currentMenuIsset=='/' && $item.index_page == 1 || $currentMenuIsset|strpos:"city" != false && $item.index_page == 1} class="home-page" {/if}>
                <a {if $item.private==1 && !$SESSION}data-toggle="modal" data-target="#loginModal" data-id="{$item.link}" class="needToLogin"{else} href="{$item.link}"{/if} title="{$item.name}">
                    <span class="fa fa-{$item.icon}"></span> {$item.name}
                </a>
            </li>
            {/if}
        {/foreach}
      </ul>
      <ul class="nav navmenu-nav userOptions">
        {if $SESSION}
        {foreach from=$menu key=key item=item}
	        {if $item.visible && $item.group=='user'}
            <li {if $item.currentMenu == 1} class="current-menu-item"{/if}>
               <a {if $item.private==1 && !$SESSION}data-toggle="modal" data-target="#loginModal" data-id="{$item.link}" class="needToLogin"{else} href="{$item.link}"{/if} title="{$item.name}">
                     <span class="fa fa-{$item.icon}"></span> {$item.name}
                </a>
            </li>
            {/if}
        {/foreach}
        {else}
          <li><a class="loginLink" {if !$SESSION}data-toggle="modal" data-target="#loginModal"{/if}><span class="fa fa-sign-in"></span> Prisijungti</a></li>
        {/if}
      </ul>
    </div>