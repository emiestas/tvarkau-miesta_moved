﻿<div class="col-md-6 pagination justify-content-start">
<div class="col-md-12 searchinput">
<form method="post" id="searchForm">
    <div class="mainSearchIner">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <input type="text" class="form-control" name="addressOrRegId" id="addressOrRegId" value="{$addressOrRegId}" autocomplete="off" placeholder='Ieškoti pagal adresą arba registracijos numerį' />
            <span class="input-group-btn">
                <button type="button" class="btn btn-default filterButton {$detailSearchIsActive}" name="filterButton"><span class="fa fa-filter"></span></button>
                <button type="submit" class="btn btn-default searchButton" name="searchButton"><span class="fa fa-search"></span></button>
            </span>
            <a id="search-clear" href="" class="fa fa-times-circle" aria-hidden="true"><span class="sr-only">Išvalyti paiešką</span></a>
        </div>
    </div>
    <input type="hidden" id="pageSelected" name="pageSelected" value="{if empty($currentPage)}1{else}{$currentPage}{/if}" />
</div>
</div>
{if !$message && $page != 'map'}
	<div class="col-md-6 reportpagination justify-content-center">
		{include file="templates/paging.tpl"}
	</div>
{/if}
<div class="col-md-12 detailedSearchIner">
<div class="row">
    <div class="detailedSearch">
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 statusesIner">
            <label class="blockTitle">Būsena</label>
            {foreach from=$problem_statuses item=type}
                <button type="button" name="{$type.ID}" class="btn btn-default selectButtons statuses {if $type.ID|in_array:$selectedStatus}activeSearchElementStatus{/if}">{$type.STATUS}</button>
            {/foreach}
            <input type="hidden" name="statusesId" id="statusesId" />
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 problemTypeIner">
            <label class="blockTitle">Problemos tipas</label>
            {foreach from=$problem_types key=key item=item}
            <button type="button" name="{$item.ID}" class="btn btn-default selectButtons problemTypes {if $item.ID|in_array:$selectedSType}activeSearchElementType{/if}">{$item.SHORT_LABEL}{if $item.ID|in_array:$selectedSType}<span class="countProblems">{$item.problemsCount}</span>{/if}</button>
			{/foreach}
            <input type="hidden" name="problemTypeId" id="problemTypeId" />
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 dateIner">
            <label class="blockTitle">Laikotarpis</label>
            <div class="col-md-12 input-group">
                <input type="text" id="dateFrom" name="dateFrom" class="form-control useDatepicker" placeholder="Data nuo" value="{$dateFrom}">
                <label class="input-group-addon btn" for="dateFrom">
                    <span class="fa fa-calendar"></span>
                </label>                    
            </div>
            <div class="col-md-12 input-group">
                <input type="text" id="dateTo" name="dateTo" class="form-control useDatepicker" placeholder="Data iki" value="{$dateTo}">
                <label class="input-group-addon btn" for="dateTo">
                    <span class="fa fa-calendar"></span>
                </label>                    
            </div>
            <label class="blockTitle">Raktažodis</label>
            <input type="text" class="form-control" name="keyWord" id="keyWord" value="{$keyWord}" autocomplete="off" placeholder='Raktažodis' />
            <div class="detailActionButtonsIner">
                <button type="submit" class="btn btn-success detailActionButtons" name="detailSearchButton" id="detailSearchButton"><span class="fa fa-search"></span> Ieškoti</button>
                <button type="button" class="btn btn-danger cancelDetailSearch detailActionButtons"><span class="fa fa-ban"></span> Atšaukti</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>
{literal}
<script>
    if($('.filterButton').hasClass('activeSearch')) {
        $('.searchButton').css('visibility', 'hidden');
        $('.detailedSearchIner').show();
    }
    
    $(document).ready(function(){
        /* search */
        $("#addressOrRegId").keyup(function(){
            if($(this).val().length > 0) {
                $('button.searchButton').addClass('activeSearch');
            } else {
                $('button.searchButton').removeClass('activeSearch');
            }
        });

        if($('#addressOrRegId').val().length > 0) {
            $('button.searchButton').addClass('activeSearch');
            $('#search-clear').show();
        }

        $('#search-clear, .cancelDetailSearch').click(function(e){
            e.preventDefault();
            history.pushState(null, null, window.location.origin + '{/literal}{$problemPage}{literal}');
            window.location.href = window.location.href;
        });

        $('.searchButton, #detailSearchButton').click(function(e){
            $("#pageSelected").val(1);
        });
    });
</script>
{/literal}