{foreach from=$menu key=key item=item}
	{if $item.visible && $item.sys_name!='logout'}
        <span id="desctopmenu">
            <a {if $item.private==1 && !$SESSION}data-toggle="modal" data-target="#loginModal" data-id="{$item.link}" class="needToLogin"{else} href="{$item.link}"{/if} title="{$item.name}">
            <div class="col-sm-2 desktopmenuitem {if $item.currentMenu == 1} current-menu-item {elseif $currentMenuIsset=='/' && $item.index_page == 1 || $currentMenuIsset|strpos:"city" != false && $item.index_page == 1} home-page {/if}">
                <span class="fa fa-{$item.icon}"></span>
                <h4 class="card-title">{$item.name}</h4>
            </div>
            </a>
        </span>
	{/if}
{/foreach}