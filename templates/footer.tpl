<div class="footer">
    <div class="row">
        <p class="copyright">© {$smarty.now|date_format:"%Y"}, Tvarkau miestą</p>
        <p class="phone"><a href="tel:{$PHONE_NUMBER}" class="phoneNumber"><span class="fa fa-phone" aria-hidden="true"></span> <span class="contactInfoText">{$PHONE_NUMBER}</span></a> <a href="mailto:{$EMAIL}"><span class="fa fa-envelope-o" aria-hidden="true"></span> <span class="contactInfoText">{$EMAIL}</span></a></p>
    </div>
</div>