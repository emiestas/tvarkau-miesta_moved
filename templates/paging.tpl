<div class="paginationInner">
    <ul class="pagination">
        {if $currentPage != 1}
        <li class="previousLink hidden-xs"><a href="#" class="previous"><span class="fa fa-angle-left"></span></a></li>
        {/if}
        
        {if $start > 1}
        <li><a href="index.php?page=1" class="pageLink">1</a></li> 
        <li class="inactivePageStart"><span>...</span></li>
        {/if}
        {for $foo=$start to $end}
        <li class="pageLinkList{if $foo == $currentPage} activePage{/if}{if $foo == $start && $start!=1} hidden-xs{/if}{if $foo == $end && $end != $last} hidden-xs{/if}"><a class="pageLink" href="index.php?page={$foo}">{$foo}</a></li>
        {/for}
        
        {if $end < $last}
        <li class="inactivePageEnd"><span>...</span></li>
        <li><a href="index.php?page={$last}" class="pageLink">{$last}</a></li> 
        {/if}

        {if $currentPage != $last}
        <li class="nextLink hidden-xs"><a href="#" class="next"><span class="fa fa-angle-right"></span></a></li>
        {/if}
    </ul>
</div>