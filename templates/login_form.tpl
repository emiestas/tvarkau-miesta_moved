<link rel="stylesheet" type="text/css" href="styles/login.css" />
<!-- Modal -->
<div id="loginModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<h3 class="title">Prisijungti</h3>
					<p>Prisijunkite Jums patogiu būdu</p>
					<ul class="socialLogin">
						<li><a id="fbLoginLink" title="Prisijungti Facebook"><img src="../images/facebook-icon.png" alt="Facebook"/></li>
						<li><a id="googleLoginLink" class="disabled hidden" title="Prisijungti Google+"><img src="../images/googlePlus.png" alt="Google Plus" /></a></li> 
						<li><a href="evartai.php" title="Elektroniniai valdžios vartai"><img width="50" src="../images/evartai.svg" alt="Evartai" /></a></li>
					</ul>
					<p>Arba su vilnius.lt vartotoju</p>
					<div class="loginError"></div>
					<form method="post" id="loginFormMain">
						<input type="hidden" value="{$pagename}" name="directionLink" id="directionLink"/>
						<div class="form-group">
							<input type="email" class="form-control" id="username" name="username" required placeholder="El. paštas">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="password" name="password" required placeholder="Slaptažodis">
						</div>
						<button class="btn btn-primary btn-login" type="submit" name="loginButton" id="loginButton">Prisijungti</button>
					</form>
					<br />
					<p>Neturite vilnius.lt vartotojo? <a target="_blank" href="{$NEW_USER_LINK}" class="ember-view btn btn-sm btn-default">Susikurti</a></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 anonymousIner">
					<h3 class="title">Pranešti anonimiškai</h3>
					<div class="informationIner">
						<p><span class="glyphicon glyphicon-remove"></span> Negalėsite stebėti savo registruotų pranešimų</p>
						<p><span class="glyphicon glyphicon-remove"></span> Negalėsite pranešti apie viešos tvarkos pažeidimus</p>
						<p><span class="glyphicon glyphicon-remove"></span> Negalėsite patikslinti savo pranešimo</p>
					</div>
					<button type="button" class="btn btn-primary btn-login" id="anonymSubmit">Pranešti</button>
				</div>
			</div>
		</div>
    </div>
	<!-- Modal content end-->
	</div>
</div>
<div id="googleModal" class="modal fade" role="dialog">
	<div style="" id="my-signin2"></div>
</div>
{literal}
<script>
function onSuccess(googleUser) {
	console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
	googleConnect( googleUser.getBasicProfile().getId(), googleUser.getBasicProfile().getName(),googleUser.getBasicProfile().getEmail())	
}
function googleConnect(user_id, user_name, user_email) {
	//var directionLink = $('#directionLink').val();
	var directionLink = 'new_problem';
	//console.log(JSON.stringify(response));
	$.ajax({  
		url:"login.php",  
		method:"POST",  
		data: {g_response_id:user_id, g_name:user_name, g_email:user_email, directionLink:directionLink},  
		success:function(data){   
			console.log(data);
			if ($.trim(data)){ 
				var res = $.parseJSON(data);
				if(res[0]==1) {  
					location.href=res[1]; 					
				}
			} 
		} 
	});
}
function onFailure(error) {
	console.log(error);
}
function renderGoogleButton() {
	gapi.signin2.render('my-signin2', {
        'scope': 'profile email',
        'width': 45,
        'height': 45,
        //'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
	});
}

$('#googleLoginLink').click(function(e){
	//console.log('ggg');
	//googleConnect(googleUser.getBasicProfile().getId(),googleUser.getBasicProfile().getName());
}); 	
	
$('a[data-toggle=modal]').click(function () {
	var data_id=$(this).data('id');
	$('#directionLink').val(data_id); 
}); 

$('#anonymSubmit').click(function(e){ 
	//location.href=$('#directionLink').val(); 
	location.href='new_problem';
});  

/*
$("#loginModal").on("shown.bs.modal", function () { 
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '{/literal}{$FACEBOOK_APP_ID}{literal}',
			cookie     : true,
			xfbml      : true,
			version    : 'v2.8'
		});
		//FB.getLoginStatus(function(response) {
			//if (response.status === 'connected') {
			//	fbConnect('check');
			//} 
		//});
		FB.AppEvents.logPageView();   
	};

	(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
})
*/

function fbConnect(type) {
    FB.api('/me', {fields: 'name, id, email' },function(response) {
		var directionLink = $('#directionLink').val();
		$.ajax({  
			url:"login.php",  
			method:"POST",  
			data: {
				fb_response_id:response.id, 
				type:type, 
				directionLink:directionLink, 
				fb_name:response.name, 
				fb_email:response.email
			},  
			success:function(data){   
				if ($.trim(data)){ 
					var res = $.parseJSON(data);
					if(res[0]==1) {  
						location.href=res[1]; 					
					}
					else if(res[0]==3){
						$('#fbLoginLink').removeClass('disabled');
						$('#fbLoginLink').prop('title', 'Jungtis kaip '+response.name);			  
					}
				}
			}  
		});
	});
}	

function fbLoginCheck(){
	var paramsLocation=window.location.toString().indexOf('#access_token=');
	if (paramsLocation>=0){
		FB.getLoginStatus(function(response) {
			var response_id=response.authResponse.userID;
			var fb_name; var fb_email;
			FB.api('/me', {fields: 'name, email'}, function(response) {
			
			////*****************
			$.ajax({  
				url:"login.php",  
				method:"POST",  
				data: {
					fb_response_id:response_id, 
					type:'login', 
					directionLink:'new_problem', 
					fb_name: response.name, 
					fb_email: response.email
				},  
				success:function(data){   
					console.log(data);
					if ($.trim(data)){ 
						var res = $.parseJSON(data);
						if(res[0]==1) {  
							location.href=res[1]; 					
						}
						else if(res[0]==3){
							$('#fbLoginLink').removeClass('disabled');
							$('#fbLoginLink').prop('title', 'Jungtis kaip '+response.name);			  
						}
					}
				}  
			});
		//*******************************
			});
		
		});
	}
}

$('#fbLoginLink').click(function(e){
	var uri = encodeURI('{/literal}{$GLOBAL_SITE_URL}{literal}');
	FB.getLoginStatus(function(response) {
		console.log(response.status);
		if (response.status === 'connected') {
			fbConnect('login');
		} 
		else {
			window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id={/literal}{$FACEBOOK_APP_ID}{literal}&redirect_uri="+uri+"&response_type=token");	 
		}
	});
});  

$('#loginButton').click(function(e){ 
	loginFormValidate();
	var username = $('#username').val();  
	var password = $('#password').val();  
	var directionLink = $('#directionLink').val(); 
	if(username != '' && password != '') {  
		$.ajax({  
			url:"login.php",  
			method:"POST",  
			data: {username:username, password:password, directionLink:directionLink}, 
			success:function(data){  
				var res = $.parseJSON(data);
				if(res[0]==1) {  
					location.href=res[1]; 					
				}
				else if(res[0]==2){
					$('.loginError').html('<span>'+res[1]+'</span>');
					$('.loginError').addClass('alert');
					$('.loginError').addClass('alert-danger');			  
				}
			}  
		});
		e.preventDefault(); //STOP default action
	}  
});  
</script>
{/literal}