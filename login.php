<?php
ini_set('session.cookie_httponly', 1); 
ini_set('session.cookie_secure', 1); 
session_start();  
include_once("./configuration/config.php");
if(isset($_POST['username']) && isset($_POST['password'])){
	if((trim($_POST['username']) != '') && (trim($_POST['password']) != '')){
		$directionLink=GLOBAL_SITE_URL.$_POST['directionLink'];
		$user_login = str_replace("'", '', trim($_POST['username']));
		$password = str_replace("'", '', trim($_POST['password']));
		$dec_password = encodeVilniusPassword($password);
		$db_webhost = dbInit('webhost');
		$db_webhost->where ('EMAIL', $user_login);
		$db_webhost->where ('PASSWORD', $dec_password);
		$user_check=$db_webhost->getOne('USERS');
		if(empty($user_check)){   // if usrname + password not present in tvarkau DB; checking in paslaugos db
			$db_paslaugos = dbInit('paslaugos');
			$db_paslaugos->where ('username', $user_login);
			$user_password_type_check = $db_paslaugos->getOne('user','old_password');
			$dec_password=$user_password_type_check['old_password']==1 ? md5($password) : $dec_password;
			$db_paslaugos->where ('username', $user_login);
			$db_paslaugos->where ('password', $dec_password);
			$user_paslaugos_check = $db_paslaugos->getOne ('user');
			if(!empty($user_paslaugos_check)){			
				$_SESSION['AUTH']=1;
				$_SESSION['PEOPLE_ID'] = $user_paslaugos_check['id'];
				$_SESSION['NAME'] = $user_paslaugos_check['name'].' '.$user_paslaugos_check['surname'];
				$_SESSION['USER_CODE'] = $user_paslaugos_check['personal_code'];
				$db_paslaugos->where ('ui.user_id', $_SESSION['PEOPLE_ID']);
				$db_paslaugos->join ('user u', 'ui.user_id=u.id', 'LEFT');
				$userInfo2 = $db_paslaugos->getOne('user_info ui','ui.email, ui.telephone, ui.fact_address, u.password');
				$_SESSION['USER_ID'] = checkAndAddUser($_SESSION['USER_CODE'], $_SESSION['NAME'], $_SESSION['PEOPLE_ID'], $userInfo2);
				print json_encode(array(1,$directionLink));
			}
			else{
				$msg  = 'Įvestas blogas vartotojo vardas arba slaptažodis.';
				$msg_class = 'msg_front_bad';
				print json_encode(array(2,'Įvestas blogas vartotojo vardas arba slaptažodis!'));
			}
		}
		else{
			$_SESSION['AUTH']=1;
			$_SESSION['NAME'] = $user_check['FULL_NAME'];
			$_SESSION['USER_CODE'] = $user_check['PERSONAL_CODE'];
			$_SESSION['USER_ID']= $user_check['ID'];
			print json_encode(array(1,$directionLink));
		}
	}
}
else if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['user_id']) && isset($_POST['is_bank'])) {
	$db = new MysqliDb (vilnius_Server, vilnius_LoginName, vilnius_Password, vilnius_DbName);
	$db->where ("user_id", $_POST['user_id']);
	$userInfo = $db->getOne  ("user_person");
	if($db->count == 1){
		$_SESSION['PEOPLE_ID'] = $userInfo['user_id'];
		$_SESSION['NAME'] = $userInfo['f_name'].' '.$userInfo['l_name'];
		$_SESSION['USER_CODE'] = $userInfo['person_nr'];
	}
	else{
		$msg  = 'Klaida. Jokios informacijos apie vartotoją nerasta.';
		$msg_class = 'msg_front_bad';
	}
}
else if(empty($_POST['ticket']) === FALSE && empty($_POST['customData']) === FALSE){
	$_SESSION['AUTH']=1;
	$ret_data = get_viisp_user_info($_POST['ticket'], $_POST['customData']);
	$_SESSION['NAME'] =  $ret_data['firstName'].' '.$ret_data['lastName'];
	$_SESSION['USER_CODE'] = $ret_data['lt-personal-code'];
	$_SESSION['IS_BANK'] = 1;
    $_SESSION['USER_ID'] = checkAndAddUser($_SESSION['USER_CODE'], $_SESSION['NAME']);
    header("Location: ".GLOBAL_SITE_URL.'user_profile');
    die();      
}
else if(isset($_POST['fb_response_id'])){
	if(isset($_POST['type'])){
		if($_POST['type']=='login'){
			$user=getUserByFB($_POST['fb_response_id']);
			if(!$user){
				$fb_name=isset($_POST['fb_name'])?$_POST['fb_name']:'';
				$fb_email=isset($_POST['fb_email'])?$_POST['fb_email']:'';
				addSocialUser($_POST['fb_response_id'], 'fb', $fb_name, $fb_email) ;
				$user=getUserByFB($_POST['fb_response_id']);
			}
			$_SESSION['AUTH']=1;
			$_SESSION['USER_ID'] = $user['ID'];
			$_SESSION['NAME'] = $user['FULL_NAME'];
			$_SESSION['USER_CODE'] = $user['PERSONAL_CODE'];
			print json_encode(array(1,$_POST['directionLink']));
		}
		else if($_POST['type']=='check'){
			if(getUserByFB($_POST['fb_response_id'])){
				print json_encode(array(3,''));
			}
		}
		else if($_POST['type']=='add'){
			$user=getUserByFB($_POST['fb_response_id']);
			if($user){
				echo $user['ID'];
			}
			else{
				updateUserInfo($_SESSION['USER_ID'],$_POST['fb_response_id'],'fb');
				echo 'pridėta!';
			}
		}
		else if($_POST['type']=='remove'){
			updateUserInfo($_SESSION['USER_ID'],'','fb');
			echo 'pašalinta!';
		}
		else if($_POST['type']=='join' && ($_SESSION['USER_ID']==$_POST['user_to_join'])){
			updateUserInfo($_SESSION['USER_ID'],$_POST['fb_response_id'],'fb');
			updateProblemsChangeUser($_POST['user_to_join'],$_SESSION['USER_ID']);
			deleteUser($_POST['user_to_join']);
		}
	}
}
else if(isset($_POST['g_response_id'])){
	$user=getUserByGoogle($_POST['g_response_id']);
	if(!$user){
		$a=addSocialUser($_POST['g_response_id'], 'google', $_POST['g_name'], $_POST['g_email']) ;
		$user=getUserByGoogle($_POST['g_response_id']);
	}
	$_SESSION['AUTH']=1;
	$_SESSION['USER_ID'] = $user['ID'];
	$_SESSION['NAME'] = $user['FULL_NAME'];
	print json_encode(array(1,$_POST['directionLink']));
}
else{
	die('error unknown');
}
?> 