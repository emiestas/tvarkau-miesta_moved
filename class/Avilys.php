<?php
class Avilys{
	
	var $soap_url;
    var $soap_user;
    var $soap_psw; 
	var $endpoint;
	var $xmnls;
	var $xml_data;   // variable for main xml received through getDocument
	
	function __construct($cfg_data){
		$this->soap_url=$cfg_data['ENDPOINT'];
		$this->soap_user=$cfg_data['USER'];
		$this->soap_psw=$cfg_data['PSW'];
    }
	
	function init($ws) {  // security header
		$this->endpointSwitch($ws);
		$header = '<?xml version="1.0" encoding="UTF-8"?>
			<soapenv:Envelope xmlns:'.$this->xmnls.'="http://www.sintagma.lt/avilys/'.$this->endpoint.'" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
				<soapenv:Header>
					<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
					<wsse:UsernameToken>
						<wsse:Username>'.$this->soap_user.'</wsse:Username>
						<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'.$this->soap_psw.'</wsse:Password>
					</wsse:UsernameToken>
					</wsse:Security>
				</soapenv:Header>';
		return $header;
	}
	
	function endpointSwitch($ws){
		switch($ws){
			case 1: $this->xmnls='rdod'; $this->endpoint='RDODocumentWS';break;
			case 2: $this->xmnls='doc'; $this->endpoint='DocLinkWS'; break;
			case 3: $this->xmnls='tdod'; $this->endpoint='TDODocumentWS';break;
		}
	}	
	
	function curlGet($post_string, $ws){   
		$this->endpointSwitch($ws);
		$headers = array(
			"Content-type: text/xml;charset=\"utf-8\"",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"Expect:",
			"Content-length: ".strlen($post_string)); 
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_VERBOSE, true);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $this->soap_url.$this->endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->soap_user.":".$this->soap_psw); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
       // curl_setopt($ch, CURLOPT_TIMEOUT, 10);    // !!!!!!!!!!!!!!!!!
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		///////////////////////////////////////
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
        $response = curl_exec($ch); 
        curl_close($ch);
		return $response;
	}

	function createDocument($data) {  // function to operate createDocumentFromTemplate 
		$log_data['DATA_ARRAY']=$data;
		$bodyAttachment_str='';
		if(!empty($data['att_name'])){ /// attachment handling
			for($i=0;$i<count($data['att_name']);$i++){
				$bodyAttachment_str.=
				'<bodyAttachment>
					<type>clsDHSAttaType.SUPP</type>
					<action>add</action>
					<title>'.$data['att_name'][$i].'</title>
					<content>'.$data['att_content'][$i].'</content>
					<contentType>'.$data['att_contenttype'][$i].'</contentType>
				</bodyAttachment>';
			}
		}
		$journal=($data['journal_oid']=='')?'':'
		<entry>
			<key>draftJournal</key>
				<value xsi:type="ns1:JournalParam" xmlns:ns1="http://www.sintagma.lt/avilys/JournalWS">
				<oid>'.$data['journal_oid'].'</oid>
				</value>
		</entry>';
		$xml_header=$this->init(1);
		$xml_post_string = $xml_header.'
			<soapenv:Body>
				<rdod:createDocumentFromTemplate>
					<documentFromTemplateParam>
						<targetStaff>
							<orgName></orgName>
						</targetStaff>
						<docOid></docOid>
		                <docAttributes>
							<entry>
								<key>senders</key>
								<value xsi:type="q2:OrgNodeListParam" xmlns:q2="http://www.sintagma.lt/avilys/OrgStructWS">
									<orgNode xsi:type="q2:OrgContactPersonParam">
										<code>'.$data['user_personal_code'].'</code>
										<name>'.$data['user_name'].'</name>
										<address>'.$data['user_address'].'</address>
										<phone>'.$data['user_phone'].'</phone>
										<email>'.$data['user_email'].'</email>
										<createModify>true</createModify>
									</orgNode>
								</value>
							</entry>
							<entry>
								<key>receivers</key>
								<value xsi:type="ns1:OrgNodeListParam" xmlns:ns1="http://www.sintagma.lt/avilys/OrgStructWS">
									<orgNode xsi:type="ns1:OrgUnitParam">
										<orgName>'.$data['structure_oid'].'</orgName>
									</orgNode>
								</value>
							</entry>'.$journal.'
							<entry>
								<key>draftOfficeCase</key>
								<value xsi:type="ns1:OfficeCaseParam" xmlns:ns1="http://www.sintagma.lt/avilys/OfficeCaseWS">
									<oid>'.$data['file_oid'].'</oid>
								</value>
							</entry>		
							<entry>
								<key>title</key>
								<value>'.$data['title'].'</value>
							</entry>						
						</docAttributes>
						<extraAttributes>
							<entry>
								<key>fld_tvarkau_tekstas</key>
								<value>'.$data['text'].'</value>
							</entry>
							<entry>
								<key>fld_tvarkau_tipas</key>
								<value>'.$data['type_name'].'</value>
							</entry>
							<entry>
								<key>fld_tvarkau_adresas</key>
								<value>'.$data['address'].'</value>
							</entry>
							<entry>
								<key>fld_tvarkau_linkas</key>
								<value>'.$data['problem_url'].'</value>
							</entry>        
						</extraAttributes>
						'.$bodyAttachment_str.'
						<templateParam>
							<oid>'.$data['template_oid'].'</oid>
						</templateParam>
						<register>true</register>
						<project>false</project>
						<linkRdoIncDocOid></linkRdoIncDocOid>
						<linkTdoResultDocOid></linkTdoResultDocOid>
					</documentFromTemplateParam>
				</rdod:createDocumentFromTemplate>
			</soapenv:Body>
			</soapenv:Envelope>'; 
		$log_data['XML_IN']=$xml_post_string;
		$xml=$this->curlGet($xml_post_string,1);	
        // echo '<pre>' . $xml . '</pre>'; die();
		$log_data['XML_OUT']=$xml;
		$a=array();
		$a['oid']=$this->getTextBetweenTags('<oid>', '</oid>',$xml);
		//$a['documentcategory']=$this->getTextBetweenTags('<documentcategory>', '</documentcategory>',$xml);
		$a['registrationDate']=$this->getTextBetweenTags('<registrationDate>', '</registrationDate>',$xml);
		$a['registrationNo']=$this->getTextBetweenTags('<registrationNo>', '</registrationNo>',$xml);
		return array($a, $log_data);
	}

	function updateDocument($oid, $data) {  // function to operate modifyDocument
		if(!empty($data['att_name'])){ /// attachment handling
			$bodyAttachment_str='';
			for($i=0;$i<count($data['att_name']);$i++){
				$bodyAttachment_str.=
				'<bodyAttachment>
					<type>clsDHSAttaType.SUPP</type>
					<action>replace</action>
					<title>'.$data['att_name'][$i].'</title>
					<content>'.$data['att_content'][$i].'</content>
					<contentType>'.$data['att_contenttype'][$i].'</contentType>
				</bodyAttachment>';
			}
		}
		$xml_header=$this->init(1);
		$xml_post_string = $xml_header.'
			<soapenv:Body>
				<rdod:modifyDocument>
					<modifyDocumentParam>
						<docOid>'.$oid.'</docOid>
						<extraAttributes>
							<entry>
								<key>fld_tvarkau_tekstas</key>
								<value>'.$data['edit_desc'].'</value>
							</entry>      
						</extraAttributes>
						'.$bodyAttachment_str.'
					</modifyDocumentParam>
				</rdod:modifyDocument>
			</soapenv:Body>
			</soapenv:Envelope>'; 
		$xml=$this->curlGet($xml_post_string,1);
        // echo '<pre>' . $xml . '</pre>';
	}
    
    function deleteProblem($oid, $reason) {
        $xml_header=$this->init(1);
		$xml_post_string = $xml_header.'
			<soapenv:Body>
				<rdod:terminate>
					<terminateParam>
						<docOid>'.$oid.'</docOid>
                        <text>Dokumentas ištrintas pranešimą registravusio vartotojo. Priežastis: '.$reason.'</text>
					</terminateParam>
				</rdod:terminate>
			</soapenv:Body>
			</soapenv:Envelope>'; 
		$xml=$this->curlGet($xml_post_string,1);
        // echo '<pre>' . $xml . '</pre>';
    }
    
    function deleteProblemImg($oid, $data) {  // function to operate modifyDocument
		if(!empty($data['foto'])) { /// attachment handling
			$bodyAttachment_str='';
			foreach($data['foto'] as $key => $img) {
				$bodyAttachment_str.=
				'<bodyAttachment>
					<type>clsDHSAttaType.SUPP</type>
					<action>remove</action>
					<title>'.$img['TITLE'].'</title>
				</bodyAttachment>';
			}
		}
		$xml_header=$this->init(1);
		$xml_post_string = $xml_header.'
			<soapenv:Body>
				<rdod:modifyDocument>
					<modifyDocumentParam>
						<docOid>'.$oid.'</docOid>
						'.$bodyAttachment_str.'
					</modifyDocumentParam>
				</rdod:modifyDocument>
			</soapenv:Body>
			</soapenv:Envelope>'; 
		$xml=$this->curlGet($xml_post_string,1);
        // echo '<pre>' . $xml . '</pre>';
	}
	
	function getDocumentList($reg_nr) {  // function to get oid
		$xml_header=$this->init(1);
		$xml_post_string = $xml_header.'
			<soapenv:Body>
				<rdod:getDocumentList>
					<getDocumentListParam>
						<searchParameters>
							<entry>
								<key>registrationNo</key>
								<value>'.$reg_nr.'</value>
							</entry>
						</searchParameters>
						<maxResults>10</maxResults>
					</getDocumentListParam>
				</rdod:getDocumentList>
			</soapenv:Body>
			</soapenv:Envelope>'; 
		$xml=$this->curlGet($xml_post_string,1);
		$oid=$this->getTextBetweenTags('<oid>', '</oid>',$xml);
		return $oid;
	}
	
	function getDocument($reg_nr, $oid=false) {
		$xml_header=$this->init(1);
		$oid=(!empty($oid))?$oid:$this->getDocumentList($reg_nr);
		if($oid){
			$xml_post_string = $xml_header.'	
				<soapenv:Body>
					<rdod:getDocument>
						<getDocumentParam>
							<docOid>'.$oid.'</docOid>
							<expand>
								<entry>executors+</entry>
							</expand>
						</getDocumentParam>
					</rdod:getDocument>
				</soapenv:Body>
			</soapenv:Envelope>'; 
			$this->xml_data=$this->curlGet($xml_post_string, 1);
			$registration_date=$this->getEntryValue('registrationDate');
			$status=$this->getEntryValue('status');
			$executor_officialname=$this->getTextBetweenTags('<officialName>', '</officialName>');
			$executor_personname=$this->getTextBetweenTags('<personName>', '</personName>');
			$executor_personphone=$this->getTextBetweenTags('<personPhone>', '</personPhone>');
			$due_by=$this->getEntryValue('dueBy');   
			$result='
			<p><b>Registracijos data:</b> '.$this->dateCut($registration_date).'</p>
			<p><b>Būsena:</b> '.$status.'</p>
			<p><b>Pagrindiniai vykdytojai:</b><br>'.$executor_officialname.'<br>'.$executor_personname.'<br>'.$executor_personphone.'</p>
			<p><b>Dokumento terminas:</b> '.$this->dateCut($due_by).'</p>';
			return array($result, $status, $oid);
		}
	}
	
	function getLinkedDocumentsOID($reg_nr) {
		$xml_header=$this->init(2);
		$oid=$this->getDocumentList($reg_nr);
		if($oid){
			$xml_post_string = $xml_header.'	
			<soapenv:Body>
				<doc:getLinkedDocuments>
					<getLinkedDocumentsParam>
						<docOid>'.$oid.'</docOid>
					</getLinkedDocumentsParam>
					</doc:getLinkedDocuments>
				</soapenv:Body>
			</soapenv:Envelope>'; 
			$xml=$this->curlGet($xml_post_string, 2);
			
			$mainSlice=explode('<type>RDMT</type>',$xml);			
			$main_task_result=substr($mainSlice[0],-200);
			$target_result=$this->getTextBetweenTags('<targetDoc>', '</targetDoc>',$main_task_result);
			$oid_result=$this->getTextBetweenTags('<oid>', '</oid>',$target_result);

			return $oid_result;
		}
	}
	
	function getResultData($reg_nr) {
		$oid=$this->getLinkedDocumentsOID($reg_nr);
        //echo '<p>OID:  '.$oid.'</p>';
		$xml_header=$this->init(3);		
		$xml_post_string = $xml_header.'	
			<soapenv:Body>
				<tdod:getDocument>
					<getTdoDocumentParam>
						<docOid>'.$oid.'</docOid> 
						<expand>
							<entry>progress+</entry>
						</expand>
						<retrieveResultAttachments>CONTENT</retrieveResultAttachments>
					</getTdoDocumentParam>
				</tdod:getDocument>
			</soapenv:Body>
		</soapenv:Envelope>'; 
		$this->xml_data=$this->curlGet($xml_post_string, 3);	
        // echo '<pre>' . $this->xml_data . '</pre>';
		$respSlice=explode('--uuid:',$this->xml_data); // divide into logical parts: respSlice[0] - header, respSlice[1] - XML, then - files
		if(count($respSlice)>1){
			$fileTitle=$this->getTextBetweenTags('<title>', '</title>',$respSlice[1]);
			$resultText=$this->getEntryValue('resultText',$respSlice[1]);
			$completionDate=$this->getEntryValue('completionDate',$respSlice[1]);
			$countAttachments=substr_count($respSlice[1], '</resultAttachment>');
			$attSlice=explode('</resultAttachment>',$respSlice[1]);
			$resultFiles=array();
			for($i=0; $i<$countAttachments; $i++){
				$fileTitle=$this->getTextBetweenTags('<title>', '</title>',$attSlice[$i]);
				$fileSlice=$this->getTextBetweenTags('Content-Disposition: attachment;name="'.$fileTitle.'"','--uuid:');
				$clean_content=implode("\n", array_slice(explode("\n", $fileSlice), 2));  // removing first two blank lines
				$realFileTitle='answ_'.date('Ymd').'_'.time().'_'.$fileTitle;
				$answ_file=GLOBAL_REAL_URL.UPLOAD_PATH.$realFileTitle;
				$fh = fopen($answ_file, 'w');
				fwrite($fh, $clean_content);
				fclose($fh);
				
				$resultFiles[$i]=$realFileTitle;
			}
			return array($resultText, $resultFiles, $completionDate);
		}
	}
	
	function getTextBetweenTags($text1,$text2,$xml=''){
		$all_text=($xml=='')?$this->xml_data:$xml;
		$arr1=explode($text1,$all_text);
		if(count($arr1)>1){
			$arr2=explode($text2,$arr1[1]);
			return $arr2[0];
		}
		else{
			return NULL;
		}
	}
	
	function getEntryValue($tag, $xml=''){
		$string1=$this->getTextBetweenTags('<entry><key>'.$tag.'</key><value', '</value></entry>', $xml);
		$arr2=explode('>',$string1);
		if(count($arr2)>1){
			return $arr2[1];
		}
	}
	
	function dateCut($date){
		$arr=explode('T',$date);
		return $arr[0];
	}
}
?>