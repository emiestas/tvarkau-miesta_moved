<?php
function getMailConfig($cityID){
	$db = dbInit('webhost');
	$db->where ('CITY_ID', $cityID);
	$mailCFG=$db->getOne('MAIL_CFG');
	$db->__destruct();
	$mailCFG['PASSWORD']=encryptDecrypt('decrypt',$mailCFG['PASSWORD']);
	return $mailCFG;	
}

function checkMailIfSent($problemID, $statusId){
	$db = dbInit('webhost');
	$db->where ('PROBLEM_ID', $problemID);
    $db->where ('NEW_STATUS_ID', $statusId);
	return $db->getOne('STATUS_LOGS');
}

function sendMail($mailTemplateId, $userEmail,$avilioId, $resultText=false, $problemType=false, $problemID=false){
	if(empty(checkMailIfSent($problemID,$mailTemplateId))){ // send only if wasn't sent before
		$mailCfg=getMailConfig($_SESSION['CITY_ID']);
		$mail = new PHPMailer;
		$mail->setLanguage('lt', LIB_REAL_URL.'PHPMailer/language/');
		$mail->CharSet = 'UTF-8';
		$mail->isSMTP();                                
		$mail->Host = $mailCfg['SERVER'];  
		$mail->SMTPAuth = $mailCfg['SMTP_AUTH'];                               
		$mail->Username = $mailCfg['USER'];                
		$mail->Password = $mailCfg['PASSWORD'];                         
		$mail->SMTPSecure = $mailCfg['SMTP_SECURE'];                          
		$mail->Port = $mailCfg['SMTP_PORT'];                                 
		$mail->setFrom($mailCfg['FROM_EMAIL'],$mailCfg['FROM_NAME']);   
		$mail->addAddress($userEmail);     // Add a recipient
		$db=dbInit('webhost');
		$db->where ('STATUS_ID', $mailTemplateId);
		$db->where ('CITY_ID', $_SESSION['CITY_ID']);
		$advInfo = $db->getOne('MAIL_TEMPLATES');
		$varsToUseInMail = array(
          '{$regNr}' => ($avilioId)?$avilioId:'(laikinai nesuteikta)',
		  '{$regError}' => ($avilioId)?'':'<p style="color:red;"><strong>Problema kolkas neužregistruota vidinėje VMS dokumentų sistemoje dėl tech. kliūčių, tačiau jas pašalinus, registracija bus automatiškai įvykdyta.<br> 
Apie tai gausite el. laišką su registracijos numeriu</strong></p>',
          '{$resultText}' => $resultText,
		  '{$problemType}' => $problemType,
		  '{$problemLink}' => GLOBAL_SITE_URL.'problem/'.$problemID
        );
		$subject=strtr($advInfo['EMAIL_SUBJECT'], $varsToUseInMail);
		$body=strtr($advInfo['EMAIL_TEXT'], $varsToUseInMail);
		$mail->Subject = $subject;
		$mail->Body  =  $body;
		$mail->IsHTML(true); 
		$mail->send();
		return array(
			'userEmail'=>$userEmail,
			'subject'=>$subject,
			'body'=>$body
		);  
	}
	else{
		return array(
			'userEmail'=>'--jau siųsta--',
			'subject'=>'--jau siųsta--',
			'body'=>'--jau siųsta--'
		);
	}
}
?>