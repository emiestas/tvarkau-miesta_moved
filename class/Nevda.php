<?php
class Nevda{
	
	var $soap_url;
    var $soap_user;
    var $soap_psw; 
	var $endpoint;
	var $xmnls;
	var $xml_data;   // variable for main xml received through getDocument
	
	function __construct(){
		//$this->soap_url=$cfg_data['ENDPOINT'];
		//$this->soap_user=$cfg_data['USER'];
		//$this->soap_psw=$cfg_data['PSW'];
    }
	
	function callAPI($url, $post_string, $ws=false)
	{   
	error_reporting(E_ALL); ini_set('display_errors','On'); 
        $curl = curl_init();
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_string); 
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		//  'Content-Type: application/json',	
'POST /Token HTTP/1.1',
'Content-Type: application/x-www-form-urlencoded'
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_VERBOSE, true);  
       // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); 
        //curl_setopt($curl, CURLOPT_USERPWD, $this->soap_user.":".$this->soap_psw); // username and password - declared at the top of the doc
       // curl_setopt($curl, CURLOPT_TIMEOUT, 10);    // !!!!!!!!!!!!!!!!!
		///////////////////////////////////////
		//curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		//curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl); 
print_r($result);
if ($result === false) {
    $info = curl_getinfo($curl);
    die('error occured during curl exec. Additional info: '. print_r($info));
}
        curl_close($curl);
		return $result;
	}
	
	function getTextBetweenTags($text1,$text2,$xml=''){
		$all_text=($xml=='')?$this->xml_data:$xml;
		$arr1=explode($text1,$all_text);
		if(count($arr1)>1){
			$arr2=explode($text2,$arr1[1]);
			return $arr2[0];
		}
		else{
			return NULL;
		}
	}
	
	function getEntryValue($tag, $xml=''){
		$string1=$this->getTextBetweenTags('<entry><key>'.$tag.'</key><value', '</value></entry>', $xml);
		$arr2=explode('>',$string1);
		if(count($arr2)>1){
			return $arr2[1];
		}
	}
	
	function dateCut($date){
		$arr=explode('T',$date);
		return $arr[0];
	}
}
?>