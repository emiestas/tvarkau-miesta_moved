<?php
function makeClickableLinks($txt) {
	$transform1=preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $txt);
	$transform2=preg_replace('/(\S+@\S+\.\S+)/', '<a href="mailto:$1">$1</a>', $transform1);
	return $transform2;
}

function alprCheck($img){
	$data = array('secret_key'=>'sk_7ebb1e5bd142a147d9e7aaa6',
				  'image_url'=>MINIO_SITE_URL.MINIO_BUCKET_NAME.'/'.$img,
				  'country'=>'eu');
	$query = http_build_query($data);
	$url = 'https://api.openalpr.com/v2/recognize_url';
	$soap_do = curl_init();
	curl_setopt($soap_do, CURLOPT_URL, $url);
	curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($soap_do, CURLOPT_POST, true);
	curl_setopt($soap_do, CURLOPT_POSTFIELDS, $query);
	$result = curl_exec($soap_do);
	$err = curl_error($soap_do);
	if (!$result) {
		echo "Klaida jungiantis prie epaslaugos.lt\n";
	}
	$json = json_decode($result, true);
	if(!empty($json['results'])){
		$blur_array=array();
		foreach($json['results'] as $res){
			$y_arr=array( $res['coordinates'][0]['y'], $res['coordinates'][1]['y'], $res['coordinates'][2]['y'], $res['coordinates'][3]['y']);
			$x=$res['coordinates'][0]['x'];
			$y=min($res['coordinates'][0]['y'],$res['coordinates'][1]['y']);
			$width = $res['coordinates'][1]['x']-$res['coordinates'][0]['x'];
			$height = max($y_arr)-min($y_arr);
			$blur_array[]=array(
				'x'=>$x, 
				'y'=>$y, 
				'width'=>$width, 
				'height'=>$height
			);
		}
		return $blur_array;
	}	
	
}

function blurImage($data_arr, $upload_path, $foto){
	
	$img1 = imagecreatefromjpeg($upload_path); // load source

	foreach($data_arr as $b){
		$img2 = imagecreatetruecolor($b['width'], $b['height']); // create img2 for selection
		imagecopy($img2, $img1, 0, 0, $b['x'], $b['y'], $b['width'], $b['height']); // copy selection to img2
		$gaussian = array(
			array(2, 3, 2),
			array(3, 6, 3),
			array(2, 3, 2)
		  );
		for ($a=1; $a<=200; $a++){
		   imagefilter($img2, IMG_FILTER_GAUSSIAN_BLUR);
		}
		imagecopymerge($img1, $img2, $b['x'], $b['y'], 0, 0, $b['width'], $b['height'], 100); // merge img2 in img1
	}
	imagejpeg($img1, MINIO_PATH.MINIO_BUCKET_NAME.'.edited/'.$foto);
	$thmbnl=createThumbnail($foto, '1');
	$upload_path2=realpath(UPLOAD_PATH.'thumbs/'.$foto);
	copy($upload_path2, MINIO_PATH.MINIO_BUCKET_NAME.'.edited.thumbs/'.$foto);
	unlink($upload_path2);
	imagedestroy($img1);
	imagedestroy($img2);
}

function get_viisp_user_info($ticket_id, $custom_data) {
	include_once(GLOBAL_REAL_URL.'viisp/xmlclasstst.php');
    $ret = array('lt-personal-code' => '',
        'lt-company-code' => '',
        'firstName' => '',
        'lastName' => '',
        'companyName' => '',
        'customData' => ''
    );
    $document = new DOMDocument('1.0', 'utf-8');
    $document->formatOutput = false;
    $request = $document->createElement('ns2:authenticationDataRequest');
    $id = $document->createAttribute('id');
    $id->value = 'uniqueNodeId';
    $xmlns2 = $document->createAttribute('xmlns:ns2');
    $xmlns2->value = 'http://www.epaslaugos.lt/services/authentication';
    $xmlns0 = $document->createAttribute('xmlns');
    $xmlns0->value = 'http://www.w3.org/2000/09/xmldsig#';
    $xmlns3 = $document->createAttribute('xmlns:ns3');
    $xmlns3->value = 'http://www.w3.org/2001/10/xml-exc-c14n#';
    $request->appendChild($xmlns2);
    $request->appendChild($xmlns0);
    $request->appendChild($xmlns3);
    $request->appendChild($id);
    $pid = $document->createElement('ns2:pid', 'VSID000000001950');
    $ticket = $document->createElement('ns2:ticket', $ticket_id);
    $request->appendChild($pid);
    $request->appendChild($ticket);
    $document->appendChild($request);
    $xml_pay2 = trim($document->saveXml());
    $doc = $xml_pay2;
    if (!$doc) {
        echo "Error while parsing the input xml document\n";
        exit;
    }
    $xmlsec = new xmlsec(GLOBAL_REAL_URL . 'viisp/keys.xml', GLOBAL_REAL_URL.'/temp');
    $xmlsec->viisp_act = 'authenticationDataRequest';
    $res = $xmlsec->sign($document, XMLSEC_RSA_SHA1);

    if (!$res)
        die($xmlsec->errorMsg . '<br>' . $xmlsec->cmd);
    else
        $assss = ''; //header('Content-type: text/xml'); 

    $res = str_replace('<?xml version="1.0" encoding="utf-8"?>', '', $res);
    $soap = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://www.epaslaugos.lt/services/authentication" xmlns:xd="http://www.w3.org/2000/09/xmldsig#">
	<soapenv:Header/>
		<soapenv:Body>
	' . $res . '
		</soapenv:Body>
	</soapenv:Envelope>';
    $url = 'https://www.epaslaugos.lt/portal/authenticationServices/auth';
    $soap_do = curl_init();
    curl_setopt($soap_do, CURLOPT_URL, $url);
    curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($soap_do, CURLOPT_POST, true);
    curl_setopt($soap_do, CURLOPT_POSTFIELDS, $soap);
    curl_setopt($soap_do, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($soap)));
    $result = curl_exec($soap_do);
    $err = curl_error($soap_do);
	
	if (!$result) {
        echo "Klaida jungiantis prie epaslaugos.lt\n";
        exit;
    }
    $response = new DOMDocument('1.0', 'utf-8');
    $response->loadXML($result);
    $authenticationAttributes = $response->getElementsByTagName('authenticationAttribute');
    $count_attr = $authenticationAttributes->length;

    for ($i = 0; $i < $count_attr; $i++) {
        $attr = $val = '';
        $attr = $authenticationAttributes->item($i)->getElementsByTagName('attribute')->item(0)->nodeValue;
        if ($authenticationAttributes->item($i)->getElementsByTagName('value')->length > 0)
            $val = $authenticationAttributes->item($i)->getElementsByTagName('value')->item(0)->nodeValue;
        $ret[$attr] = $val;
    }

    $userInformation = $response->getElementsByTagName('userInformation');
    $count_info = $userInformation->length;

    for ($i = 0; $i < $count_info; $i++) {
        $attr = $val = '';
        $attr = $userInformation->item($i)->getElementsByTagName('information')->item(0)->nodeValue;
        if ($userInformation->item($i)->getElementsByTagName('stringValue')->length > 0)
            $val = $userInformation->item($i)->getElementsByTagName('stringValue')->item(0)->nodeValue;
        $ret[$attr] = $val;
    }

    $customData = $response->getElementsByTagName('customData');
    $count_custom = $customData->length;

    for ($i = 0; $i < $count_custom; $i++) {
        $attr = 'customData';
        $val = '';
        $val = $customData->item($i)->nodeValue;
        $ret[$attr] = $val;
    }

    if ($ret['customData'] == $custom_data)
        return $ret;
    else
        return array('lt-personal-code' => '', 'lt-company-code' => '', 'firstName' => '', 'lastName' => '', 'companyName' => '', 'customData' => '');
}

function dbInit($type){
	if($type=='paslaugos'){
		$host=paslaugos_Server;
		$username=paslaugos_LoginName;
		$password=paslaugos_Password;
		$db=paslaugos_DbName;
		$prefix=paslaugos_Prefix;
		$charset='utf8';
	}
	else if($type=='vilnius'){
		$host=vilnius_Server;
		$username=vilnius_LoginName;
		$password=vilnius_Password;
		$db=vilnius_DbName;
		$prefix=vilnius_Prefix;
		$charset='';
	}
	else{
		$host=PROBLEMOS_HOST;
		$username=PROBLEMOS_USER;
		$password=PROBLEMOS_PASS;
		$db=PROBLEMOS_DB;
		$prefix=PREFIX;
		$charset='utf8';
	}
	$dbinit=new MysqliDb (Array (
		'host' => $host,
		'username' =>$username, 
		'password' => $password,
		'db'=> $db,
		'prefix' => $prefix,
		'charset' => $charset));
	return $dbinit;
}

function getActiveCities() {
    $db = dbInit('webhost');
    $db->where ('IS_ACTIVE', 1);
	$activeCities=$db->get('MAIN_CFG', null, 'ID, CITY_LABEL');
	$db->__destruct();
    return $activeCities;
}

function addSocialUser($userID, $social, $userName=false, $userEmail=false) {
	$db = dbInit('webhost');
	$fb_or_g=($social=='fb')?'FACEBOOK_ID':'GOOGLE_ID';
	$data = Array (
		$fb_or_g => $userID,
		'CITY_ID' => $_SESSION['CITY_ID'],
		'FULL_NAME' =>  $userName,
		'LAST_LOGIN_TIME' =>  date('Y-m-d H:i:s'),
		'EMAIL' => $userEmail
	);
    $db->insert ('USERS', $data); 
	return $db;
}

function checkAndAddUser($userCode, $userName, $userInVlnDb=null, $paslaugos_data=null) {
	$db = dbInit('webhost');
    $db->where ('PERSONAL_CODE', $userCode);
    $userInfo = $db->getOne('USERS');
	if($db->count == 1){
        if(isset($_SESSION['IS_BANK'])) {
            $db->where ('PERSONAL_CODE', $userCode);
            $userData = Array (
				'PERSONAL_CODE' => $userCode,
				'FULL_NAME' => $userName
		    );
            $db->update ('USERS', $userData);
        } 
		elseif(!isset($userInfo['ID_VLN_DB'])) {
            $userData = array();
            if(!isset($userInfo['ID_VLN_DB'])) {
                $userData['ID_VLN_DB'] =  $userInVlnDb;
            }
            if(!isset($userInfo['PHONE'])) {
                $userData['PHONE'] =  $paslaugos_data['telephone'];
            }
            if(!isset($userInfo['EMAIL'])) {
                $userData['EMAIL'] =  $paslaugos_data['email'];
            }
            if(!isset($userInfo['ADDRESS'])) {
                $userData['ADDRESS'] =  $paslaugos_data['fact_address'];
            }
			if(!isset($userInfo['PASSWORD'])) {
                $userData['PASSWORD'] =  $paslaugos_data['password'];
            }
            $db->where ('PERSONAL_CODE', $userCode);
            $db->update ('USERS', $userData);
        }
        setUserLoginDate($userCode);
    }
	else{
		$data = Array (
			'CITY_ID' => isset($_SESSION['CITY_ID'])?$_SESSION['CITY_ID']:1,
			'PERSONAL_CODE' => $userCode,
			'FULL_NAME' => $userName,
			'ID_VLN_DB' => $userInVlnDb,
			'LAST_LOGIN_TIME' =>  date('Y-m-d H:i:s'),
			'PHONE' => $paslaugos_data['telephone'],
			'EMAIL' => $paslaugos_data['email'],
			'ADDRESS' => $paslaugos_data['fact_address'],
			'PASSWORD' => $paslaugos_data['password']
		);
        $db->insert ('USERS', $data);
	}
	$userInfo=getUserByCode($userCode);
	$db->__destruct();
	return $userInfo['ID'];
}

function getUserByCode($userCode){
	$db = MysqliDb::getInstance();
	//$db = dbInit('webhost');
	$db->where ('PERSONAL_CODE', $userCode);
	return $db->getOne('USERS');
}

function getUserByFB($fbCode){
	$db = dbInit('webhost');
	$db->where ('FACEBOOK_ID', $fbCode);
	$getUser=$db->getOne('USERS');
	$db->__destruct();
	return $getUser;
}

function getUserByGoogle($gCode){
	$db = dbInit('webhost');
	$db->where ('GOOGLE_ID', $gCode);
	$getUser=$db->getOne('USERS');
	$db->__destruct();
	return $getUser;
}

function setUserLoginDate($userCode){
	$db = MysqliDb::getInstance();
	$data = Array (
		'LAST_LOGIN_TIME' => date('Y-m-d H:i:s')
	);
	$db->where ('PERSONAL_CODE', $userCode);
	$db->update ('USERS', $data);
}

function encodeVilniusPassword($pass) {
	$PwdSalt1 = VILNIUS_PSW_SALT1;
	$PwdSalt2 = VILNIUS_PSW_SALT2;
	$password = md5($PwdSalt1 . $pass . $PwdSalt2);
	return $password;
}

function getUserInfo($id){ 
	$db = dbInit('webhost');
	$db->where ('ID', $id);
	$user=$db->getOne('USERS');
	$db->__destruct();
	return $user;
}

function updateUserInfo($id, $data, $case=false){
	$db = dbInit('webhost');
	if($case='user_profile' && is_array($data)){
		$updateData = Array (
			'EMAIL' => $data['EMAIL'],
			'PHONE' => $data['PHONE'],
			'ADDRESS' => $data['ADDRESS']
		);		
	}
	else if($case='fb'){
		$updateData = Array (
			'FACEBOOK_ID' => $data
		);	
	}
	$db->where ('ID', $id);
	$db->update ('USERS', $updateData);
	$db->__destruct();
	return true;
}

function deleteUser($id){
	$db = dbInit('webhost');
	$db->where ('ID', $id);
	$db->delete ('USERS');
	$db->__destruct();
}

function addProblem($data){
	$addr_expl=explode(',',$data['new_place']);
	$postcode=explode('Vilnius ',$addr_expl[1]);
	$street_expl=preg_split("/\s+(?=\S*+$)/",$addr_expl[0]);
	$lks_coord=(!empty($data['new_lat']) && !empty($data['new_lng']))?geo2grid($data['new_lat'],$data['new_lng']):'';
	$violation_datetime=($data['new_type']==20)?$data['violation_date'].' '.str_replace(' ', '', $data['violation_time']):'';
	$description=($data['new_type']==20) ? makeClickableLinks($data['new_desc']).'<br>Pažeidimo data ir laikas: '.$violation_datetime.'<br>Automobilio valst. nr.: '.$data['car_plate_no']
	:makeClickableLinks($data['new_desc']);
	$db = dbInit('webhost');
	$insertArr = Array (
		'CITY_ID' => $_SESSION['CITY_ID'],
		'ADDRESS' => $data['new_place'],
		'ADDRESS_POSTCODE' => $postcode[1],
		'ADDRESS_STREET' => $street_expl[0],
		'ADDRESS_HOUSE_NO' => $street_expl[1],
		'COORDS_GOOGLE_LAT' => $data['new_lat'],
		'COORDS_GOOGLE_LNG' => $data['new_lng'],
		'COORDS_LKS_X' => $lks_coord[0], 
		'COORDS_LKS_Y' => $lks_coord[1], 
		'PROBLEM_TYPE_ID' => $data['new_type'], 
		'DESCRIPTION' => $description,
		'REG_DATE' => $db->now(), 
		'USER_ID' =>($_SESSION['AUTH']==1)?$_SESSION['USER_ID']:ANONYMOUS_USER_ID, 
		'STATUS_ID' => '1',
		'CAR_PLATE_NO' => $data['car_plate_no'],
		'VIOLATION_DATETIME' => $violation_datetime,
		'REPORTER_NAME' => $data['user_name'],
		'REPORTER_EMAIL' => $data['user_email'],
		'REPORTER_ADDRESS' => $data['user_address'],
		'REPORTER_PHONE'=> $data['user_phone']
	);
	$problem_id=$db->insert('REG_PROBLEMS', $insertArr);
	if(isset($data['foto'])&&!empty($data['foto'])){
		foreach($data['foto'] as $key => $foto){
			if(basename($foto) == $foto){
				$upload_path=UPLOAD_PATH.$foto;
				if(file_exists($upload_path)){
					$thmbnl=createThumbnail($foto);
					$foto_ext=strtolower(pathinfo($upload_path, PATHINFO_EXTENSION));
					copy($upload_path, MINIO_PATH.MINIO_BUCKET_NAME.'/'.$foto);
					if($thmbnl != 'PDF_IMG_SMALL' && $thmbnl != 'FILE_IMG_SMALL'){
						$upload_path2=UPLOAD_PATH.'thumbs/'.$foto;
						copy($upload_path2, MINIO_PATH.MINIO_BUCKET_NAME.'.thumbs/'.$foto);
						unlink($upload_path2);
					}
					$alpr=alprCheck($foto);
					
					if(!empty($alpr)){
						blurImage($alpr, $upload_path, $foto);
						$edited=1;
					}

					$title = 'Prisegtas_dokumentas_'.($key+1);

					$insertArrFiles = Array (
						'PROBLEM_ID' => $problem_id,
						'FILE_TYPE' => $foto_ext,
						'SAVED_AS' => $foto,
						'TITLE' => $title,
						'THUMBNAIL' => $thmbnl,
						'BUCKET' => MINIO_BUCKET_NAME,
						'EDITED' => (isset($edited) && $edited==1)?1:0
					);
					
					$db2=MysqliDb::getInstance();
					$db2->insert('REG_FILES', $insertArrFiles);
					$db2->__destruct();
				}
			}
		}
	}
	$db->__destruct();
	return $problem_id;
}

function updateProblemsChangeUser($old_user,$new_user){
	$db = dbInit('webhost');
	$db->where ('USER_ID', $old_user);
	$updateData=Array (
			'USER_ID' => $new_user
		);	
	$db->update ('REG_PROBLEMS', $updateData);	
	$db->__destruct();
}

function updateProblem($problem_id,$field,$new_value,$data=false){
	if(!empty($data[2])){
		$slice=explode('T',$data[2]);
		if(count($slice)>1){
			$completionDate = $slice[0].' '.substr($slice[1], 0, 8);
		}
		else{
			$completionDate = date("Y-m-d");
		}
	}
	$editArr=array();
	if(!empty($data[2])){
		$editArr['COMPLETE_DATE']=$completionDate;
		$editArr['ANSWER']=makeClickableLinks($data[0]);
	}	
	$editArr[$field]=$new_value;
	$db = dbInit('webhost');
	$db->where ('ID', $problem_id);
	$db->update ('REG_PROBLEMS', $editArr);
	if(isset($data[1])&&!empty($data[1])){
		foreach($data[1] as $answ_file){
			$foto_path=realpath(GLOBAL_REAL_URL.UPLOAD_PATH.$answ_file);
			$foto_ext=strtolower(pathinfo($foto_path, PATHINFO_EXTENSION));		
			$pageCFG=getPageConfig($_SESSION['CITY_ID']);
			$answer_bucket='answers.'.$pageCFG['CITY_SYSTEM_NAME'];
			copy($foto_path, MINIO_PATH.$answer_bucket.'/'.$answ_file);
			unlink($foto_path);
			$answerData = Array (
				'PROBLEM_ID' =>$problem_id,
				'FILE_TYPE' => $foto_ext,
				'SAVED_AS' => $answ_file,
				'THUMBNAIL' => '',
				'BUCKET' =>  $answer_bucket
			);
			$db->insert ('REG_FILES_ANSWER', $answerData);
		}
	}
	$db->__destruct();
	return $problem_id;
}

function updateProblemLikes($problem_id,$type){
	$db = dbInit('webhost');
	$db->where ('ID', $problem_id);
	$updateData = Array ('NUM_'.$type => $db->inc(1));
	$db->update ('REG_PROBLEMS', $updateData);
	$db->__destruct();
	return $problem_id;
}

function updateProblemLog($problem_id,$new_status,$mail){
	$db = dbInit('webhost');
	$insertLog = Array (
		'PROBLEM_ID' => $problem_id,
		'NEW_STATUS_ID' => $new_status,
		'DATE' => $db->now(),
		'EMAIL_RECEIVER' => $mail['userEmail'],
		'EMAIL_SUBJECT' => $mail['subject'],
		'EMAIL_BODY' => $mail['body']
	);
	$db->insert('STATUS_LOGS', $insertLog);
	$db->__destruct();	
	return $problem_id;
}

function updateAvilysTxLog($problem_id,$log_data){
	$db = dbInit('webhost');
	$data_array=json_encode($log_data['DATA_ARRAY']);
	$insertLog = Array (
		'TX_DATE' => $db->now(),
		'PROBLEM_ID' => $problem_id,
        'CITY_ID' => $_SESSION['CITY_ID'],
		'DATA_ARRAY' => $data_array,
		'XML_IN' => $log_data['XML_IN'],
		'XML_OUT' => $log_data['XML_OUT']
	);
	$db->insert('REG_AVILYS_TX_LOGS', $insertLog);
	$db->__destruct();
}

function getPageConfig($cityID){
	$db = dbInit('webhost');
	$db->where ('ID', $cityID);
	$pgCFG=$db->getOne('MAIN_CFG');
	$db->__destruct();
	return $pgCFG;	
}

function getDVSConfig($cityID){
	$db = dbInit('webhost');
	$db->where ('CITY_ID', $cityID);
	$dvsCFG=$db->getOne('DVS_CFG');
	$db->__destruct();
	$dvsCFG['PSW']=encryptDecrypt('decrypt',$dvsCFG['PSW']);
	return $dvsCFG;	
}

function encryptDecrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = ENC_KEY;
    $secret_iv = ENC_IV;
    
    $key = hash('sha256', $secret_key); // hash
	$iv = substr(hash('sha256', $secret_iv), 0, 16);// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	
    if ($action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
	} 
	else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

function getImgBucket($problem_id){
	$db = dbInit('webhost');
	$db->where ('PROBLEM_ID', $problem_id);
	$imgArr=$db->getOne('REG_FILES');
	$db->__destruct();
	return $imgArr['BUCKET'];
}

function addProblemAvilys($arr, $problem_id, $cron=false, $city_id=false) { 
	if(!empty($arr['foto'])){
		for($i=0;$i<count($arr['foto']);$i++){
			if (basename($arr['foto'][$i]) == $arr['foto'][$i]){
				$path=GLOBAL_REAL_URL.UPLOAD_PATH.$arr['foto'][$i];
				if (file_exists($path)) {
					if($cron==1){
						$download_path=MINIO_SITE_URL.getImgBucket($problem_id).'/'.$arr['foto'][$i];
						copy($download_path, $path);
					}
					$content = file_get_contents($path);
					$arg['att_content'][$i] = base64_encode($content);
					$arg['att_name'][$i] = 'Prisegtas_dokumentas_'.($i + 1);
					$arg['att_contenttype'][$i] = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path);
					unlink($path);
				}
			}
		}
	}
	// **user data //   
	$usr_type=(isset($_SESSION['USER_ID']) && $_SESSION['USER_ID']>0)?$_SESSION['USER_ID']:ANONYMOUS_USER_ID;
	$usr_code=(isset($_SESSION['AUTH']) && $_SESSION['AUTH']==1)?$_SESSION['USER_CODE']:ANONYMOUS_USER_CODE;  
	$USER_ID=($cron==1)?$arr['user_id']:$usr_type;
	$user_info=getUserInfo($USER_ID);
	$USER_CODE=($cron==1)?$user_info['PERSONAL_CODE']:$usr_code;

	$arg['user_personal_code']=$USER_CODE;			
	$arg['user_name']=filter_var($user_info['FULL_NAME'], FILTER_SANITIZE_STRING);
	$arg['user_address']=filter_var($user_info['ADDRESS'], FILTER_SANITIZE_STRING);
	$arg['user_phone']=$user_info['PHONE'];
	$arg['user_email']=$user_info['EMAIL'];
	
	// **executor data //
	$executor_data=getExecutor($arr['new_type']);
	$arg['template_oid']=$executor_data['TEMPLATE_OID'];
	$arg['structure_oid']=$executor_data['STRUCTURE_OID'];
	$arg['file_oid']=$executor_data['FILE_OID'];
	$arg['journal_oid']=$executor_data['JOURNAL_OID'];
	$arg['problem_url'] = GLOBAL_SITE_URL.'problem/'.$problem_id;
	
	$violation_date=(!empty($arr['violation_date']))?' Pažeidimo data ir laikas: '.$arr['violation_date'].' ':'';
	$violation_time=(!empty($arr['violation_time']))?$arr['violation_time'].';':'';
	$car_plate_no=(!empty($arr['car_plate_no']))?' Automobilio valst. nr: '.$arr['car_plate_no'].';':'';
	$reporter_name=(!empty($arr['reporter_name']))?' Pranešėjas: '.$arr['reporter_name'].';':'';
	$reporter_email=(!empty($arr['reporter_email']))?' El.paštas: '.$arr['reporter_email'].';':'';
	$personal_data=(!empty($arr['personal_data']))?' Asmens kodas/gimimo data: '.$arr['personal_data'].';':'';
	
	$arg['text'] = ($arr['new_type']==20)?
	$arr['new_desc'].$violation_date.$violation_time.$car_plate_no.$reporter_name.$reporter_email.$personal_data:
	$arr['new_desc'];
	
	$user_name=(!empty($arr['user_name']))?' Pranešėjas: '.$arr['user_name'].';':'';
	$user_email=(!empty($arr['user_email']))?' El.paštas: '.$arr['user_email'].';':'';
	$user_address=(!empty($arr['user_address']))?' Adresas: '.$arr['user_address'].';':'';
	$user_phone=(!empty($arr['user_phone']))?' Tel. nr.: '.$arr['user_phone'].';':'';
	$arg['text'] = (!isset($_SESSION['AUTH']))?
	$arg['text'].$user_name.$user_email.$user_address.$user_phone:
	$arg['text']; 
	$arg['address']=$arr['new_place'];
	$arg['type_name']=getProblemType($arr['new_type']);
	$arg['title']='TVARKAU MIESTĄ PRANEŠIMAS ('.strtoupper($arg['type_name']).')';
	$arg['text'] = htmlspecialchars($arg['text']);
	$dvsCFG=getDVSConfig($_SESSION['CITY_ID']);
	$avilys=new Avilys($dvsCFG);
	$avilys_result=$avilys->createDocument($arg);
	$oid=$avilys_result[0]['oid'];
	$registrationNo=$avilys_result[0]['registrationNo'];
	if(AVILYS_DEBUG==1) updateAvilysTxLog($problem_id,$avilys_result[1]);
	return $avilys_result[0];
}

function updateProblemAvilys($problem_oid, $arr) {
	if(!empty($arr['foto'])){
		for($i=0;$i<count($arr['foto']);$i++){
			$content=file_get_contents(MINIO_PATH.getImgBucket($arr['problem_id']).'/'.$arr['foto'][$i]);
			$arg['att_content'][$i]=base64_encode($content);
			$arg['att_name'][$i]='Prisegtas_dokumentas_'.($i+1);
			$arg['att_contenttype'][$i]=finfo_file(finfo_open(FILEINFO_MIME_TYPE), MINIO_PATH.getImgBucket($arr['problem_id']).'/'.$arr['foto'][$i]);
		}
	}
	$arg['edit_desc']=$arr['edit_desc'];
	$arg['edit_desc'] = strip_tags($arg['edit_desc']);
	$dvsCFG=getDVSConfig($_SESSION['CITY_ID']);
	$avilys=new Avilys($dvsCFG);
	$avilys_result=$avilys->updateDocument($problem_oid, $arg);
	return $avilys_result[1];
}

function deleteProblemImg($problem_oid, $arr) {
	$dvsCFG=getDVSConfig($_SESSION['CITY_ID']);
	$avilys=new Avilys($dvsCFG);
	$avilys_result=$avilys->deleteProblemImg($problem_oid, $arr);
	return $avilys_result[1];
}

function updateAvilysTable($avilys_reg_nr,$avilys_oid){
	$db = dbInit('webhost');
	$updateData = Array (
		'AVILYS_OID' => $avilys_oid,
		'LAST_UPDATE' => date('Y-m-d H:i:s')
	);	
	$db->where ('AVILYS_REG_NR', $avilys_reg_nr);
	$db->update ('REG_AVILYS', $updateData);
	$db->__destruct();
	return true;	
}

function getProblemDataAvilys($problem_reg_nr, $problem_oid=false, $city_id) {
	$dvsCFG=getDVSConfig($city_id);
	$avilys=new Avilys($dvsCFG);
	$avilys_result=$avilys->getDocument($problem_reg_nr, $problem_oid);
	return $avilys_result;
}

function getResultAvilys($problem_reg_nr, $city_id) {
	$dvsCFG=getDVSConfig($city_id);
	$avilys=new Avilys($dvsCFG);
	$avilys_result=$avilys->getResultData($problem_reg_nr);
	return $avilys_result;
}

function linkProblemToAvilys($avilys, $problem){  
	$d1=explode('T',$avilys['registrationDate']); 
	$d2=explode('.',$d1[1]);
	$insertArr = Array (
		'PROBLEM_ID' => $problem, 
		'AVILYS_OID' => $avilys['oid'], 
		'AVILYS_REG_NR' => $avilys['registrationNo'], 
		'REG_DATE' => $d1[0].' '.$d2[0]
	);
	$db = dbInit('webhost');
	$insert_id=$db->insert('REG_AVILYS', $insertArr);
	$db->__destruct();
	return $insert_id;
}

function dateValidation($date){
	$date_arr  = explode('-', $date);
	if (count($date_arr) == 3) {
		if (checkdate($date_arr[1], $date_arr[2], $date_arr[0])) {
			return true;
		}
		else{
			return false;
		}	
	}
	else{
		return false;
	}
}

function filteredSearch($search_arr=false){
	$filterArr=array();
	if($search_arr !== false){
		if(!empty($search_arr['addressOrRegId'])){
			$filterArr[] = array('(p.ADDRESS LIKE ? OR avilys.AVILYS_REG_NR LIKE ?)', array('%'.trim($search_arr['addressOrRegId']).'%','%'.trim($search_arr['addressOrRegId']).'%'));
		}
        if(!empty($search_arr['problemStatus'])){
			$selectedStatusTypeArray=explode(',',$search_arr['problemStatus']);
			$filterArr[] = array('p.STATUS_ID', $selectedStatusTypeArray, 'IN');
		}
        if(!empty($search_arr['problemType'])){
			$selectedProblemTypeArray=explode(',',$search_arr['problemType']);		
			$filterArr[] = array('p.PROBLEM_TYPE_ID', $selectedProblemTypeArray, 'IN');
		}
        if(!empty($search_arr['dateFrom']) && dateValidation($search_arr['dateFrom'])){
			$filterArr[] = array('p.REG_DATE', $search_arr['dateFrom'].' 00:00:00', '>=');
		}
		if(!empty($search_arr['dateTo']) && dateValidation($search_arr['dateTo'])){
			$filterArr[] = array('p.REG_DATE', $search_arr['dateTo'].' 23:59:59', '<=');
		}
        if(!empty($search_arr['keyWord'])){
			$filterArr[] = array('p.DESCRIPTION', '%'.$search_arr['keyWord'].'%', 'like');
		}
		if(!empty($search_arr['USER_ID'])){
			$filterArr[] = array('p.USER_ID',$search_arr['USER_ID']);
		}
        if(!empty($search_arr['search_status'])){
			$filterArr[] = array('p.STATUS_ID',$search_arr['search_status']);
		}
	}
	if((int)$_SESSION['CITY_ID']){
		$filterArr[]=array('p.CITY_ID',$_SESSION['CITY_ID']);
	} 
	return $filterArr;
}


function getProblemCount($search_arr = false, $user_id = false, $city_id = false, $user_problems = false){
	if(is_int($user_id) && empty($search_arr['addressOrRegId'])) {
		$search_arr['USER_ID'] = $user_id;
	}
	$db = dbInit('webhost');
	$db->join('REG_AVILYS avilys', 'p.ID = avilys.PROBLEM_ID', 'LEFT');
    if($user_problems) { $db->join('SUBSCRIBED_PROBLEMS sp', 'sp.PROBLEM_ID = p.ID', 'LEFT'); }
    $db->where('p.DELETED', 1, '!=');
	$filter = filteredSearch($search_arr);
	foreach($filter as $f) {
		call_user_func_array(array($db, 'where'), $f);
	}
	if(is_int($city_id)) {
		$db->where('p.CITY_ID', $city_id);
	}

	if($user_problems && empty($search_arr['addressOrRegId'])) { 
		$db->orWhere('sp.USER_ID', $user_id); 
	} else if($user_problems && !empty($search_arr['addressOrRegId'])) {
		$db->where('(p.USER_ID = ? OR sp.USER_ID = ?)', array($user_id, $user_id)); 
	}

	$count = $db->getValue('REG_PROBLEMS p', 'count(p.ID)');
	$db->__destruct();
	return $count;
}

function countProblemsByProblemType($typeId, $search_arr=null, $userId = null) {
	$db = dbInit('webhost');
	$db->join('REG_AVILYS avilys', 'p.ID=avilys.PROBLEM_ID', 'LEFT');
    $db->where('p.DELETED', 1, '!=');
    $db->where ('PROBLEM_TYPE_ID', $typeId);
	$filter=filteredSearch($search_arr);
	foreach($filter as $f){
		call_user_func_array(array($db, 'where'),$f);
	}
    $count = $db->getValue ("REG_PROBLEMS p", "count(PROBLEM_TYPE_ID)");
	$db->__destruct();
    return $count;
}

function getProblemsPhotos($problemId) {
    $db = dbInit('webhost');
    $db->where ("PROBLEM_ID", $problemId);
	$getPhotos=$db->get('REG_FILES', null, 'SAVED_AS, TITLE, THUMBNAIL, BUCKET');
	$db->__destruct();
    return $getPhotos;
}

function getPhotosByID($id) {
    $db = dbInit('webhost');
    $db->where ("ID", $id, 'IN');
	$getPhotos=$db->get('REG_FILES', null, 'SAVED_AS, TITLE');
	$db->__destruct();
    return $getPhotos;
}
	
function getProblemList($page = false, $search_arr = false, $user_id = false, $city_id = false) {
	if(is_int($user_id) && empty($search_arr['addressOrRegId'])) {
		$search_arr['USER_ID']=$user_id;
	}
	$db = dbInit('webhost');
	$filter=filteredSearch($search_arr);
	foreach($filter as $f) {
		call_user_func_array(array($db, 'where'),$f);
	}

	if(is_int($city_id)) {
		$db->where('p.CITY_ID', $city_id);
	}

	$db->where('p.DELETED', 1, '!=');
	if($user_id && empty($search_arr['addressOrRegId'])) { 
		$db->orWhere('sp.USER_ID', $user_id); 
	} else if($user_id && !empty($search_arr['addressOrRegId'])) {
		$db->where('(p.USER_ID = ? OR sp.USER_ID = ?)', array($user_id, $user_id)); 
	}
	
	if($page > 0) {
		$page = $page - 1; //actual
		$pg_size = PAGE_SIZE;
		$limit_start = $pg_size*$page;
		$limit = array($limit_start, $pg_size);
	} else {
		$limit = NULL;
	}
	$db->join('PROBLEM_TYPES tp', 'p.PROBLEM_TYPE_ID=tp.ID', 'LEFT');
	$db->join('REG_AVILYS avilys', 'p.ID=avilys.PROBLEM_ID', 'LEFT');
	$db->join('STATUS_TYPES st', 'st.ID=p.STATUS_ID', 'LEFT');
	$db->join('SUBSCRIBED_PROBLEMS sp', 'sp.PROBLEM_ID = p.ID', 'LEFT');
	$db->orderBy('p.ID','DESC');
	$getProblemList = $db->get('REG_PROBLEMS p', $limit, '
		p.PROBLEM_TYPE_ID AS PROBLEM_TYPE_ID,
		tp.PROBLEM_TYPE AS PROBLEM_TYPE,
		p.ID AS PROBLEM_ID, 
		p.REG_DATE AS PROBLEM_DATE,
		p.COMPLETE_DATE AS PROBLEM_CLOSE_DATE,
		p.DESCRIPTION AS PROBLEM_DESC,
		p.ADDRESS AS PROBLEM_ADDRESS,
		p.USER_ID AS PROBLEM_USER,
        p.VERSION AS VERSION,
		avilys.AVILYS_REG_NR AS PROBLEM_AVILIO_NR,
		avilys.AVILYS_OID AS PROBLEM_AVILIO_OID,
		st.STATUS AS PROBLEM_STATUS_NAME,
		st.COLOR AS PROBLEM_STATUS_COLOR');
	$data = array();
	foreach($getProblemList as $p){
		$db2=MysqliDb::getInstance();
		$db2->where('PROBLEM_ID', $p['PROBLEM_ID']);
		$problemFile=$db2->getOne('REG_FILES');
		$db2->__destruct();
		if($problemFile['THUMBNAIL']==''){
			$p['THUMBNAIL']=MODULE_IMAGES_URL.'no_image_small.png';
		}
		else if($problemFile['THUMBNAIL']=='FILE_IMG_SMALL'){
			$p['THUMBNAIL']=MODULE_IMAGES_URL.'file_image_small.png';
		}
		else if($problemFile['THUMBNAIL']=='PDF_IMG_SMALL'){
			$p['THUMBNAIL']=MODULE_IMAGES_URL.'pdf_image_small.png';
		}
		else{
			if($problemFile['EDITED']==1){
				$p['THUMBNAIL']=MINIO_SITE_URL.$problemFile['BUCKET'].'.edited.thumbs/'.$problemFile['THUMBNAIL'];
			}
			else{
				$p['THUMBNAIL']=MINIO_SITE_URL.$problemFile['BUCKET'].'.thumbs/'.$problemFile['THUMBNAIL'];
			}	
		}
		$data[]=$p;
	}
	$db->__destruct();
	return $data;	
}

function getLocations($search_arr = null){
	$db = dbInit('webhost');
	$db->join('PROBLEM_TYPES tp', 'p.PROBLEM_TYPE_ID=tp.ID', 'LEFT');
	$db->join('REG_AVILYS avilys', 'p.ID=avilys.PROBLEM_ID', 'LEFT');
	$db->join('STATUS_TYPES st', 'st.ID=p.STATUS_ID', 'LEFT');
	$db->join('EXECUTOR exe', 'exe.TYPE_ID=p.PROBLEM_TYPE_ID', 'LEFT');
    $db->where('p.DELETED', 1, '!=');
	$db->orderBy('p.ID','DESC');
	$filter=filteredSearch($search_arr);
	foreach($filter as $f){
		call_user_func_array(array($db, 'where'),$f);
	}
	$getLocations=$db->get('REG_PROBLEMS p', null, '
		p.COORDS_GOOGLE_LAT AS koordLAT,
		p.COORDS_GOOGLE_LNG AS koordLONG,
		p.PROBLEM_TYPE_ID AS PROBLEM_TYPE_ID,
		tp.PROBLEM_TYPE AS PROBLEM_TYPE,
		p.ID AS PROBLEM_ID,
        p.ANSWER AS PROBLEM_ANSWER,
		p.REG_DATE AS PROBLEM_DATE,
		p.COMPLETE_DATE AS PROBLEM_CLOSE_DATE,
		p.DESCRIPTION AS PROBLEM_DESC,
		p.ADDRESS AS PROBLEM_ADDRESS,
		p.USER_ID AS PROBLEM_USER,
        p.VERSION AS VERSION,
		avilys.AVILYS_REG_NR AS PROBLEM_AVILIO_NR,
		st.STATUS AS PROBLEM_STATUS_NAME,
		st.COLOR AS PROBLEM_STATUS_COLOR,
        p.COMPLETE_DATE AS PROBLEM_CLOSE_DATE,
        exe.DEP_NAME AS PROBLEM_DEP_NAME');
	$db->__destruct();
	foreach($getLocations as $g){
		$data[]=array(
            'koordLAT'=>$g['koordLAT'],
            'koordLONG'=>$g['koordLONG'],
            'PROBLEM_TYPE'=>$g['PROBLEM_TYPE'],
            'PROBLEM_DATE'=>$g['PROBLEM_DATE'],
            'PROBLEM_ADDRESS'=>$g['PROBLEM_ADDRESS'],
            'PROBLEM_DESC'=>$g['PROBLEM_DESC'],
            'PROBLEM_STATUS_NAME'=>$g['PROBLEM_STATUS_NAME'],
            'PROBLEM_STATUS_COLOR'=>$g['PROBLEM_STATUS_COLOR'],
            'PROBLEM_AVILIO_NR'=>(!empty($g['PROBLEM_AVILIO_NR']) ? $g['PROBLEM_AVILIO_NR'] : $g['PROBLEM_ID'] ),
            'PROBLEM_ANSWER'=>$g['PROBLEM_ANSWER'],
            'PROBLEM_CLOSE_DATE'=>$g['PROBLEM_CLOSE_DATE'],
            'PROBLEM_DEP_NAME'=>$g['PROBLEM_DEP_NAME'],
            'problemPhotos' => getProblemsPhotos($g['PROBLEM_ID'])
        );
	}
	return $data;
}

function getProblem($ID){
	if($ID){
		$db = dbInit('webhost');
		$db->join('PROBLEM_TYPES tp', 'p.PROBLEM_TYPE_ID=tp.ID', 'LEFT');
		$db->join('REG_AVILYS avilys', 'p.ID=avilys.PROBLEM_ID', 'LEFT');
		$db->join('STATUS_TYPES st', 'st.ID=p.STATUS_ID', 'LEFT');
		$db->join('EXECUTOR exe', 'exe.TYPE_ID=p.PROBLEM_TYPE_ID', 'LEFT');
        $db->where('p.DELETED', 1, '!=');
		$db->where ('p.ID', $ID);
		$getProblem=$db->getOne('REG_PROBLEMS p', '
		tp.PROBLEM_TYPE AS PROBLEM_TYPE,
		p.PROBLEM_TYPE_ID AS PROBLEM_TYPE_ID,
		p.ID AS PROBLEM_ID,
		p.STATUS_ID AS PROBLEM_STATUS,
		p.REG_DATE AS PROBLEM_DATE,
		p.COMPLETE_DATE AS PROBLEM_CLOSE_DATE,
		p.DESCRIPTION AS PROBLEM_DESC,
		p.ADDRESS AS PROBLEM_ADDRESS,
		p.VIOLATION_DATETIME AS VIOLATION_DATETIME,
		p.CAR_PLATE_NO AS CAR_PLATE_NO,
		p.ANSWER AS PROBLEM_ANSWER,
		avilys.AVILYS_REG_NR AS PROBLEM_AVILIO_NR,
		avilys.AVILYS_OID AS PROBLEM_AVILIO_OID,
		p.COORDS_GOOGLE_LAT AS koordLAT,
		p.COORDS_GOOGLE_LNG AS koordLONG,
		p.COORDS_LKS_X AS koordX, 
		p.COORDS_LKS_Y AS koordY,
		p.USER_ID AS PROBLEM_USER,
		p.NUM_LIKE AS NUM_LIKE,
		p.NUM_DISLIKE AS NUM_DISLIKE,
		st.STATUS AS PROBLEM_STATUS_NAME,
		st.COLOR AS PROBLEM_STATUS_COLOR,
		exe.DEP_NAME AS PROBLEM_DEP_NAME,
		p.REPORTER_NAME AS REPORTER_NAME,
		p.REPORTER_EMAIL AS REPORTER_EMAIL,
		p.REPORTER_BIRTHDATE AS REPORTER_BIRTHDATE,
		p.VERSION AS VERSION');
		if(count($getProblem)>0){
			$getProblem['PROBLEM_DESC']=nl2br($getProblem['PROBLEM_DESC']);
			$getProblem['PROBLEM_ANSWER']=nl2br($getProblem['PROBLEM_ANSWER']);
			/// get attached files
			$db2=MysqliDb::getInstance();
			$db2->where('PROBLEM_ID', $ID);
			$problemFile=$db2->get('REG_FILES', null, 'ID, SAVED_AS, FILE_TYPE, BUCKET, EDITED');
			$db2->__destruct();
			$data_foto=array();
			foreach($problemFile as $pf){
				$data_foto[]=$pf;
			}
			/// get answer files
			$db3=MysqliDb::getInstance();
			$db3->where('PROBLEM_ID', $ID);
			$answerFile=$db3->get('REG_FILES_ANSWER',null,'FILE_NAME, SAVED_AS, FILE_TYPE, BUCKET');
			$db3->__destruct();
			$answer_foto=array();
			foreach($answerFile as $af){
				$answer_foto[]=$af;
			}
			
			return array(
				'data'=>$getProblem,
				'foto'=>$data_foto,
				'answer_foto'=>$answer_foto
			);
		}
	}
	else{
		return false;
	}
}

function addImages($ID, $imgs, $title) {
    if(!empty($imgs)) {
		foreach($imgs as $key => $foto){
			if(basename($foto) == $foto){
				$upload_path=UPLOAD_PATH.$foto;
				if(file_exists($upload_path)){
					$thmbnl=createThumbnail($foto);
					$foto_ext=strtolower(pathinfo($upload_path, PATHINFO_EXTENSION));
					copy(realpath($upload_path), MINIO_PATH.MINIO_BUCKET_NAME.'/'.$foto);

					if($thmbnl != 'PDF_IMG_SMALL' && $thmbnl != 'FILE_IMG_SMALL'){
						$upload_path2=realpath(UPLOAD_PATH.'thumbs/'.$foto);
						copy($upload_path2, MINIO_PATH.MINIO_BUCKET_NAME.'.thumbs/'.$foto);
						unlink($upload_path2);
					}
					$bucket=MINIO_BUCKET_NAME;
					$insertArrFiles = Array (
						'PROBLEM_ID' => $ID,
						'FILE_TYPE'  => $foto_ext,
						'SAVED_AS'   => $foto,
						'TITLE'      => $title[$key],
						'THUMBNAIL'  => $thmbnl,
						'BUCKET'     => $bucket
					);
					$db = MysqliDb::getInstance();
					$db->insert('REG_FILES', $insertArrFiles);
					$db->__destruct();
				}
			}
		}
	}
}

function deleteImages($ID) {
    $db = MysqliDb::getInstance();
	$db->where('ID', $ID, 'IN');
	$results = $db->get('REG_FILES', null, 'SAVED_AS');

	foreach($results as $result) {
		unlink(realpath(MINIO_PATH.MINIO_BUCKET_NAME.'/'.$result['SAVED_AS']));
		unlink(realpath(MINIO_PATH.MINIO_BUCKET_NAME.'.thumbs/'.$result['SAVED_AS']));

		if(file_exists(MINIO_PATH.MINIO_BUCKET_NAME.'.edited/'.$result['SAVED_AS'])) {
			unlink(realpath(MINIO_PATH.MINIO_BUCKET_NAME.'.edited/'.$result['SAVED_AS']));
			unlink(realpath(MINIO_PATH.MINIO_BUCKET_NAME.'.edited.thumbs/'.$result['SAVED_AS'])); 
		}
	}
	
	$db->where('ID', $ID, 'IN');
    $db->delete('REG_FILES');
}

function getProblemCity($id){
	$db = dbInit('webhost');
	$db->where ('ID', $id);
	$city=$db->getOne('REG_PROBLEMS');
	$db->__destruct();
	return $city['CITY_ID'];
}

function getProblemType($id){
	$db = dbInit('webhost');
	$db->where ('ID', $id);
	$problemType=$db->getOne('PROBLEM_TYPES');
	$db->__destruct();
	return $problemType['PROBLEM_TYPE'];
}

function getProblemTypes($searchCriterias = null, $activeProblems = null, $userId= null, $parentId= null){
	$db = dbInit('webhost');
	$db->where ('ACTIVE', '1');
	$db->where ('CITY_ID', $_SESSION['CITY_ID']);
	if($parentId>0){
		$db->where ('PARENT_ID', $parentId);
	}
	//$db->orderBy('CHAR_LENGTH(PROBLEM_TYPE)','ASC');
	$db->orderBy('PROBLEM_TYPE','ASC');
	$problemArr=$db->get('PROBLEM_TYPES'); 
	$db->__destruct();
	foreach($problemArr as $t){
		$problemTypes[]=array(
			'ID'=>$t['ID'],
			'PROBLEM_TYPE'=>$t['PROBLEM_TYPE'],
			'PARENT_ID'=>$t['PARENT_ID'],
            'SHOW_REGISTER' =>$t['SHOW_REGISTER'],
            'SHORT_LABEL' =>$t['SHORT_LABEL'],
            'KEYWORDS' =>$t['KEYS'],
			'ATTACHMENT' =>$t['ATTACHMENT'],
            'problemsCount' => (isset($activeProblems) ? (in_array($t['ID'], $activeProblems) ? countProblemsByProblemType($t['ID'], $searchCriterias, $userId) : null) : null)
		);
	}
	return $problemTypes;
}

function getProblemStatuses(){  
	$db = dbInit('webhost');
	$db->where ('ACTIVE', '1');
	$statusList=$db->get('STATUS_TYPES');
	$db->__destruct();
	return $statusList;
}

function getExecutor($id){
	$db = dbInit('webhost');
	$db->where ('TYPE_ID', $id);
	$executor=$db->getOne('EXECUTOR');
	$db->__destruct();
	return $executor;
}

function createThumbnail($img, $blur = false) {
	$upload_image = (isset($blur) && $blur=='1') ? MINIO_PATH.MINIO_BUCKET_NAME.'.edited/'.$img : GLOBAL_REAL_URL.UPLOAD_PATH.$img;    //upload image path
	$file_ext = strtolower(pathinfo($upload_image, PATHINFO_EXTENSION));
	if($file_ext == 'jpg' || $file_ext == 'jpeg' || $file_ext == 'png' || $file_ext == 'gif') {
		//thumbnail creation
		$thumbnail = GLOBAL_REAL_URL.UPLOAD_PATH.'thumbs/'.$img;
		$magicianObj = new imageLib($upload_image);
		$magicianObj->resizeImage(1080, 720, true);
		$magicianObj->saveImage($upload_image, 90);
		$magicianObj->resizeImage(200, 200, true);
		$magicianObj->saveImage($thumbnail, 90);
		
		$thumbnail = $img;
	} else if($file_ext=='pdf') {
		$thumbnail = 'PDF_IMG_SMALL';
	} else {
		$thumbnail = 'FILE_IMG_SMALL';
	}

	return $thumbnail;
}

function grid2geo($x, $y) {
	$pi = pi();
	$distsize = 3;
	$j = 0;
	$units = 1;
	$k = 0.9998;
	$a = 6378137;
	$f = 1 / 298.257223563;
	$b = $a * (1 - $f);
	$e2 = ($a * $a - $b * $b) / ($a * $a);
	$e = sqrt($e2);
	$ei2 = ($a * $a - $b * $b) / ($b * $b);
	$ei = sqrt($ei2);
	$n = ($a - $b) / ($a + $b);
	$G = $a * (1 - $n) * (1 - $n * $n) * (1 + (9 / 4) * $n * $n + (255 / 64) * pow($n, 4)) * ($pi / 180);
	$north = ($y - 0) * $units;
	$east = ($x - 500000) * $units;
	$m = $north / $k;
	$sigma = ($m * $pi) / (180 * $G);
	$footlat = $sigma + ((3 * $n / 2) - (27 * pow($n, 3) / 32)) * sin(2 * $sigma) + ((21 * $n * $n / 16) - (55 * pow($n, 4) / 32)) * sin(4 * $sigma) + (151 * pow($n, 3) / 96) * sin(6 * $sigma) + (1097 * pow($n, 4) / 512) * sin(8 * $sigma);
	$rho = $a * (1 - $e2) / pow(1 - ($e2 * sin($footlat) * sin($footlat)), (3 / 2));
	$nu = $a / sqrt(1 - ($e2 * sin($footlat) * sin($footlat)));
	$psi = $nu / $rho;
	$t = tan($footlat);
	$x = $east / ($k * $nu);
	$laterm1 = ($t / ($k * $rho )) * ( $east * $x / 2);
	$laterm2 = ($t / ($k * $rho )) * ( $east * pow($x, 3) / 24) * (-4 * $psi * $psi + 9 * $psi * (1 - $t * $t) + 12 * $t * $t );
	$laterm3 = ($t / ($k * $rho )) * ( $east * pow($x, 5) / 720) * (8 * pow($psi, 4) * (11 - 24 * $t * $t) - 12 * pow($psi, 3) * (21 - 71 * $t * $t) + 15 * $psi * $psi * (15 - 98 * $t * $t + 15 * pow($t, 4)) + 180 * $psi * (5 * $t * $t - 3 * pow($t, 4)) + 360 * pow($t, 4));
	$laterm4 = ($t / ($k * $rho )) * ( $east * pow($x, 7) / 40320) * (1385 + 3633 * $t * $t + 4095 * pow($t, 4) + 1575 * pow($t, 6));
	$latrad = $footlat - $laterm1 + $laterm2 - $laterm3 + $laterm4;
	$lat_deg = rad2deg($latrad);
	$seclat = 1 / cos($footlat);
	$loterm1 = $x * $seclat;
	$loterm2 = (pow($x, 3) / 6) * $seclat * ($psi + 2 * $t * $t);
	$loterm3 = (pow($x, 5) / 120) * $seclat * (-4 * pow($psi, 3) * (1 - 6 * $t * $t) + $psi * $psi * (9 - 68 * $t * $t) + 72 * $psi * $t * $t + 24 * pow($t, 4));
	$loterm4 = (pow($x, 7) / 5040) * $seclat * (61 + 662 * $t * $t + 1320 * pow($t, 4) + 720 * pow($t, 6));
	$w = $loterm1 - $loterm2 + $loterm3 - $loterm4;
	$longrad = deg2rad(24) + $w;
	$lon_deg = rad2deg($longrad);
	return array($lat_deg, $lon_deg);
}

function geo2grid($lat, $lon) {
	$pi = pi();
	$distsize = 3;
	$j = 0;
	$units = 1;
	$latddd = $lat;
	$latrad = deg2rad($latddd);
	$londdd = $lon;
	$lonrad = deg2rad($londdd);
	$k = 0.9998;
	$a = 6378137;
	$f = 1 / 298.257223563;
	$b = $a * (1 - $f);
	$e2 = ($a * $a - $b * $b) / ($a * $a);
	$e = sqrt($e2);
	$ei2 = ($a * $a - $b * $b) / ($b * $b);
	$ei = sqrt($ei2);
	$n = ($a - $b) / ($a + $b);
	$G = $a * (1 - $n) * (1 - $n * $n) * (1 + (9 / 4) * $n * $n + (255 / 64) * pow($n, 4)) * ($pi / 180);

	$w = $londdd - 24;
	$w = deg2rad($w);
	$t = tan($latrad);
	$rho = $a * (1 - $e2) / pow(1 - ($e2 * sin($latrad) * sin($latrad)), (3 / 2));
	$nu = $a / sqrt(1 - ($e2 * sin($latrad) * sin($latrad)));
	$psi = $nu / $rho;
	$coslat = cos($latrad);
	$sinlat = sin($latrad);

	$A0 = 1 - ($e2 / 4) - (3 * $e2 * $e2 / 64) - (5 * pow($e2, 3) / 256);
		$A2 = (3 / 8) * ($e2 + ($e2 * $e2 / 4) + (15 * pow($e2, 3) / 128));
		$A4 = (15 / 256) * ($e2 * $e2 + (3 * pow($e2, 3) / 4));
		$A6 = 35 * pow($e2, 3) / 3072;
		$m = $a * (($A0 * $latrad) - ($A2 * sin(2 * $latrad)) + ($A4 * sin(4 * $latrad)) - ($A6 * sin(6 * $latrad)));

		$eterm1 = ($w * $w / 6) * $coslat * $coslat * ($psi - $t * $t);
		$eterm2 = (pow($w, 4) / 120) * pow($coslat, 4) * (4 * pow($psi, 3) * (1 - 6 * $t * $t) + $psi * $psi * (1 + 8 * $t * $t) - $psi * 2 * $t * $t + pow($t, 4));
		$eterm3 = (pow($w, 6) / 5040) * pow($coslat, 6) * (61 - 479 * $t * $t + 179 * pow($t, 4) - pow($t, 6));
		$dE = $k * $nu * $w * $coslat * (1 + $eterm1 + $eterm2 + $eterm3);
		//$east = $this->roundoff(500000 + ($dE / $units), $distsize);
	$east = roundoff(500000 + ($dE / $units), $distsize);

		$nterm1 = ($w * $w / 2) * $nu * $sinlat * $coslat;
		$nterm2 = (pow($w, 4) / 24) * $nu * $sinlat * pow($coslat, 3) * (4 * $psi * $psi + $psi - $t * $t);
		$nterm3 = (pow($w, 6) / 720) * $nu * $sinlat * pow($coslat, 5) * (8 * pow($psi, 4) * (11 - 24 * $t * $t) - 28 * pow($psi, 3) * (1 - 6 * $t * $t) + $psi * $psi * (1 - 32 * $t * $t) - $psi * 2 * $t * $t + pow($t, 4));
		$nterm4 = (pow($w, 8) / 40320) * $nu * $sinlat * pow($coslat, 7) * (1385 - 3111 * $t * $t + 543 * pow($t, 4) - pow($t, 6));
		$dN = $k * ($m + $nterm1 + $nterm2 + $nterm3 + $nterm4);
		//$north = $this->roundoff(0 + ($dN / $units), $distsize);
	$north = roundoff(0 + ($dN / $units), $distsize);

	return array($east, $north);
}

function roundoff($x, $y) {
	$x = round($x * pow(10, $y)) / pow(10, $y);
	return $x;
}

function saveSearchCriterias($criterias,$session,$type){
	$db = dbInit('webhost');
	if (isset($session['AUTH'])):
		$insertArray = array('CITY_ID'=>$session['CITY_ID'],'SEARCH_DATA'=>json_encode($criterias),'USER_ID'=>$session['USER_ID'],'IP_ADDRESS'=>$_SERVER['REMOTE_ADDR'],'SEARCH_TYPE'=>$type);
	else:
		$insertArray = array('CITY_ID'=>$session['CITY_ID'],'SEARCH_DATA'=>json_encode($criterias),'IP_ADDRESS'=>$_SERVER['REMOTE_ADDR'],'SEARCH_TYPE'=>$type);
	endif;
	$db->insert('SEARCH_LOGS', $insertArray);
}

function addToSubscription($problemID, $userID) {
    $db = dbInit('webhost');
	$data = Array (
		'USER_ID' => $userID,
		'PROBLEM_ID' => $problemID,
	);
    $db->where('USER_ID', $userID);
    $db->where('PROBLEM_ID', $problemID);
    $count = $db->getOne('SUBSCRIBED_PROBLEMS');
    if($count < 1) {
        $db->insert('SUBSCRIBED_PROBLEMS', $data);    
    }
	return $db;
}

function checkIfSubscribed($problemID, $userID) {
    $db = dbInit('webhost');
    $db->where('PROBLEM_ID', $problemID);
    $db->where('USER_ID', $userID);
    $db->getOne('SUBSCRIBED_PROBLEMS'); 
    
    if($db->count > 0) {
        return true;
    } else {
        return false;
    }
}

function checkIfProblemOwnedByOwner($problemID, $userID) {
    $db = dbInit('webhost');
    $db->where('ID', $problemID);
    $db->where('USER_ID', $userID);
    $count = $db->getOne('REG_PROBLEMS'); 
    
    if($count > 0) {
        return true;
    } else {
        return false;
    }
}

function unsubscribe($problemID, $userID) {
    $db = dbInit('webhost');
    $db->where('PROBLEM_ID', $problemID);
    $db->where('USER_ID', $userID);
    $db->delete('SUBSCRIBED_PROBLEMS');
}

function deleteProblem($problemID, $problemOID, $reason) {
    if(empty($reason)) { $reason = 'neįrašyta'; }
    
    $db = dbInit('webhost');
    $data = array(
        'PROBLEM_ID'    => $problemID,
        'REASON'        => $reason,
        'DELETION_DATE' => date('Y-m-d H:i:s')
    );
	$db->insert('REG_PROBLEMS_DELETED', $data);

	$data = array(
        'DELETED' => 1,
    );
	$db->where('ID', $problemID);
	$db->update('REG_PROBLEMS', $data);
    
    $dvsCFG = getDVSConfig($_SESSION['CITY_ID']);
	$avilys = new Avilys($dvsCFG);
    $avilys->deleteProblem($problemOID, $reason);
}

?>