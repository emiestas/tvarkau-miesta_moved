<?php
class Menu{
	var $menuArray;
	function __construct(){
		$this -> problems_list = array(
			'sys_name' => 'problems_list',
			'php'      => MODULE_PATH_INNER .'problems_list/problems_list.php',
			'tpl'      => MODULE_PATH_INNER .'problems_list/problems_list.tpl',
			'name'     => 'Pranešimų sąrašas',
			'icon' => 'file-text',
			'private' => 0,
			'hide' => 0,
			'group' => '',
            'index_page' => 1
		);
		
		$this -> new_problem = array(
			'sys_name' => 'new_problem',
			'php'  => MODULE_PATH_INNER .'new_problem/new_problem.php',
			'tpl'  => MODULE_PATH_INNER .'new_problem/new_problem.tpl',
			'name' => 'Naujas pranešimas',
			'icon' => 'plus',
			'private' => 1,
			'hide' => 0,
			'group' => '',
			'index_page' => 0
		);
		
		$this -> map = array(
			'sys_name' => 'map',
			'php'      => MODULE_PATH_INNER .'map/map.php',
			'tpl'      => MODULE_PATH_INNER .'map/map.tpl',
			'name'     => 'Žemėlapis',
			'icon' => 'map-marker',
			'private' => 0,
			'hide' => 0,
			'group' => '',
			'index_page' => 0
		);
		
		$this -> user_problems_list = array(
			'sys_name' => 'user_problems_list',
			'php'      => MODULE_PATH_INNER .'user_problems_list/user_problems_list.php',
			'tpl'      => MODULE_PATH_INNER .'problems_list/problems_list.tpl',
			'name'     => 'Mano pranešimai',
			'icon' => 'list-ol',
			'private' => 1,
			'hide' => 0,
            'group' => 'user',
			'index_page' => 0
		);
		
		$this -> logout = array(
			'sys_name' => 'logout',
			'php'      => 'logout.php',
			'tpl'    => '',
			'name'     => 'Atsijungti',
			'icon' => 'sign-out',
			'private' => 0,
			'hide' => 0,
            'group' => 'user',
			'index_page' => 0
		);
		
		$this -> statistics = array(
			'sys_name' => 'statistics',
			'php'      => MODULE_PATH_INNER .'statistics/statistics.php',
			'tpl'      => MODULE_PATH_INNER .'statistics/statistics.tpl',
			'name'     => 'Statistika',
			'icon' => 'bar-chart',
			'private' => 0,
			'hide' => 0,
			'group' => '',
			'index_page' => 0
		);
		
		$this -> user_profile = array(
			'sys_name' => 'user_profile',
			'php'      => MODULE_PATH_INNER .'user_profile/user_profile.php',
			'tpl'      => MODULE_PATH_INNER .'user_profile/user_profile.tpl',
			'name'     => (isset($_SESSION['NAME']) ? $_SESSION['NAME'] : 'Prisijungti'),
			'menu'		=>	array(),
			'icon' => (isset($_SESSION['NAME']) ? 'user' : 'sign-in'),
			'private' => 1,
			'hide' => 0,
            'group' => 'user',
			'index_page' => 0
		);
		$this -> menuArray   = array();
		$this -> menuArray[] = $this -> new_problem;
		$this -> menuArray[] = $this -> map;
		$this -> menuArray[] = $this -> problems_list;
		$this -> menuArray[] = $this -> user_problems_list;
		$this -> menuArray[] = $this -> statistics;
		$this -> menuArray[] = $this -> user_profile;
		$this -> menuArray[] = $this -> logout;
	}
     
	function getMenu(){
		return $this -> menuArray;
	}
}
?>