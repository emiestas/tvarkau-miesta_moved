<?php
//logout
ini_set('session.cookie_httponly', 1);
ini_set('session.cookie_secure', 1);  
session_start();
session_destroy();
session_unset();
header("Location: ".GLOBAL_SITE_URL);
?>