<?
include_once("./configuration/config.php");
?>
<div>   

        <img alt="Progress" src="images/VILNIUS_RED_TRANSPARENT_RGB.png" id="imgProg" />
    
<div style="display:hidden;" id="my-signin2"></div>
</div>

<script src="<?=LIB_SITE_URL?>libs/jquery/jquery-min.js"></script>
<meta name="google-signin-client_id" content="<?=GOOGLE_CLIENT_ID?>">
<script src="https://apis.google.com/js/platform.js?onload=renderGoogleButton" async defer></script>
<script>
$(document).ready(function() {
	$("#imgProg").show();
	//$('#LoadPage').load('Default.aspx', function() {
	//	$("#imgProg").hide();
	//});
});

function onSuccess(googleUser) {
	console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
	googleConnect( googleUser.getBasicProfile().getId(), googleUser.getBasicProfile().getName(),googleUser.getBasicProfile().getEmail())	
}
function googleConnect(user_id, user_name, user_email) {
	//var directionLink = $('#directionLink').val();
	var directionLink = 'new_problem';
	//console.log(JSON.stringify(response));
	$.ajax({  
		url:"login.php",  
		method:"POST",  
		data: {g_response_id:user_id, g_name:user_name, g_email:user_email, directionLink:directionLink},  
		success:function(data){   
			console.log(data);
			if ($.trim(data)){ 
				var res = $.parseJSON(data);
				if(res[0]==1) {  
					location.href=res[1]; 					
				}
			} 
		} 
	});
}
function onFailure(error) {
	console.log(error);
}
function renderGoogleButton() {
	gapi.signin2.render('my-signin2', {
        'scope': 'profile email',
        'width': 45,
        'height': 45,
        //'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
	});
}
</script>
<?
//header("Location: new_problem");
?>